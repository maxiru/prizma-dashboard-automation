# Prizma Dashboard UI automation

**How to launch tests locally:**

*** Navigate to folder ```prizma-dashboard-automation``` where project was pulled.

*** Disable Jenkins parameter and enable and setup local parameters. Don't forget to setup appPath!!! Example:

        //for Jenkins
        String platform = System.getProperty("gs.platform");
        String link = System.getProperty("gs.link");

        **Disable/Commit**

        //for local testing
        String platform = "Chrome";
        String link = "https://dev.prizma.simis.ai/";

        **Enable/Uncommit**

*** Execute command in terminal to launch needed test suite (command for each test suite could be found in `build.gradle`). Example: ```./gradlew clean testTask --info```

**How to add new tests to Jenkins job (pipeline):**

*** Add new test suite for tests folder (`prizma.maxiru.tests`), add new test suit class for testsuite folder(`Prizma.maxiru.testsuite`), add new task in `build.gradle`. Example:

`task testNewTest(type: Test) {
    testLogging.events = ["failed", "passed", "skipped"]
    ignoreFailures = true
    systemProperties System.properties
    include 'Prizma/maxiru/GSNewTestSuite.class'
}`

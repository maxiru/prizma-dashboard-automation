package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaHomePage.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PrizmaSignupPage {

    //Elements with XPath, Tag and CCS selector
    public static final String FULL_NAME_INPUT = "//input[@type='text']";
    public static final String LAST_NAME_INPUT = "//input[@type='text']";
    public static final String CREATOR_CHECKBOX = ".auth-reg label:nth-child(6)";
    public static final String INVESTOR_CHECKBOX = ".auth-reg label:nth-child(7)";
    public static final String EMAIL_INPUT = ".auth-reg label:nth-child(3) input[type=text]";
    public static final String DISPLAY_NAME_INPUT = ".auth-reg label:nth-child(2) input[type=text]";
    public static final String PASSWORD_INPUT = ".auth-reg label:nth-child(4) input[type=password]";
    public static final String REPEAT_PASSWORD_INPUT = ".auth-reg label:nth-child(5) input[type=password]";
    public static final String REGISTER_BTN = "//button[text()='Sign up']";
    public static final String SIGN_IN_BTN = "//div[@class='switch-auth']/button";
    public static final String SHOW_PASSWORD_BTN = "//button[@class='switch']";
    public static final String HIDE_PASSWORD_BTN = "//button[@class='switch _visible']";
    public static final String SHOW_REPEAT_PASSWORD_BTN = "//button[@class='switch']";
    public static final String HIDE_REPEAT_PASSWORD_BTN = "//button[@class='switch _visible']";
    public static final String MESSAGE_TEXT = ".body .message";

    //Other elements
    public static String LP_DISPLAY_NAME_FOR_FUTURE_TESTS = "";
    public static String LP_EMAIL_FOR_FUTURE_TESTS = "";
    public static String LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS = "";
    public static String LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS = "";
    public static String LP_EMAIL_USER_A_FOR_FUTURE_TESTS = "";
    public static String LP_EMAIL_USER_B_FOR_FUTURE_TESTS = "";
    public static String FIRST_NAME_ERROR_MESSAGE = "Please enter a valid full name";
    public static String LAST_NAME_ERROR_MESSAGE = "Please enter a valid full name";
    public static String ROLES_ERROR_MESSAGE = "You should choose at least one of role: creator or investor";
    public static String EMAIL_ERROR_MESSAGE = "Please enter a valid email";
    public static String DISPLAY_NAME_ERROR_MESSAGE = "You enter display name should contains at least 3 symbols";
    public static String PASSWORD_ERROR_MESSAGE = "You enter password should contains at least 3 symbols";
    public static String USER_ALREADY_EXIST_ERROR_MESSAGE = "A user with that username already exists.";
    public static String EMAIL_ALREADY_EXIST_ERROR_MESSAGE = "A user with this email already exists.";
    public static String PASSWORD_NOT_MATCH_ERROR_MESSAGE = "You have entered mismatched passwords";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getFullNameInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(FULL_NAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FULL_NAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(FULL_NAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + FULL_NAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getCreatorCheckbox() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(CREATOR_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CREATOR_CHECKBOX + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(CREATOR_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + CREATOR_CHECKBOX + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getInvestorCheckbox() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INVESTOR_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTOR_CHECKBOX + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(INVESTOR_CHECKBOX));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTOR_CHECKBOX + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getEmailInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(EMAIL_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EMAIL_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(EMAIL_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EMAIL_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getDisplayNameInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(DISPLAY_NAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DISPLAY_NAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(DISPLAY_NAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DISPLAY_NAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getPasswordInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRepeatPasswordInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(REPEAT_PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REPEAT_PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(REPEAT_PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REPEAT_PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRegisterBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(REGISTER_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REGISTER_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(REGISTER_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REGISTER_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getSignInBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SIGN_IN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIGN_IN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SIGN_IN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIGN_IN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getShowPasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SHOW_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SHOW_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getHidePasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(HIDE_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(HIDE_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getShowRepeatPasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SHOW_REPEAT_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_REPEAT_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SHOW_REPEAT_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_REPEAT_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getHideRepeatPasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(HIDE_REPEAT_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_REPEAT_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(HIDE_REPEAT_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_REPEAT_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getMessageText() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(MESSAGE_TEXT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + MESSAGE_TEXT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(MESSAGE_TEXT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + MESSAGE_TEXT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnFullNameInput() {
        PrizmaUtils.scrollToElementOnPage(getFullNameInput());
        PrizmaDriver.waitToBeClickable(getFullNameInput());
        getFullNameInput().click();
    }

    public static void checkCreatorCheckboxOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getCreatorCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void uncheckCreatorCheckboxOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getCreatorCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void checkCreatorCheckboxOnSignUpPageWithEnter() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (!PrizmaDriver.isChromeBrowser()) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            getCreatorCheckbox().sendKeys(Keys.RETURN);
        } else {
            getCreatorCheckbox().sendKeys(Keys.ENTER);
        }
    }

    public static void uncheckCreatorCheckboxOnSignUpPageWithEnter() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (!PrizmaDriver.isChromeBrowser()) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            getCreatorCheckbox().sendKeys(Keys.RETURN);
        } else {
            getCreatorCheckbox().sendKeys(Keys.ENTER);
        }
    }

    public static void checkCreatorCheckboxOnSignUpPageWithTab() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getCreatorCheckbox().sendKeys(Keys.TAB);

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void checkInvestorCheckboxOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getInvestorCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void uncheckInvestorCheckboxOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getInvestorCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void checkInvestorCheckboxOnSignUpPageWithEnter() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (!PrizmaDriver.isChromeBrowser()) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            getInvestorCheckbox().sendKeys(Keys.RETURN);
        } else {
            getInvestorCheckbox().sendKeys(Keys.ENTER);
        }
    }

    public static void uncheckInvestorCheckboxOnSignUpPageWithEnter() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        if (!PrizmaDriver.isChromeBrowser()) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            getInvestorCheckbox().sendKeys(Keys.RETURN);
        } else {
            getInvestorCheckbox().sendKeys(Keys.ENTER);
        }
    }

    public static void checkInvestorCheckboxOnSignUpPageWithTab() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getInvestorCheckbox().sendKeys(Keys.TAB);

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnEmailInput() {
        PrizmaUtils.scrollToElementOnPage(getEmailInput());
        PrizmaDriver.waitToBeClickable(getEmailInput());
        getEmailInput().click();
    }

    public static void clickOnDisplayNameInput() {
        PrizmaUtils.scrollToElementOnPage(getDisplayNameInput());
        PrizmaDriver.waitToBeClickable(getDisplayNameInput());
        getDisplayNameInput().click();
    }

    public static void clickOnPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getPasswordInput());
        PrizmaDriver.waitToBeClickable(getPasswordInput());
        getPasswordInput().click();
    }

    public static void clickOnRepeatPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getRepeatPasswordInput());
        PrizmaDriver.waitToBeClickable(getRepeatPasswordInput());
        getRepeatPasswordInput().click();
    }

    public static void clickOnRegisterBtnOnSignUpPage() {
        PrizmaUtils.scrollToElementOnPage(getRegisterBtn());
        PrizmaDriver.waitToBeClickable(getRegisterBtn());
        getRegisterBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnSignInBtnOnSignUpPage() {
        PrizmaUtils.scrollToElementOnPage(getSignInBtn());
        PrizmaDriver.waitToBeClickable(getSignInBtn());
        getSignInBtn().click();
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearFullNameInput() {
        PrizmaUtils.scrollToElementOnPage(getFullNameInput());
        PrizmaDriver.waitToBeClickable(getFullNameInput());
        getFullNameInput().clear();
    }

    public static void clearEmailInput() {
        PrizmaUtils.scrollToElementOnPage(getEmailInput());
        PrizmaDriver.waitToBeClickable(getEmailInput());
        getEmailInput().clear();
    }

    public static void clearDisplayNameInput() {
        PrizmaUtils.scrollToElementOnPage(getDisplayNameInput());
        PrizmaDriver.waitToBeClickable(getDisplayNameInput());
        getDisplayNameInput().clear();
    }

    public static void clearPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getPasswordInput());
        PrizmaDriver.waitToBeClickable(getPasswordInput());
        getPasswordInput().clear();
    }

    public static void clearRepeatPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getRepeatPasswordInput());
        PrizmaDriver.waitToBeClickable(getRepeatPasswordInput());
        getRepeatPasswordInput().clear();
    }

    public static void clickOnShowPasswordBtnOnSignUpPage() {
        try {
            PrizmaDriver.waitToBeClickable(getShowPasswordBtn());
            getShowPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getHidePasswordBtn());
            getHidePasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnHidePasswordBtnOnSignUpPage() {
        try {
            PrizmaDriver.waitToBeClickable(getHidePasswordBtn());
            getHidePasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getShowPasswordBtn());
            getShowPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnShowRepeatPasswordBtnOnSignUpPage() {
        try {
            PrizmaDriver.waitToBeClickable(getShowRepeatPasswordBtn());
            getShowRepeatPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getHideRepeatPasswordBtn());
            getHideRepeatPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnHideRepeatPasswordBtnOnSignUpPage() {
        try {
            PrizmaDriver.waitToBeClickable(getHideRepeatPasswordBtn());
            getHideRepeatPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getShowRepeatPasswordBtn());
            getShowRepeatPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to set the value into element
     */
    public static void setFullNameOnSignUpPage(String fullName) {
        if (fullName.isEmpty()) {
            clickOnFullNameInput();
            clearFullNameInput();
        } else {
            clickOnFullNameInput();
            clearFullNameInput();
            getFullNameInput().sendKeys(fullName);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setFirstNameOnSignUpPageWithEnter(String firstName) {
        if (firstName.isEmpty()) {
            clickOnFullNameInput();
            clearFullNameInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getFullNameInput().sendKeys(Keys.RETURN);
            } else {
                getFullNameInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnFullNameInput();
            clearFullNameInput();
            getFullNameInput().sendKeys(firstName);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getFullNameInput().sendKeys(Keys.RETURN);
            } else {
                getFullNameInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setFullNameOnSignUpPageWithTab(String fullName) {
        if (fullName.isEmpty()) {
            clickOnFullNameInput();
            clearFullNameInput();
            getFullNameInput().sendKeys(Keys.TAB);
        } else {
            clickOnFullNameInput();
            clearFullNameInput();
            getFullNameInput().sendKeys(fullName);
            getFullNameInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPageWithoutModifications(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();
        } else {
            clickOnEmailInput();
            clearEmailInput();

            LP_EMAIL_FOR_FUTURE_TESTS = "";
            LP_EMAIL_FOR_FUTURE_TESTS = email;
            getEmailInput().sendKeys(LP_EMAIL_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPage(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();
        } else {
            clickOnEmailInput();
            clearEmailInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = email + "." + date + "@autotest.com";
            LP_EMAIL_FOR_FUTURE_TESTS = "";
            LP_EMAIL_FOR_FUTURE_TESTS = tempEmail;
            getEmailInput().sendKeys(LP_EMAIL_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPageAndSaveUserAToVariable(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();
        } else {
            clickOnEmailInput();
            clearEmailInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = email + "." + date + "@autotest.com";
            LP_EMAIL_USER_A_FOR_FUTURE_TESTS = "";
            LP_EMAIL_USER_A_FOR_FUTURE_TESTS = tempEmail;
            getEmailInput().sendKeys(LP_EMAIL_USER_A_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPageAndSaveUserBToVariable(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();
        } else {
            clickOnEmailInput();
            clearEmailInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = email + "." + date + "@autotest.com";
            LP_EMAIL_USER_B_FOR_FUTURE_TESTS = "";
            LP_EMAIL_USER_B_FOR_FUTURE_TESTS = tempEmail;
            getEmailInput().sendKeys(LP_EMAIL_USER_B_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPageWithoutTimestampWithTab(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();
            getEmailInput().sendKeys(Keys.TAB);
        } else {
            clickOnEmailInput();
            clearEmailInput();
            getEmailInput().sendKeys(email);
            getEmailInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignUpPageWithoutTimestampWithEnter(String email) {
        if (email.isEmpty()) {
            clickOnEmailInput();
            clearEmailInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getEmailInput().sendKeys(Keys.RETURN);
            } else {
                getEmailInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnEmailInput();
            clearEmailInput();
            getEmailInput().sendKeys(email);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getEmailInput().sendKeys(Keys.RETURN);
            } else {
                getEmailInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPage(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = displayName + "." + date;
            LP_DISPLAY_NAME_FOR_FUTURE_TESTS = "";
            LP_DISPLAY_NAME_FOR_FUTURE_TESTS = tempEmail;
            getDisplayNameInput().sendKeys(LP_DISPLAY_NAME_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPageAndSaveUserAToVariable(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = displayName + "." + date;
            LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS = "";
            LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS = tempEmail;
            getDisplayNameInput().sendKeys(LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPageAndSaveUserBToVariable(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();

            int date = (int) (new Date().getTime() / 1000);
            String tempEmail = displayName + "." + date;
            LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS = "";
            LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS = tempEmail;
            getDisplayNameInput().sendKeys(LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPageWithoutTimestamp(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
            getDisplayNameInput().sendKeys(displayName);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPageWithoutTimestampWithEnter(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getDisplayNameInput().sendKeys(Keys.RETURN);
            } else {
                getDisplayNameInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
            getDisplayNameInput().sendKeys(displayName);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getDisplayNameInput().sendKeys(Keys.RETURN);
            } else {
                getDisplayNameInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setDisplayNameOnSignUpPageWithoutTimestampWithTab(String displayName) {
        if (displayName.isEmpty()) {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
            getDisplayNameInput().sendKeys(Keys.TAB);
        } else {
            clickOnDisplayNameInput();
            clearDisplayNameInput();
            getDisplayNameInput().sendKeys(displayName);
            getDisplayNameInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignUpPage(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignUpPageWithEnter(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getPasswordInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getPasswordInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignUpPageWithTab(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(Keys.TAB);
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);
            getPasswordInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRepeatPasswordOnSignUpPage(String repeatPassword) {
        if (repeatPassword.isEmpty()) {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();
        } else {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();
            getRepeatPasswordInput().sendKeys(repeatPassword);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRepeatPasswordOnSignUpPageWithEnter(String repeatPassword) {
        if (repeatPassword.isEmpty()) {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getRepeatPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getRepeatPasswordInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();
            getRepeatPasswordInput().sendKeys(repeatPassword);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getRepeatPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getRepeatPasswordInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setRepeatPasswordOnSignUpPageWithTab(String repeatPassword) {
        if (repeatPassword.isEmpty()) {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();
            getRepeatPasswordInput().sendKeys(Keys.TAB);
        } else {
            clickOnRepeatPasswordInput();
            clearRepeatPasswordInput();
            getRepeatPasswordInput().sendKeys(repeatPassword);
            getRepeatPasswordInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromMessageText() {
        try {
            return getMessageText().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatFirstNameErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(FIRST_NAME_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as first name error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatLastNameErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(LAST_NAME_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as last name error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatRolesErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(ROLES_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as roles error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatEmailErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(EMAIL_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as email error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatDisplayNameErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(DISPLAY_NAME_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as display name error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatPasswordErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(PASSWORD_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as username error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserAlreadyExistErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(USER_ALREADY_EXIST_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user already exist error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatEmailAlreadyExistErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(EMAIL_ALREADY_EXIST_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as email already exist error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(PASSWORD_NOT_MATCH_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as password not match error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserRedirectFromSignUpPageToSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(SIGN_IN_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user from sign in page not redirect to sign up page. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserRedirectFromSignUpPageToHomePage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(CREATOR_BTN), 15);
        } catch (Throwable ex) {
            try {
                PrizmaDriver.waitDriver(By.cssSelector(ACCOUNT_BTN), 15);
            } catch (Throwable ex2) {
                Assert.fail("Looks like that something when wrong as user can't pass sign up flow and can't from sign up page redirect to home page. Check video and logs, or something was changed on Prizma DD side.");
            }
        }
    }
}

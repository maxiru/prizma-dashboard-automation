package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.List;

import prizma.maxiru.framework.common.*;

import static app.PrizmaHomePage.*;
import static app.PrizmaSignupPage.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class PrizmaSigninPage {

    //Elements with XPath, Tag and CCS selector
    public static final String USERNAME_INPUT = "input";
    public static final String EMAIL_INPUT = "input";
    public static final String PASSWORD_INPUT = "input";
    public static final String SIGN_IN_BTN = "//button[text()='Sign in']";
    public static final String REGISTRATION_BTN = "//div[@class='switch-auth']/button";
    public static final String SHOW_PASSWORD_BTN = "//button[@class='switch']";
    public static final String HIDE_PASSWORD_BTN = "//button[@class='switch _visible']";
    public static final String MESSAGE_TEXT = "//div[@class='message']";

    //Other elements
    public static String EMAIL_ERROR_MESSAGE_ON_SIGN_IN_PAGE = "Please enter a valid email";
    public static String USERNAME_ERROR_MESSAGE = "You enter username should contains at least 3 symbols";
    public static String PASSWORD_ERROR_MESSAGE = "You enter password should contains at least 3 symbols";
    public static String INCORRECT_EMAIL_PASSWORD_ERROR_MESSAGE = "Invalid Credentials.";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getUsernameInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.tagName(USERNAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USERNAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.tagName(USERNAME_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USERNAME_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getEmailInputOnSignInPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.tagName(EMAIL_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EMAIL_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.tagName(EMAIL_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EMAIL_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getPasswordInput() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.tagName(PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.tagName(PASSWORD_INPUT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PASSWORD_INPUT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(list.size() - 1);
        }
    }

    public static WebElement getSignInBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SIGN_IN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIGN_IN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SIGN_IN_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SIGN_IN_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getRegistrationBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(REGISTRATION_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REGISTRATION_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(REGISTRATION_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + REGISTRATION_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getShowPasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(SHOW_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(SHOW_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SHOW_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getHidePasswordBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(HIDE_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(HIDE_PASSWORD_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + HIDE_PASSWORD_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getMessageText() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(MESSAGE_TEXT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + MESSAGE_TEXT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(MESSAGE_TEXT));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + MESSAGE_TEXT + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnEmailInputOnSignInPage() {
        PrizmaUtils.scrollToElementOnPage(getEmailInputOnSignInPage());
        PrizmaDriver.waitToBeClickable(getEmailInputOnSignInPage());
        getEmailInputOnSignInPage().click();
    }

    public static void clickOnUsernameInput() {
        PrizmaUtils.scrollToElementOnPage(getUsernameInput());
        PrizmaDriver.waitToBeClickable(getUsernameInput());
        getUsernameInput().click();
    }

    public static void clickOnPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getPasswordInput());
        PrizmaDriver.waitToBeClickable(getPasswordInput());
        getPasswordInput().click();
    }

    public static void clickOnSignInBtnOnSignInPage() {
        PrizmaUtils.scrollToElementOnPage(getSignInBtn());
        PrizmaDriver.waitToBeClickable(getSignInBtn());
        getSignInBtn().click();
    }

    public static void clickOnRegistrationBtnOnSignInPage() {
        PrizmaUtils.scrollToElementOnPage(getRegistrationBtn());
        PrizmaDriver.waitToBeClickable(getRegistrationBtn());
        getRegistrationBtn().click();
    }

    public static void clickOnShowPasswordBtnOnSignInPage() {
        try {
            PrizmaDriver.waitToBeClickable(getShowPasswordBtn());
            getShowPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getHidePasswordBtn());
            getHidePasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnHidePasswordBtnOnSignInPage() {
        try {
            PrizmaDriver.waitToBeClickable(getHidePasswordBtn());
            getHidePasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitToBeClickable(getShowPasswordBtn());
            getShowPasswordBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearEmailInputOnSignInPage() {
        PrizmaUtils.scrollToElementOnPage(getEmailInputOnSignInPage());
        PrizmaDriver.waitToBeClickable(getEmailInputOnSignInPage());
        getEmailInputOnSignInPage().clear();
    }

    public static void clearUsernameInput() {
        PrizmaUtils.scrollToElementOnPage(getUsernameInput());
        PrizmaDriver.waitToBeClickable(getUsernameInput());
        getUsernameInput().clear();
    }

    public static void clearPasswordInput() {
        PrizmaUtils.scrollToElementOnPage(getPasswordInput());
        PrizmaDriver.waitToBeClickable(getPasswordInput());
        getPasswordInput().click();
    }

    /***
     * Methods to set the value into element
     */
    public static void setEmailOnSignInPage(String email) {
        if (email.isEmpty()) {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
        } else {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(email);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setUsernameOnSignInPage(String username) {
        if (username.isEmpty()) {
            clickOnUsernameInput();
            clearUsernameInput();
        } else {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(username);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setUsernameOnSignInPageForUserA() {
        if (LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS.isEmpty()) {
            clickOnUsernameInput();
            clearUsernameInput();
        } else {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(LP_DISPLAY_NAME_USER_A_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignInPageForUserA() {
        if (LP_EMAIL_USER_A_FOR_FUTURE_TESTS.isEmpty()) {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
        } else {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(LP_EMAIL_USER_A_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setUsernameOnSignInPageForUserB() {
        if (LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS.isEmpty()) {
            clickOnUsernameInput();
            clearUsernameInput();
        } else {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(LP_DISPLAY_NAME_USER_B_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignInPageForUserB() {
        if (LP_EMAIL_USER_B_FOR_FUTURE_TESTS.isEmpty()) {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
        } else {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(LP_EMAIL_USER_B_FOR_FUTURE_TESTS);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setUsernameOnSignInPageWithEnter(String username) {
        if (username.isEmpty()) {
            clickOnUsernameInput();
            clearUsernameInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getUsernameInput().sendKeys(Keys.RETURN);
            } else {
                getUsernameInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(username);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getUsernameInput().sendKeys(Keys.RETURN);
            } else {
                getUsernameInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignInPageWithEnter(String email) {
        if (email.isEmpty()) {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getEmailInputOnSignInPage().sendKeys(Keys.RETURN);
            } else {
                getEmailInputOnSignInPage().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(email);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getEmailInputOnSignInPage().sendKeys(Keys.RETURN);
            } else {
                getEmailInputOnSignInPage().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setUsernameOnSignInPageWithTab(String username) {
        if (username.isEmpty()) {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(Keys.TAB);
        } else {
            clickOnUsernameInput();
            clearUsernameInput();
            getUsernameInput().sendKeys(username);
            getUsernameInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setEmailOnSignInPageWithTab(String email) {
        if (email.isEmpty()) {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(Keys.TAB);
        } else {
            clickOnEmailInputOnSignInPage();
            clearEmailInputOnSignInPage();
            getEmailInputOnSignInPage().sendKeys(email);
            getEmailInputOnSignInPage().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignInPage(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignInPageWithEnter(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getPasswordInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getPasswordInput().sendKeys(Keys.RETURN);
            } else {
                getPasswordInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPasswordOnSignInPageWithTab(String password) {
        if (password.isEmpty()) {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(Keys.TAB);
        } else {
            clickOnPasswordInput();
            clearPasswordInput();
            getPasswordInput().sendKeys(password);
            getPasswordInput().sendKeys(Keys.TAB);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void signInBackToAccount(String email, String password) {
        try {
            setEmailOnSignInPage(email);
            setPasswordOnSignInPage(password);
            clickOnSignInBtnOnSignInPage();
            clickOnCreatorBtnOnHomePage();
            verifyThatUserRedirectFromSignInPageToHomePage();
        } catch (Throwable ex) {
            clickOnCreatorBtnOnHomePage();
            verifyThatUserRedirectFromSignInPageToHomePage();
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromMessageText() {
        try {
            return getMessageText().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatEmailErrorMessageAppearOnSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(EMAIL_ERROR_MESSAGE_ON_SIGN_IN_PAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as email error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUsernameErrorMessageAppearOnSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(USERNAME_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as username error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatPasswordErrorMessageAppearOnSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(PASSWORD_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as password error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatIncorrectEmailPasswordErrorMessageAppearOnSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromMessageText(), containsString(INCORRECT_EMAIL_PASSWORD_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as incorrect email/password error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserRedirectFromSignInPageToSignUpPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(REGISTER_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user from sign in page not redirect to sign up page. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserRedirectFromSignInPageToHomePage() {
        PrizmaDriver.waitFor(PrizmaConstants.LOG_OUT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(CREATOR_BTN), 15);
        } catch (Throwable ex) {
            try {
                try {
                    PrizmaDriver.waitDriver(By.cssSelector(INVESTOR_BTN), 15);
                } catch (Throwable ex2) {
                    PrizmaDriver.waitDriver(By.cssSelector(ACCOUNT_BTN), 15);
                }
            } catch (Throwable ex2) {
                Assert.fail("Looks like that something when wrong as user from sign in page not redirect to home page. Check video and logs, or something was changed on Prizma DD side.");
            }
        }
    }
}

package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.List;

import prizma.maxiru.framework.common.*;

import static app.PrizmaCreatorPage.*;
import static app.PrizmaInvestorBotDetailsPage.*;
import static app.PrizmaStorePage.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static prizma.maxiru.framework.common.PrizmaUtils.*;

public class PrizmaInvestorPage {

    //Elements with XPath, Tag and CCS selector
    public static final String VIEW_STORE_BTN = "//a[text()='View Store']";
    public static final String USER_AGREEMENT_OPEN_BTN_ON_INVESTOR_PAGE = "//span[text()='User Agreement']";
    public static final String USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE = ".modal-new__close-button";
    public static final String USER_AGREEMENT_TOGGLE_ON_INVESTOR_PAGE = "//label[contains(@class,'input-checkbox take-agree__checkbox')]";
    public static final String START_INVESTING_BTN_ON_INVESTOR_PAGE = "//button[text()='Start Investing']";
    public static final String TOTAL_RETURN_VALUE = ".bot-subscriptions-summary > div:nth-child(2) > .bot-subscriptions-summary__col__value";
    public static final String TOTAL_INVESTED_VALUE = ".bot-subscriptions-summary > div:nth-child(3) > .bot-subscriptions-summary__col__value";
    public static final String TOTAL_ASSETS_VALUE = ".bot-subscriptions-summary > div:nth-child(4) > .bot-subscriptions-summary__col__value";
    public static final String BOT_CARDS_LIST = ".bot-subscriptions__list";
    public static final String BOT_CARD = ".bot-subscriptions-row";
    public static final String BOT_NAME_LABEL = ".bot-subscription-row__name";
    public static final String COMMON_BOT_INFO = ".bot-subscriptions-cell__info";
    public static final String EXCHANGE_LABEL = ".bot-subscription-row__info__exchange";
    public static final String SUBSCRIPTION_DATE_LABEL = ".bot-subscription-row__info__date";
    public static final String RATE_FEE_LABEL = ".bot-subscription-row__info__rate";
    public static final String STATE_LABEL = ".bot-subscription-row__state";
    public static final String AVATAR_IMAGE = ".bot-subscription-row__avatar";
    public static final String BOT_PROFIT_LABEL = ".bot-subscription-row > div:nth-child(2) > div:nth-child(1) > .bot-subscription-row__characteristic__value > span:nth-child(1)";
    public static final String BOT_PERFORMANCE_LABEL = ".bot-subscription-row > div:nth-child(2) > div:nth-child(1) > .bot-subscription-row__characteristic__value > span:nth-child(2)";
    public static final String INVESTED_LABEL = ".bot-subscription-row > div:nth-child(2) > div:nth-child(2) > .bot-subscription-row__characteristic__value";
    public static final String EXIT_AMOUNT_LABEL = ".bot-subscription-row > div:nth-child(2) > div:nth-child(3) > .bot-subscription-row__characteristic__value > span:nth-child(1)";
    public static final String ASSETS_STABLECOIN_LABEL = ".bot-subscription-row > div:nth-child(3) > div:nth-child(1) > .bot-subscription-row__characteristic__value > div:nth-child(1) > .bot-subscription-row__asset_size";
    public static final String ASSETS_BOUGHT_SYMBOL_LABEL = ".bot-subscription-row > div:nth-child(3) > div:nth-child(1) > .bot-subscription-row__characteristic__value > div:nth-child(2) > span:nth-child(1)";
    public static final String ASSETS_BOUGHT_SYMBOL_USD_LABEL = ".bot-subscription-row > div:nth-child(3) > div:nth-child(1) > .bot-subscription-row__characteristic__value > div:nth-child(2) > span:nth-child(2)";
    public static final String BOT_TOTAL_ASSETS_LABEL = ".bot-subscription-row > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)";

    public static final String INVESTOR_STORE_TABLE_BOT_TOGGLE = "//div[@class='bot-subscriptions-cell__toggle']";
    public static final String INVESTOR_STORE_TABLE_BOT_TOGGLE_UNCHECKED = "//label[@class='input-checkbox']";
    public static final String INVESTOR_STORE_TABLE_BOT_TOGGLE_CHECKED = "//div[@class='bot-subscriptions-cell__toggle']";

    public static final String SETTINGS_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(1)";
    public static final String START_STOP_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(2)";
    public static final String DELETE_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(3)";

    public static final String SETTINGS_FORM = ".edit-botsubscription__form";
    public static final String INVESTED_AMOUNT_INPUT = ".edit-botsubscription__form > div:nth-child(1) > .edit-botsubscription__input > input:nth-child(2)";
    public static final String EXIT_AMOUNT_INPUT = ".edit-botsubscription__form > div:nth-child(2) > .edit-botsubscription__input > input:nth-child(2)";
    public static final String NOTIFY_LOWER_THAN_CHECKBOX = ".edit-botsubscription__form > div:nth-child(3) > .input-checkbox > span:nth-child(2)";
    public static final String NOTIFY_LOWER_THAN_INPUT = ".edit-botsubscription__form > div:nth-child(3) > .edit-botsubscription__input > input:nth-child(2)";
    public static final String INVESTED_AMOUNT_WARNING_LABEL = ".edit-botsubscription__form > div:nth-child(1) > .edit-botsubscription__input > .input-number__warning";
    public static final String EXIT_AMOUNT_WARNING_LABEL = ".edit-botsubscription__form > div:nth-child(2) > .edit-botsubscription__input > .input-number__warning";
    public static final String NOTIFY_LOWER_THAN_WARNING_LABEL = ".edit-botsubscription__form > div:nth-child(3) > .edit-botsubscription__input > .input-number__warning";
    public static final String OK_BTN_ON_SETTINGS_FORM = ".edit-botsubscription__buttons > button:nth-child(1)";
    public static final String CANCEL_BTN_ON_SETTINGS_FORM = ".edit-botsubscription__buttons > button:nth-child(2)";

    public static final String OK_BTN_ON_DEACTIVATE_BOT_DIALOG = ".stop-botsubscription button:nth-child(1)";
    public static final String CLOSE_BTN_ON_BOT_DETAILS = ".modal-new__close-button";

    //Other elements
    public static int BOT_POSITION_IN_LIST = 0;

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getViewStoreBtn() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(VIEW_STORE_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VIEW_STORE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(VIEW_STORE_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + VIEW_STORE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getUserAgreementOpenBtnOnInvestorPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(USER_AGREEMENT_OPEN_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_OPEN_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(USER_AGREEMENT_OPEN_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_OPEN_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getUserAgreementCloseBtnOnInvestorPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getUserAgreementToggleOnInvestorPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(USER_AGREEMENT_TOGGLE_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_TOGGLE_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(USER_AGREEMENT_TOGGLE_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + USER_AGREEMENT_TOGGLE_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getStartInvestingBtnOnInvestorPage() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(START_INVESTING_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_INVESTING_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(START_INVESTING_BTN_ON_INVESTOR_PAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_INVESTING_BTN_ON_INVESTOR_PAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getTotalReturnLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TOTAL_RETURN_VALUE));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TOTAL_RETURN_VALUE));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TOTAL_RETURN_VALUE + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getTotalInvestedLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TOTAL_INVESTED_VALUE));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TOTAL_INVESTED_VALUE));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TOTAL_INVESTED_VALUE + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getTotalAssetsLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TOTAL_ASSETS_VALUE));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TOTAL_ASSETS_VALUE));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TOTAL_ASSETS_VALUE + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBotCardsList() {
        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_CARDS_LIST), 60);
        } catch (Throwable ex) {
            //ignore
        }

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_CARDS_LIST));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_CARDS_LIST + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_CARDS_LIST));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_CARDS_LIST + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(0);
        }
    }

    public static WebElement getBotCardByPosition(int position) {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(BOT_CARD));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static int getBotsCount() {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(BOT_CARD));
        return list.size();
    }

    public static WebElement getBotCardByPositionFromVariable() {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(BOT_CARD));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(BOT_POSITION_IN_LIST - 1);
    }

    public static WebElement getBotNameLabelByPosition(int position) {
        List<WebElement> list;
        list = getBotCardByPosition(position).findElements(By.cssSelector(BOT_NAME_LABEL));
        if (list.size() == 0) {
            fail("Looks like that something when wrong as Investor table bot name label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0);
    }

    public static WebElement getBotNameLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getBotCardByPosition(BOT_POSITION_IN_LIST).findElements(By.cssSelector(BOT_NAME_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot name label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0);
    }

    public static WebElement getAvatar(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(AVATAR_IMAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + AVATAR_IMAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(AVATAR_IMAGE));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + AVATAR_IMAGE + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getExchangeLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(EXCHANGE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EXCHANGE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(EXCHANGE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EXCHANGE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getSubscriptionDateLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(SUBSCRIPTION_DATE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SUBSCRIPTION_DATE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(SUBSCRIPTION_DATE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SUBSCRIPTION_DATE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getRateFeeLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(RATE_FEE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RATE_FEE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(RATE_FEE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + RATE_FEE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getStateLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(STATE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STATE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(STATE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + STATE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBotInfoLabelByPosition(int position) {
        List<WebElement> list;
        list = getBotCardByPosition(position).findElements(By.cssSelector(COMMON_BOT_INFO));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0);
    }

    public static WebElement getBotInfoLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getBotCardByPosition(BOT_POSITION_IN_LIST).findElements(By.cssSelector(COMMON_BOT_INFO));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0);
    }

    public static WebElement getBotProfitLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_PROFIT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_PROFIT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_PROFIT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_PROFIT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static List getBotProfitsList() {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(BOT_PROFIT_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list;
    }

    public static WebElement getBotPerformanceLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_PERFORMANCE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_PERFORMANCE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_PERFORMANCE_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_PERFORMANCE_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getInvestedAmountLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INVESTED_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTED_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(INVESTED_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTED_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static List getBotInvestedAmountList() {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(INVESTED_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list;
    }

    public static WebElement getExitAmountLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(EXIT_AMOUNT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EXIT_AMOUNT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(EXIT_AMOUNT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + EXIT_AMOUNT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getAssetsStableCoinLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ASSETS_STABLECOIN_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_STABLECOIN_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ASSETS_STABLECOIN_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_STABLECOIN_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBoughtSymbolAssetsLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ASSETS_BOUGHT_SYMBOL_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_BOUGHT_SYMBOL_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ASSETS_BOUGHT_SYMBOL_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_BOUGHT_SYMBOL_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBoughtSymbolInUSDAssetsLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ASSETS_BOUGHT_SYMBOL_USD_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_BOUGHT_SYMBOL_USD_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ASSETS_BOUGHT_SYMBOL_USD_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + ASSETS_BOUGHT_SYMBOL_USD_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBotTotalAssetsLabel(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_TOTAL_ASSETS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_TOTAL_ASSETS_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_TOTAL_ASSETS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_TOTAL_ASSETS_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static List getBotTotalAssetsList() {
        List<WebElement> list;
        list = getBotCardsList().findElements(By.cssSelector(BOT_TOTAL_ASSETS_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list;
    }

    public static WebElement getSettingsBtn(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(SETTINGS_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SETTINGS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(SETTINGS_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + SETTINGS_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getStartStopBtn(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(START_STOP_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_STOP_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(START_STOP_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + START_STOP_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getDeleteBtn(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(DELETE_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DELETE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(DELETE_BTN));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + DELETE_BTN + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(SETTINGS_FORM));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(SETTINGS_FORM));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + SETTINGS_FORM + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getInvestedAmountInputInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(INVESTED_AMOUNT_INPUT));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(INVESTED_AMOUNT_INPUT));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + INVESTED_AMOUNT_INPUT + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getInvestedAmountErrorInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(INVESTED_AMOUNT_WARNING_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(INVESTED_AMOUNT_WARNING_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + INVESTED_AMOUNT_WARNING_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getExitAmountInputInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(EXIT_AMOUNT_INPUT));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(EXIT_AMOUNT_INPUT));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + EXIT_AMOUNT_INPUT + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getExitAmountErrorInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(EXIT_AMOUNT_WARNING_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(EXIT_AMOUNT_WARNING_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + EXIT_AMOUNT_WARNING_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getNotifyLowerThanCheckboxInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_CHECKBOX));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_CHECKBOX));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + NOTIFY_LOWER_THAN_CHECKBOX + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getNotifyLowerThanAmountInputInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_INPUT));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_INPUT));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + NOTIFY_LOWER_THAN_INPUT + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getNotifyLowerThanAmountErrorInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_WARNING_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(NOTIFY_LOWER_THAN_WARNING_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + NOTIFY_LOWER_THAN_WARNING_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getOkBtnInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(OK_BTN_ON_SETTINGS_FORM));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(OK_BTN_ON_SETTINGS_FORM));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + OK_BTN_ON_SETTINGS_FORM + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getCancelBtnInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CANCEL_BTN_ON_SETTINGS_FORM));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(CANCEL_BTN_ON_SETTINGS_FORM));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + CANCEL_BTN_ON_SETTINGS_FORM + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getOkBtnOnDeactivateBotDialog() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(OK_BTN_ON_DEACTIVATE_BOT_DIALOG));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(OK_BTN_ON_DEACTIVATE_BOT_DIALOG));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + OK_BTN_ON_DEACTIVATE_BOT_DIALOG + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getCloseBotDetailsModalScreenBtn() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CLOSE_BTN_ON_BOT_DETAILS));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(CLOSE_BTN_ON_BOT_DETAILS));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + CLOSE_BTN_ON_BOT_DETAILS + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static int getCreatedByMeCheckboxCheckedCountByPosition(int position) {
        List<WebElement> list1;
        List<WebElement> list2;
        list1 = getBotCardByPosition(position).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE));

        list2 = list1.get(0).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE_CHECKED));
        if (list2.size() == 0) {
            return 0;
        }
        return list2.size();
    }

    public static int getCreatedByMeCheckboxUncheckedCountByPosition(int position) {
        List<WebElement> list1;
        List<WebElement> list2;
        list1 = getBotCardByPosition(position).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE));

        list2 = list1.get(0).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE_UNCHECKED));
        if (list2.size() == 0) {
            return 0;
        }
        return list2.size();
    }

    public static int getCreatedByMeCheckboxCheckedCountByPositionFromVariable() {
        List<WebElement> list1;
        List<WebElement> list2;
        list1 = getBotCardByPosition(BOT_POSITION_IN_LIST).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE));

        list2 = list1.get(0).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE_CHECKED));
        if (list2.size() == 0) {
            return 0;
        }
        return list2.size();
    }

    public static int getCreatedByMeCheckboxUncheckedCountByPositionFromVariable() {
        List<WebElement> list1;
        List<WebElement> list2;
        list1 = getBotCardByPosition(BOT_POSITION_IN_LIST).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE));

        list2 = list1.get(0).findElements(By.xpath(INVESTOR_STORE_TABLE_BOT_TOGGLE_UNCHECKED));
        if (list2.size() == 0) {
            return 0;
        }
        return list2.size();
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnViewStoreBtnOnInvestorPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getViewStoreBtn());
            PrizmaDriver.waitToBeClickable(getViewStoreBtn());
            getViewStoreBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnUserAgreementOpenBtnOnInvestorPage() {
        PrizmaUtils.scrollToElementOnPage(getUserAgreementOpenBtnOnInvestorPage());
        PrizmaDriver.waitToBeClickable(getUserAgreementOpenBtnOnInvestorPage());
        getUserAgreementOpenBtnOnInvestorPage().click();

        try {
            PrizmaDriver.waitDriver(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Open User Agreement btn not open User Agreement dialog. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUserAgreementCloseBtnOnInvestorPage() {
        PrizmaUtils.scrollToElementOnPage(getUserAgreementCloseBtnOnInvestorPage());
        PrizmaDriver.waitToBeClickable(getUserAgreementCloseBtnOnInvestorPage());
        getUserAgreementCloseBtnOnInvestorPage().click();

        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on User Agreement dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void clickOnUserAgreementToggleOnInvestorPageToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getUserAgreementToggleOnInvestorPage().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUserAgreementToggleOnInvestorPageToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getUserAgreementToggleOnInvestorPage().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStartInvestingBtnOnInvestorPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getStartInvestingBtnOnInvestorPage());
            PrizmaDriver.waitToBeClickable(getStartInvestingBtnOnInvestorPage());
            getStartInvestingBtnOnInvestorPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnBotOnInvestorPageToOpenBotDetailsByName(String name) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            PrizmaUtils.scrollToElement(getInvestorMainTableNameLabelByName(name));
            getInvestorMainTableNameLabelByName(name).click();
        } catch (Throwable ex) {
            //
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCloseBotDetailsModalScreenBtn() {

        try {
            getCloseBotDetailsModalScreenBtn().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getCloseBotDetailsModalScreenBtn());
            getCloseBotDetailsModalScreenBtn().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnSettingsBtn(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getSettingsBtn(position).click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStartStopBtn(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getStartStopBtn(position).click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnDeleteBotButton(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getDeleteBtn(position).click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getDeleteBtn(position));
            getDeleteBtn(position).click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnOkBtnInSettingsScreen() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getOkBtnInSettingsForm().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCancelBtnInSettingsScreen() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getCancelBtnInSettingsForm().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotOkBtnOnDeactivateBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getOkBtnOnDeactivateBotDialog().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getOkBtnOnDeactivateBotDialog());
            getOkBtnOnDeactivateBotDialog().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */

    public static float getTotalReturn() {
        try {
            return Float.parseFloat(getTotalReturnLabel().getText().substring(4));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getTotalInvested() {
        try {
            return Float.parseFloat(getTotalInvestedLabel().getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getTotalAssets() {
        try {
            return Float.parseFloat(getTotalAssetsLabel().getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getAvatarURLInBotsList(int position) {
        try {
            return getAvatar(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotNameLabelFromInvestorTableByPosition(int position) {
        try {
            return getBotNameLabelByPosition(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotNameFromInvestorTableBotNameLabelByPositionFromVariable() {
        try {
            return getBotNameLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotInfoFromInvestorTableBotNameLabelByPosition(int position) {
        try {
            return getBotInfoLabelByPosition(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotInfoFromInvestorTableBotNameLabelByPositionFromVariable() {
        try {
            return getBotInfoLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getExchangeInBotsList(int position) {
        try {
            return getExchangeLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getDateOfActivationInBotsList(int position) {
        try {
            return getSubscriptionDateLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getRateInBotsList(int position) {
        try {
            return getRateFeeLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getStateInBotsList(int position) {
        try {
            return getStateLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBotProfitUsdInBotsList(int position) {
        try {
            return Float.parseFloat(getBotProfitLabel(position).getText().replace(" $ ",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getSumOfAllBotsProfit() {
        try {
            List<Float> profitsList = profitWebElementsToFloat(getBotProfitsList());
            return floatListSum(profitsList);
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getBotPerformanceInBotsList(int position) {
        try {
            //String performance = getExitAmountLabel().getText();
            //return Float.parseFloat(performance.substring(0;performance.length()-1));
            return getBotPerformanceLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBotInvestedAmountInBotsList(int position) {
        try {
            return Float.parseFloat(getInvestedAmountLabel(position).getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getSumOfAllBotsInvestedAmount() {
        try {
            List<Float> investedAmountsList = amountWebElementsToFloat(getBotInvestedAmountList());
            return floatListSum(investedAmountsList);
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getExitAmountInBotsList(int position) {
        try {
            return Float.parseFloat(getExitAmountLabel(position).getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getCurrentAssetsInStablecoinInBotsList(int position) {
        try {
            String currentAssets = getAssetsStableCoinLabel(position).getText();
            if (currentAssets.contains("USDT")) {
                return Float.parseFloat(currentAssets.replace(" USDT", ""));
            } else {
                return Float.parseFloat(currentAssets.replace(" USD", ""));
            }
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getCurrentSymbolAssetsInBotsList(int position) {
        try {
            return getBoughtSymbolAssetsLabel(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getCurrentAssetsInUsdInBotsList(int position) {
        try {
            return Float.parseFloat(getBoughtSymbolInUSDAssetsLabel(position).getText().replace("($ ", "")
                    .replace(")", ""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getBotTotalAssetsUsdInBotsList(int position) {
        try {
            return Float.parseFloat(getBotTotalAssetsLabel(position).getText().replace("$ ",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getSumOfAllBotsTotalAssetsUsd() {
        try {
            List<Float> totalAssetsList = amountWebElementsToFloat(getBotTotalAssetsList());
            return floatListSum(totalAssetsList);
        } catch (Throwable ex) {
            return 0;
        }
    }

        /***
     * Asserts methods
     */
    public static void saveBotFromPositionOneForFutureTests() {
        BOT_NAME = "";
        BOT_NAME = getBotNameLabelFromInvestorTableByPosition(1);
    }

    public static void saveBotFromPositionXForFutureTests(int position) {
        BOT_NAME = "";
        BOT_NAME = getBotNameLabelFromInvestorTableByPosition(position);
    }

    public static void verifyThatBotPresentInListOfBotsOnInvestorPage(String botName, String fileName) {
        boolean flag = false;

        int count = getBotsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameLabelFromInvestorTableByPosition(i);
            if (name.contains(botName)) {
                flag = true;
                BOT_POSITION_IN_LIST = i;
                break;
            }
        }

        if (!flag) {
            PrizmaDriver.getScreenshot(fileName);
        }
    }

    public static void verifyThatBotPresentInListOfBotsOnInvestorPage(String botName) {
        boolean flag = false;

        int count = getBotsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameLabelFromInvestorTableByPosition(i);
            if (name.contains(botName)) {
                flag = true;
                BOT_POSITION_IN_LIST = i;
                break;
            }
        }

        if (!flag) {
            PrizmaDriver.getScreenshot(botName);
        }
    }

    public static void verifyThatBotFromVariablePresentInListOfBotsOnInvestorPage() {
        boolean flag = false;

        int count = getBotsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameLabelFromInvestorTableByPosition(i);
            if (name.contains(BOT_NAME)) {
                flag = true;
                BOT_POSITION_IN_LIST = i;
                break;
            }
        }

        if (!flag) {
            Assert.fail("Looks like that something when wrong as bot was not present in bots list on Investor page when should be there. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNotPresentInListOfBotsOnInvestorPage(String botName, String fileName) {
        boolean flag = false;

        int count = getBotsCount();
        if (count>0){
            for (int i = 1; i <= count; i++) {
                String name = getBotNameLabelFromInvestorTableByPosition(i);
                if (name.contains(botName)) {
                    flag = true;
                    BOT_POSITION_IN_LIST = i;
                    break;
                }
            }

            if (flag) {
                PrizmaDriver.getScreenshot(fileName);
            }
        }
    }

    public static void verifyThatBotNotPresentInListOfBotsOnInvestorPage(String botName) {
        boolean flag = false;

        int count = getBotsCount();
        if (count>0){
            for (int i = 1; i <= count; i++) {
                String name = getBotNameLabelFromInvestorTableByPosition(i);
                if (name.contains(botName)) {
                    flag = true;
                    BOT_POSITION_IN_LIST = i;
                    break;
                }
            }

            if (flag) {
                PrizmaDriver.getScreenshot(botName);
            }
        }
    }

    public static void verifyThatBotFromVariableNotPresentInListOfBotsOnInvestorPage(String fileName) {
        boolean flag = false;

        int count = getBotsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameLabelFromInvestorTableByPosition(i);
            if (name.contains(BOT_NAME)) {
                flag = true;
                BOT_POSITION_IN_LIST = i;
                break;
            }
        }

        if (flag) {
            PrizmaDriver.getScreenshot(fileName);
        }
    }

    public static void verifyThatBotToggleForBotOnPositionXIsEnabled(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxCheckedCountByPosition(position);

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as bot toggle was not enabled when should be for bot on position: " + position + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotToggleForBotOnPositionXIsDisabled(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxUncheckedCountByPosition(position);

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as bot toggle was not disabled when should be for bot on position: " + position + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotToggleForBotOnPositionFromVariableIsEnabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxCheckedCountByPositionFromVariable();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as bot toggle was not enabled when should be for bot on position: " + BOT_POSITION_IN_LIST + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotToggleForBotOnPositionFromVariableIsDisabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxUncheckedCountByPositionFromVariable();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as bot toggle was not disabled when should be for bot on position: " + BOT_POSITION_IN_LIST + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameInInvestorTableByPosition(String botInfo, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotInfoFromInvestorTableBotNameLabelByPosition(position), containsString(botInfo));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position " + position + " was: " + getBotInfoFromInvestorTableBotNameLabelByPosition(position) + " but expected result should be: " + botInfo + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameInvestorTableByPositionByPositionFromVariable(String botInfo) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotInfoFromInvestorTableBotNameLabelByPositionFromVariable(), containsString(botInfo));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position " + BOT_POSITION_IN_LIST + " was: " + getBotInfoFromInvestorTableBotNameLabelByPositionFromVariable() + " but expected result should be: " + botInfo + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserRedirectFromInvestorPageToStorePageAfterClickOnViewStoreBtn() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(STORE_LABEL), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user from Investor page not redirect to Store page after click on View Store btn. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatInvestorSplashScreenPresentOnInvestorPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(START_INVESTING_BTN_ON_INVESTOR_PAGE), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user open Investor page and was not land on Investor Splash Screen but should. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatInvestorSplashScreenNotPresentOnInvestorPage() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_INVESTOR_PAGE), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as user open Investor page and was land on Investor Splash Screen but should be no Investor Splash Screen as user has correct role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatTotalReturnIsCorrect() {

        Assert.assertEquals(getSumOfAllBotsProfit(), getTotalReturn(), 0);

    }

    public static void verifyThatTotalInvestedAmountIsCorrect() {

        Assert.assertEquals(getSumOfAllBotsInvestedAmount(), getTotalInvested(), 0);

    }

    public static void verifyThatTotalAssetsIsCorrect() {

        Assert.assertEquals(getSumOfAllBotsTotalAssetsUsd(), getTotalAssets(), 0);

    }

    public static void verifyThatBotAvatarIsCorrect(int position) {

        assertNotNull(getAvatarURLInBotsList(position));

    }

    public static void verifyThatBotExchangeIsCorrect(String exchange, int position) {

        Assert.assertEquals(exchange, getExchangeInBotsList(position));

    }

    public static void verifyThatBotActivationDateIsCorrect(int position) {

        assertNotNull(getDateOfActivationInBotsList(position));

    }

    public static void verifyThatBotRateCorrect(int position) {

        assertNotNull(getRateInBotsList(position));

    }

    public static void verifyThatStateIsCorrect(String state, int position) {

        Assert.assertEquals(state, getStateInBotsList(position));

    }

    public static void verifyThatBotProfitEqualsProfitInBotsDetails(String botName, int position) {

        float actualResult = getBotProfitUsdInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getBotProfitUsd(), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatBotPerformanceEqualsPerformanceInBotsDetails(String botName, int position) {

        String actualResult = getBotPerformanceInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getBotPerformance(), actualResult);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatBotInvestedAmountEqualsAmountInBotsDetails(String botName, int position) {

        float actualResult = getBotInvestedAmountInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getBotInvestedAmount(), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatExitAmountEqualsAmountInBotsDetails(String botName, int position) {

        float actualResult = getExitAmountInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getExitAmount(), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatCurrentAssetsInStablecoinEqualsAssetsInBotsDetails(String botName, int position) {

        float actualResult = getCurrentAssetsInStablecoinInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getCurrentAssetsInStablecoin(), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatBoughtSymbolAssetsEqualsAssetsInBotsDetails(String botName, int position) {

        String actualResult = getCurrentSymbolAssetsInBotsList(position);
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(getBoughtSymbolAssets(), actualResult);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }

    public static void verifyThatBoughtSymbolAssetsInUsdEqualsAssetsInBotsDetails(String botName, int position) {

        float actualResult = Math.round(getCurrentAssetsInUsdInBotsList(position));
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(Math.round(getBoughtSymbolAssetsInUsd()), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }
    public static void verifyThatTotalAssetsAssetsInUsdEqualsAssetsInBotsDetails(String botName, int position) {

        float actualResult = Math.round(getBotTotalAssetsUsdInBotsList(position));
        clickOnBotOnInvestorPageToOpenBotDetailsByName(botName);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        Assert.assertEquals(Math.round(getBotTotalAssets()), actualResult,0);
        clickOnCloseBotDetailsModalScreenBtn();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

    }
}

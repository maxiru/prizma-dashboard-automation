package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.List;

import prizma.maxiru.framework.common.*;


import static app.PrizmaHomePage.clickOnCreatorBtnOnHomePage;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class PrizmaCreatorPage {

    //Elements with XPath, Tag and CCS selector
    public static final String UPLOAD_ANOTHER_BOT_BTN = "//button[text()='Upload Another Bot']";
    public static final String UPLOAD_BOT_BTN = "//button[contains(text(),'Upload bot')]";
    public static final String BOT_NAME_INPUT = "//input[@type='text']";
    public static final String BOT_RATE_INPUT = ".bot-upload__input-commission input";
    public static final String BOT_DESCRIPTION_INPUT = ".bot-upload__input-description textarea";
    public static final String BOT_NAME_LIMIT_ERROR_LABEL = "//span[contains(@class,'input-text__warning')]";
    public static final String BOT_NAME_ERROR_LABEL = "//span[contains(@class,'input-text__warning')]";
    public static final String BOT_NAME_ALREADY_EXIST_LABEL = "//span[contains(@class,'input-text__warning')]";
    public static final String BOT_NAME_VALIDATION_FAILED_LABEL = "//span[contains(@class,'input-text__warning')]";
    public static final String BOT_UPLOAD_NO_PERMISSION_LABEL = "//span[contains(@class,'input-text__warning')]";
    public static final String BOT_UPLOAD_BTN = "//input[@type='file']";
    public static final String BOT_UPLOAD_MANDATORY_LABEL = "//span[contains(@class,'input-file__warning')]";
    public static final String BOT_UPLOAD_INVALID_FILE_LABEL = "//span[contains(@class,'input-file__warning')]";
    public static final String BOT_UPLOAD_FILE_SIZE_LABEL = "//span[contains(@class,'input-file__warning')]";
    public static final String PRIVATE_BOT_CHECKBOX = "//label[contains(@class,'input-checkbox bot-upload__checkbox-private')]";
    public static final String BOT_UPLOAD_CONFIRM_BTN = "//button[text()='Upload']";
    public static final String BOT_UPLOAD_CANCEL_BTN = "//button[text()='Cancel']";
    public static final String DOWNLOAD_PRIZMA_BTN = "//a[text()='Download Prizma SDK']";
    public static final String NEXT_PAYOUT_EARNED_LABEL = ".creator-stats div:nth-child(4) .stat-info__value";
    public static final String TOTAL_EARNED_LABEL = ".creator-stats > div > div:nth-child(1) > .stat-info__value";
    public static final String TOTAL_INVESTORS_COUNT_LABEL = ".creator-stats > div > div:nth-child(2) > .stat-info__value";
    public static final String BOTS_UPLOAD_COUNT_LABEL = "div:nth-child(3) .stat-info__value.h2";
    public static final String BOT_NAME_LABEL = ".bot-row__name";
    public static final String BOT_VERSION_LABEL = "//div[@class='bot-cell__version']";
    public static final String BOT_STATUS_LABEL = ".bot-row__status";
    public static final String BOT_STATUS_LABEL_CSS = ".bot-row__status";
    public static final String DELETE_BTN = ".bot-row__manage > span:nth-child(3)";
    public static final String DOWNLOAD_BTN = ".bot-row__manage > button:nth-child(2)";
    public static final String PRIVATE_CHECKBOX = ".bot-row__manage > label:nth-child(1)";
    public static final String BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN = ".bot-row__error-details";
    public static final String INVESTORS_COUNT_LABEL = ".bots__list div:nth-child(4) div:nth-child(2) .bot-cell__value";
    public static final String PROFIT_ALL_TIME_LABEL = "//div[@class='bot-cell__profit']";
    public static final String PERFORMANCE_ALL_TIME_LABEL = "//div[contains(@class,'bot-cell__performance')]";
    public static final String PROFIT_LAST_THREE_MONTH_LABEL = "//div[@class='bot-cell__profit']";
    public static final String PERFORMANCE_LAST_THREE_MONTH_LABEL = "//div[contains(@class,'bot-cell__performance')]";
    public static final String PROFIT_SINCE_LAST_PAYOUT_LABEL = "//div[@class='bot-cell__profit']";
    public static final String PERFORMANCE_SINCE_LAST_PAYOUT_LABEL = "//div[contains(@class,'bot-cell__performance')]";
    public static final String BOTS_EMPTY_LABEL = ".bots__list__empty";
    public static final String USER_AGREEMENT_OPEN_BTN_ON_CREATOR_PAGE = "//span[text()='User Agreement']";
    public static final String USER_AGREEMENT_CLOSE_BTN_ON_CREATOR_PAGE = ".modal-new__close-button";
    public static final String USER_AGREEMENT_TOGGLE_ON_CREATOR_PAGE = "//label[contains(@class,'input-checkbox take-agree__checkbox')]";
    public static final String START_CREATING_BTN_ON_CREATOR_PAGE = "//button[text()='Start Creating']";

    //Other elements
    public static String BOT_NAME_LIMIT_ERROR_MESSAGE = "You enter name should contains from 3 to 20 latin letters, numbers or non-repeating symbols";
    public static String BOT_NAME_ERROR_MESSAGE = "You enter name should contains from 3 to 20 latin letters, numbers or non-repeating symbols";
    public static String BOT_NAME_VALIDATION_FAILED_ERROR_MESSAGE = "The bot name can consist of only Latin laters, numbers, and symbols";
    public static String BOT_NAME_ALREADY_EXIST_ERROR_MESSAGE = "Bot with this name already exists.";
    public static String BOT_UPLOAD_MANDATORY_ERROR_MESSAGE = "The file is required";
    public static String BOT_UPLOAD_INVALID_FILE_ERROR_MESSAGE = "The file you specified should be a zip file";
    public static String BOT_UPLOAD_FILE_SIZE_ERROR_MESSAGE = "The file you specified should has size not great 100 Mb";
    public static String BOT_UPLOAD_NO_PERMISSION_ERROR_MESSAGE = "You do not have permission to perform this action.";
    public static String BOT_NAME = "";
    public static int CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = 0;

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getUploadAnotherBotBtn() {
        return getElementByXpath(UPLOAD_ANOTHER_BOT_BTN);
    }

    public static WebElement getUploadBotBtn() {
        return getElementByXpath(UPLOAD_BOT_BTN);
    }

    public static WebElement getBotNameInput() {
        return getElementByXpath(BOT_NAME_INPUT);
    }

    public static WebElement getBotRateInput() {
        return getElementByCssSelector(BOT_RATE_INPUT);
    }

    public static WebElement getBotDescriptionInput() {
        return getElementByCssSelector(BOT_DESCRIPTION_INPUT);
    }


    public static WebElement getBotNameLimitErrorLabel() {
        return getElementByXpath(BOT_NAME_LIMIT_ERROR_LABEL);
    }

    public static WebElement getBotNameErrorLabel() {
        return getElementByXpath(BOT_NAME_ERROR_LABEL);
    }

    public static WebElement getBotNameAlreadyExistErrorLabel() {
        return getElementByXpath(BOT_NAME_ALREADY_EXIST_LABEL);
    }

    public static WebElement getBotNameValidationFailedErrorLabel() {
        return getElementByXpath(BOT_NAME_VALIDATION_FAILED_LABEL);
    }

    public static WebElement getBotUploadMandatoryErrorLabel() {
        return getElementByXpath(BOT_UPLOAD_MANDATORY_LABEL);
    }

    public static WebElement getBotUploadInvalidFileErrorLabel() {
        return getElementByXpath(BOT_UPLOAD_INVALID_FILE_LABEL);
    }

    public static WebElement getBotUploadNoPermissionErrorLabel() {
        return getElementByXpath(BOT_UPLOAD_NO_PERMISSION_LABEL);
    }

    public static WebElement getBotUploadFileSizeErrorLabel() {
        return getElementByXpath(BOT_UPLOAD_FILE_SIZE_LABEL);
    }

    public static WebElement getPrivateBotCheckbox() {
        return getElementByXpath(PRIVATE_BOT_CHECKBOX);
    }

    public static WebElement getBotUploadConfirmBtn() {
        return getElementByXpath(BOT_UPLOAD_CONFIRM_BTN);
    }

    public static WebElement getBotUploadCancelBtn() {
        return getElementByXpath(BOT_UPLOAD_CANCEL_BTN);
    }

    public static WebElement getDownloadPrizmaBtn() {
        return getElementByXpath(DOWNLOAD_PRIZMA_BTN);
    }

    public static WebElement getNextPayoutEarnedLabel() {
        return getElementByCssSelector(NEXT_PAYOUT_EARNED_LABEL);
    }

    public static WebElement getTotalEarnedLabel() {
        return getElementByCssSelector(TOTAL_EARNED_LABEL);
    }

    public static WebElement getTotalInvestorsCountLabel() {
        return getElementByCssSelector(TOTAL_INVESTORS_COUNT_LABEL);
    }

    public static WebElement getBotsUploadCountLabel() {
        return getElementByCssSelector(BOTS_UPLOAD_COUNT_LABEL);
    }

    public static WebElement getBotNameLabelInUploadedBotsSection(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_NAME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_NAME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_NAME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_NAME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static int getBotNameLabelInUploadedBotsCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_NAME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_NAME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_NAME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_NAME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.size();
        }
    }

    public static WebElement getBotVersionLabelInUploadedBotsSection(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOT_VERSION_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_VERSION_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOT_VERSION_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_VERSION_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBotStatusLabelInUploadedBotsSection(int position) {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(BOT_STATUS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_STATUS_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(BOT_STATUS_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + BOT_STATUS_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBotStatusLabelInUploadedBotsSection(String name) {
        WebElement webElement;
        WebElement statusElement = null;

        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.xpath("//*[text()[contains(.,'" + name + "')]]/parent::div"));
            if (webElement.isDisplayed()) {
                statusElement = webElement.findElement(By.cssSelector(BOT_STATUS_LABEL_CSS));
            } else {
                Assert.fail("Looks like that something when wrong as List of elements with locator: //*[text()[contains(.,'" + name + "')]]/parent::div is empty. Check video and logs, or something was changed on DD side.");
            }
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.xpath("//*[text()[contains(.,'" + name + "')]]/parent::div"));
            if (webElement.isDisplayed()) {
                statusElement = webElement.findElement(By.cssSelector(BOT_STATUS_LABEL_CSS));
            } else {
                Assert.fail("Looks like that something when wrong as List of elements with locator: //*[text()[contains(.,'" + name + "')]]/parent::div is empty. Check video and logs, or something was changed on DD side.");
            }
        }
        return statusElement;
    }

    public static WebElement getDeleteBtn() {
        return getElementByCssSelector(DELETE_BTN);
    }

    public static WebElement getDownloadBtn() {

        return getElementByCssSelector(DOWNLOAD_BTN);
    }

    public static WebElement getPrivateCheckbox() {

        return getElementByCssSelector(PRIVATE_CHECKBOX);
    }

    public static WebElement getBotDetailsViewErrorDetailsDialogBtn() {
        return getElementByCssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN);
    }

    public static WebElement getInvestorsCountLabelInUploadedBotsSection(int position) {

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INVESTORS_COUNT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTORS_COUNT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(INVESTORS_COUNT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + INVESTORS_COUNT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(position - 1);
        }
    }

    public static WebElement getBotsEmptyLabelInUploadedBotsSection() {
        return getElementByCssSelector(BOTS_EMPTY_LABEL);
    }

    public static WebElement getProfitAllTimeLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 0;
                break;
            case 2:
                change_position = 3;
                break;
            case 3:
                change_position = 6;
                break;
            case 4:
                change_position = 9;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PROFIT_ALL_TIME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_ALL_TIME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PROFIT_ALL_TIME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_ALL_TIME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getProfitLastThreeMonthLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 1;
                break;
            case 2:
                change_position = 4;
                break;
            case 3:
                change_position = 7;
                break;
            case 4:
                change_position = 10;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PROFIT_LAST_THREE_MONTH_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_LAST_THREE_MONTH_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PROFIT_LAST_THREE_MONTH_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_LAST_THREE_MONTH_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getProfitSinceLastPayoutLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 2;
                break;
            case 2:
                change_position = 5;
                break;
            case 3:
                change_position = 8;
                break;
            case 4:
                change_position = 11;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PROFIT_SINCE_LAST_PAYOUT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_SINCE_LAST_PAYOUT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PROFIT_SINCE_LAST_PAYOUT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PROFIT_SINCE_LAST_PAYOUT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getPerformanceAllTimeLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 0;
                break;
            case 2:
                change_position = 3;
                break;
            case 3:
                change_position = 6;
                break;
            case 4:
                change_position = 9;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PERFORMANCE_ALL_TIME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_ALL_TIME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PERFORMANCE_ALL_TIME_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_ALL_TIME_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getPerformanceLastThreeMonthLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 1;
                break;
            case 2:
                change_position = 4;
                break;
            case 3:
                change_position = 7;
                break;
            case 4:
                change_position = 10;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PERFORMANCE_LAST_THREE_MONTH_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_LAST_THREE_MONTH_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PERFORMANCE_LAST_THREE_MONTH_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_LAST_THREE_MONTH_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getPerformanceSinceLastPayoutLabelInUploadedBotsSection(int position) {
        int change_position = 0;
        switch (position) {
            case 1:
                change_position = 2;
                break;
            case 2:
                change_position = 5;
                break;
            case 3:
                change_position = 8;
                break;
            case 4:
                change_position = 11;
                break;
        }

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(PERFORMANCE_SINCE_LAST_PAYOUT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_SINCE_LAST_PAYOUT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(PERFORMANCE_SINCE_LAST_PAYOUT_LABEL));
            if (list.size() == 0) {
                Assert.fail("Looks like that something when wrong as List of elements with locator: " + PERFORMANCE_SINCE_LAST_PAYOUT_LABEL + " is empty. Check video and logs, or something was changed on DD side.");
            }
            return list.get(change_position);
        }
    }

    public static WebElement getUserAgreementOpenBtnOnCreatorPage() {
        return getElementByXpath(USER_AGREEMENT_OPEN_BTN_ON_CREATOR_PAGE);
    }

    public static WebElement getUserAgreementCloseBtnOnCreatorPage() {
        return getElementByCssSelector(USER_AGREEMENT_CLOSE_BTN_ON_CREATOR_PAGE);
    }

    public static WebElement getUserAgreementToggleOnCreatorPage() {
        return getElementByXpath(USER_AGREEMENT_TOGGLE_ON_CREATOR_PAGE);
    }

    public static WebElement getStartCreatingBtnOnCreatorPage() {
        return getElementByXpath(START_CREATING_BTN_ON_CREATOR_PAGE);
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnUploadAnotherBotBtnOnCreatorPage() {
        PrizmaUtils.scrollToElementOnPage(getUploadAnotherBotBtn());
        PrizmaDriver.waitToBeClickable(getUploadAnotherBotBtn());
        getUploadAnotherBotBtn().click();

        try {
            PrizmaDriver.waitDriver(By.xpath(BOT_UPLOAD_CANCEL_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Open Upload bot dialog -> Upload bot dialog not open. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUploadBotBtnOnCreatorPage() {
        PrizmaUtils.scrollToElementOnPage(getUploadBotBtn());
        PrizmaDriver.waitToBeClickable(getUploadBotBtn());
        getUploadBotBtn().click();

        try {
            PrizmaDriver.waitDriver(By.xpath(BOT_UPLOAD_CANCEL_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Open Upload bot dialog -> Upload bot dialog not open. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnPrivateBotCheckboxOnUploadBotDialogToDisable() {
        try {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

            getPrivateBotCheckbox().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable() {
        try {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

            getPrivateBotCheckbox().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnBotUploadConfirmBtnOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaUtils.scrollToElementOnPage(getBotUploadConfirmBtn());
            PrizmaDriver.waitToBeClickable(getBotUploadConfirmBtn());
            getBotUploadConfirmBtn().click();
        } catch (Throwable ex) {
            try {
                PrizmaDriver.waitToBeClickable(getBotUploadConfirmBtn());
                getBotUploadConfirmBtn().click();
            } catch (Throwable ex2) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            }
        }
    }

    public static void clickOnBotUploadCancelBtnOnUploadBotDialog() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotUploadCancelBtn());
            PrizmaDriver.waitToBeClickable(getBotUploadCancelBtn());
            getBotUploadCancelBtn().click();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnBotNameInput() {
        PrizmaUtils.scrollToElementOnPage(getBotNameInput());
        PrizmaDriver.waitToBeClickable(getBotNameInput());
        getBotNameInput().click();
    }

    public static void clickOnBotRateInput() {
        PrizmaUtils.scrollToElementOnPage(getBotRateInput());
        PrizmaDriver.waitToBeClickable(getBotRateInput());
        getBotRateInput().click();
    }

    public static void clickOnBotDescriptionInput() {
        PrizmaUtils.scrollToElementOnPage(getBotDescriptionInput());
        PrizmaDriver.waitToBeClickable(getBotDescriptionInput());
        getBotDescriptionInput().click();
    }

    public static void clickOnBotNameInUnloadBotsSectionByPosition(int position) {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotNameLabelInUploadedBotsSection(position));
            PrizmaDriver.waitToBeClickable(getBotNameLabelInUploadedBotsSection(position));
            getBotNameLabelInUploadedBotsSection(position).click();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnBotNameInUnloadBotsSectionByPositionFromVariable() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotNameLabelInUploadedBotsSection(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS));
            PrizmaDriver.waitToBeClickable(getBotNameLabelInUploadedBotsSection(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS));
            getBotNameLabelInUploadedBotsSection(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).click();
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnDownloadPrizmaBtnOnCreatorPage(String fileName, int timer) {
        try {
            PrizmaUtils.scrollToElementOnPage(getDownloadPrizmaBtn());
            PrizmaDriver.waitToBeClickable(getDownloadPrizmaBtn());
            getDownloadPrizmaBtn().click();

            PrizmaUtils.waitTillFileDownload(fileName, timer);
        } catch (Throwable ex) {
            PrizmaUtils.waitTillFileDownload(fileName, timer);
        }
    }

    public static void clickOnDeleteBtn() {
        try {
            PrizmaUtils.scrollToElementOnPage(getDeleteBtn());
            PrizmaDriver.waitToBeClickable(getDeleteBtn());
            getDeleteBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnDownloadBtn(int timer) {
        try {
            PrizmaUtils.scrollToElementOnPage(getDownloadBtn());
            getDownloadBtn().click();

            PrizmaUtils.waitTillFileDownload(BOT_NAME, timer);
        } catch (Throwable ex) {
            PrizmaUtils.waitTillFileDownload(BOT_NAME, timer);
        }
    }

    public static void clickOnPrivateCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getPrivateCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnPrivateCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getPrivateCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to upload new invite image
    public static void uploadNewBotZipFile(String fileName) {
        if (PrizmaDriver.isSelenoid()) {
            List<WebElement> list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(BOT_UPLOAD_BTN));
            WebElement input = list.get(0);
            PrizmaUtils.makeUploadButtonVisible(input);
            String path = System.getProperty("user.dir") + "/files/" + fileName;
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            input.sendKeys(path);
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } else {
            List<WebElement> list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(BOT_UPLOAD_BTN));
            WebElement input = list.get(0);
            PrizmaUtils.makeUploadButtonVisible(input);
            String path = System.getProperty("user.dir") + "/files/" + fileName;
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
            input.sendKeys(path);
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void clickOnUserAgreementOpenBtnOnCreatorPage() {
        PrizmaUtils.scrollToElementOnPage(getUserAgreementOpenBtnOnCreatorPage());
        PrizmaDriver.waitToBeClickable(getUserAgreementOpenBtnOnCreatorPage());
        getUserAgreementOpenBtnOnCreatorPage().click();

        try {
            PrizmaDriver.waitDriver(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_CREATOR_PAGE), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Open User Agreement btn not open User Agreement dialog. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUserAgreementCloseBtnOnCreatorPage() {
        PrizmaUtils.scrollToElementOnPage(getUserAgreementCloseBtnOnCreatorPage());
        PrizmaDriver.waitToBeClickable(getUserAgreementCloseBtnOnCreatorPage());
        getUserAgreementCloseBtnOnCreatorPage().click();

        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(USER_AGREEMENT_CLOSE_BTN_ON_CREATOR_PAGE), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on User Agreement dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void clickOnUserAgreementToggleOnCreatorPageToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getUserAgreementToggleOnCreatorPage().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUserAgreementToggleOnCreatorPageToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getUserAgreementToggleOnCreatorPage().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStartCreatingBtnOnCreatorPage() {
        try {
            PrizmaUtils.scrollToElementOnPage(getStartCreatingBtnOnCreatorPage());
            PrizmaDriver.waitToBeClickable(getStartCreatingBtnOnCreatorPage());
            getStartCreatingBtnOnCreatorPage().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearBotNameInput() {
        PrizmaUtils.scrollToElementOnPage(getBotNameInput());
        PrizmaDriver.waitToBeClickable(getBotNameInput());
        getBotNameInput().clear();
    }

    public static void clearBotRateInput() {
        PrizmaUtils.scrollToElementOnPage(getBotRateInput());
        PrizmaDriver.waitToBeClickable(getBotRateInput());
        getBotRateInput().clear();
    }

    public static void clearBotDescriptionInput() {
        PrizmaUtils.scrollToElementOnPage(getBotDescriptionInput());
        PrizmaDriver.waitToBeClickable(getBotDescriptionInput());
        getBotDescriptionInput().clear();
    }

    /***
     * Methods to set the value into element
     */
    public static void setBotNameOnUploadANewBotDialog(String botName) {
        if (botName.isEmpty()) {
            clickOnBotNameInput();
            clearBotNameInput();
        } else {
            clickOnBotNameInput();
            clearBotNameInput();
            getBotNameInput().sendKeys(botName);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotRateOnUploadANewBotDialog(String botRate) {
        if (botRate.isEmpty()) {
            clickOnBotRateInput();
            clearBotRateInput();
        } else {
            clickOnBotRateInput();
            clearBotRateInput();
            getBotRateInput().sendKeys(botRate);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotDescriptionOnUploadANewBotDialog(String botDescription) {
        if (botDescription.isEmpty()) {
            clickOnBotDescriptionInput();
            clearBotDescriptionInput();
        } else {
            clickOnBotDescriptionInput();
            clearBotDescriptionInput();
            getBotDescriptionInput().sendKeys(botDescription);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotNameOnUploadANewBotDialogWhichIsAutoGenerated() {
        String botName = PrizmaUtils.generateRandomWord();
        BOT_NAME = botName;

        if (botName.isEmpty()) {
            clickOnBotNameInput();
            clearBotNameInput();
        } else {
            clickOnBotNameInput();
            clearBotNameInput();
            getBotNameInput().sendKeys(botName);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotNameOnUploadANewBotDialogWithEnter(String botName) {
        if (botName.isEmpty()) {
            clickOnBotNameInput();
            clearBotNameInput();

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getBotNameInput().sendKeys(Keys.RETURN);
            } else {
                getBotNameInput().sendKeys(Keys.ENTER);
            }
        } else {
            clickOnBotNameInput();
            clearBotNameInput();
            getBotNameInput().sendKeys(botName);

            if (!PrizmaDriver.isChromeBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                getBotNameInput().sendKeys(Keys.RETURN);
            } else {
                getBotNameInput().sendKeys(Keys.ENTER);
            }

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void deleteAllBotsFromThePage() {
        int count;

        try {
            count = getBotNameLabelInUploadedBotsCount();
        } catch (Throwable ex) {
            count = 0;
        }

        if (count != 0) {
            for (int i = 1; i <= count; i++) {
                clickOnDeleteBtn();
                PrizmaUtils.refreshPage();
                clickOnCreatorBtnOnHomePage();

                PrizmaDriver.waitFor(PrizmaConstants.LOG_OUT_TIME);
            }
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromNextPayoutEarnedLabel() {
        try {
            return getNextPayoutEarnedLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromTotalEarnedLabel() {
        try {
            return getTotalEarnedLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromTotalInvestorsCountLabel() {
        try {
            return getTotalInvestorsCountLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotsUploadCountLabel() {
        try {
            return getBotsUploadCountLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotNameLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getBotNameLabelInUploadedBotsSection(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotVersionLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getBotVersionLabelInUploadedBotsSection(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotStatusLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getBotStatusLabelInUploadedBotsSection(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotStatusLabelInUploadedBotsSectionByBotName(String name) {
        try {
            return getBotStatusLabelInUploadedBotsSection(name).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotsEmptyLabelInUploadedBotsSection() {
        try {
            return getBotsEmptyLabelInUploadedBotsSection().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromInvestorsCountLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getInvestorsCountLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitAllTimeLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getProfitAllTimeLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getProfitLastThreeMonthLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromProfitSinceLastPayoutLabelInUploadedBotsSection(int position) {
        try {
            return getProfitSinceLastPayoutLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getPerformanceAllTimeLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getPerformanceLastThreeMonthLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(int position) {
        try {
            return getPerformanceSinceLastPayoutLabelInUploadedBotsSection(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotNameLimitErrorLabel() {
        try {
            return getBotNameLimitErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotNameErrorLabel() {
        try {
            return getBotNameErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotNameAlreadyExistErrorLabel() {
        try {
            return getBotNameAlreadyExistErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotNameValidationFailedErrorLabel() {
        try {
            return getBotNameValidationFailedErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotUploadMandatoryErrorLabel() {
        try {
            return getBotUploadMandatoryErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotUploadInvalidFileErrorLabel() {
        try {
            return getBotUploadInvalidFileErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotUploadNoPermissionErrorLabel() {
        try {
            return getBotUploadNoPermissionErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromBotUploadFileSizeErrorLabel() {
        try {
            return getBotUploadFileSizeErrorLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (!flag) {
            Assert.fail("Looks like that something when wrong as bot details view error details dialog button was hidden when should be present as per user role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotDetailsViewErrorDetailsDialogBtnIsNotPresent() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN), 5);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as bot details view error details dialog button was present when should be hidden as per user role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyNextPayoutEarnedLabel(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromNextPayoutEarnedLabel(), containsString(count));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Next Payout Earned count was: " + getTextFromNextPayoutEarnedLabel() + " but expected result should be: " + count + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyTotalEarnedCountLabel(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromTotalEarnedLabel(), containsString(count));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Total Earned count was: " + getTextFromTotalEarnedLabel() + " but expected result should be: " + count + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyTotalInvestorsCountLabel(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromTotalInvestorsCountLabel(), containsString(count));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Total Investors count was: " + getTextFromTotalInvestorsCountLabel() + " but expected result should be: " + count + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotsUploadCountLabel(String count) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotsUploadCountLabel(), containsString(count));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bots upload count was: " + getTextFromBotsUploadCountLabel() + " but expected result should be: " + count + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameLabelFromVariableInUploadedBotsSectionOnPosition(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabelInUploadedBotsSectionOnPosition(position), containsString(BOT_NAME));
        } catch (Throwable ex) {
            try {
                Assert.fail("Looks like that something when wrong as Bot name on position was: " + getTextFromBotNameLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + BOT_NAME + " Check video and logs, or something was changed on Prizma DD side.");
            } catch (Throwable ex2) {
                //ignore
            }
        }
    }

    public static void verifyBotNameLabelInUploadedBotsSectionOnPosition(String botName, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabelInUploadedBotsSectionOnPosition(position), containsString(botName));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position was: " + getTextFromBotNameLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + botName + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS), containsString(botName));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position was: " + getTextFromBotNameLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS) + " but expected result should be: " + botName + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotVersionLabelInUploadedBotsSectionOnPosition(String botVersion, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotVersionLabelInUploadedBotsSectionOnPosition(position), containsString(botVersion));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot version on position was: " + getTextFromBotVersionLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + botVersion + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotBotsEmptyLabelInUploadedBotsSection(String message) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        String text = getTextFromBotsEmptyLabelInUploadedBotsSection();
        assertEquals("Looks like that something when wrong as Bot Empty label not appear but should. Check video and logs, or something was changed on Prizma DD side.",message,text);
    }

    public static void verifyBotStatusLabelInUploadedBotsSectionOnPosition(String botStatus, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotStatusLabelInUploadedBotsSectionOnPosition(position), containsString(botStatus));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot status on position was: " + getTextFromBotStatusLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + botStatus + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(String botStatus) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotStatusLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS), containsString(botStatus));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot status on position was: " + getTextFromBotStatusLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS) + " but expected result should be: " + botStatus + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(String name) {

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        int count = 0;
        int maxTries = 600;
        String botStatus;
        Boolean status = true;
        while (status) {
            botStatus = getTextFromBotStatusLabelInUploadedBotsSectionByBotName(name);
            try {
                status = false;
                assertThat(botStatus, containsString("Published"));
            } catch (Throwable ex) {
                if (botStatus.contains("failed")) {
                    Assert.fail("Looks like that something when wrong as Bot status on position was: '" + botStatus + "', but expected result should be: 'Published'. Check video and logs, or something was changed on Prizma DD side.");
                } else {
                    if (count <= maxTries) {
                        count++;
                        PrizmaDriver.waitFor(PrizmaConstants.REFRESH_WAIT_TIME);
                        PrizmaUtils.refreshPage();
                        status = true;
                    } else {
                        Assert.fail("Looks like that something when wrong as Bot status on position was: '" + botStatus + "', but expected result should be: 'Published'. Check video and logs, or something was changed on Prizma DD side.");
                    }
                }
            }
        }
    }

    public static void verifyUntilBotStatusIsFailedInUploadedBotsSectionByBotName(String name) {

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        int count = 0;
        int maxTries = 600;
        String botStatus;
        Boolean status = true;
        while (status) {
            botStatus = getTextFromBotStatusLabelInUploadedBotsSectionByBotName(name);
            try {
                status = false;
                assertThat(botStatus, containsString("Validation failed"));
            } catch (Throwable ex) {
                if (botStatus.contains("failed")) {
                    Assert.fail("Looks like that something when wrong as Bot status on position was: '" + botStatus + "', but expected result should be: 'Validation failed'. Check video and logs, or something was changed on Prizma DD side.");
                } else {
                    if (count <= maxTries) {
                        count++;
                        PrizmaDriver.waitFor(PrizmaConstants.REFRESH_WAIT_TIME);
                        PrizmaUtils.refreshPage();
                        status = true;
                    } else {
                        Assert.fail("Looks like that something when wrong as Bot status on position was: '" + botStatus + "', but expected result should be: 'Validation failed'. Check video and logs, or something was changed on Prizma DD side.");
                    }
                }
            }
        }
    }

    public static void verifyInvestorsCountLabelInUploadedBotsSectionOnPosition(String investorsCount, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromInvestorsCountLabelInUploadedBotsSectionOnPosition(position), containsString(investorsCount));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot Investor count on position was: " + getTextFromInvestorsCountLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + investorsCount + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(String investorsCount) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromInvestorsCountLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS), containsString(investorsCount));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot Investor count on position was: " + getTextFromInvestorsCountLabelInUploadedBotsSectionOnPosition(CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS) + " but expected result should be: " + investorsCount + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyProfitAllTimeLabelInUploadedBotsSectionOnPosition(String profitValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromProfitAllTimeLabelInUploadedBotsSectionOnPosition(position), containsString(profitValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot all time profit on position was: " + getTextFromProfitAllTimeLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + profitValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(String profitValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(position), containsString(profitValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot last three month profit on position was: " + getTextFromProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + profitValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyProfitSinceLastPayoutLabelInUploadedBotsSection(String profitValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromProfitSinceLastPayoutLabelInUploadedBotsSection(position), containsString(profitValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot since last payout profit on position was: " + getTextFromProfitSinceLastPayoutLabelInUploadedBotsSection(position) + " but expected result should be: " + profitValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(position), containsString(performanceValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot perfomance time on position was: " + getTextFromPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + performanceValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(position), containsString(performanceValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot last three month perfomance on position was: " + getTextFromPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + performanceValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(position), containsString(performanceValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot since last payout perfomance on position was: " + getTextFromPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(position) + " but expected result should be: " + performanceValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLimitErrorLabel(), containsString(BOT_NAME_LIMIT_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot name limit error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNameLimitErrorMessageNotAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameLimitErrorLabel(), containsString(BOT_NAME_LIMIT_ERROR_MESSAGE));
            Assert.fail("Looks like that something when wrong as bot name limit error message appear but shouldn't. Check video and logs, or something was changed on Prizma DD side.");
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void verifyThatBotNameErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameErrorLabel(), containsString(BOT_NAME_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot name error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameAlreadyExistErrorLabel(), containsString(BOT_NAME_ALREADY_EXIST_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot name already exist error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNameValidationFailedErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotNameValidationFailedErrorLabel(), containsString(BOT_NAME_VALIDATION_FAILED_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot name validation failed error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotUploadMandatoryErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotUploadMandatoryErrorLabel(), containsString(BOT_UPLOAD_MANDATORY_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot upload mandatory error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotUploadInvalidFileErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotUploadInvalidFileErrorLabel(), containsString(BOT_UPLOAD_INVALID_FILE_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot upload invalid file error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotUploadNoPermissionErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotUploadNoPermissionErrorLabel(), containsString(BOT_UPLOAD_NO_PERMISSION_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot upload no permission error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotUploadFileSizeErrorMessageAppearOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotUploadFileSizeErrorLabel(), containsString(BOT_UPLOAD_FILE_SIZE_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as bot upload file size error message not appear. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(BOT_UPLOAD_CANCEL_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Upload bot dialog close when user tried to upload new bot with incorrect data bot. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithCorrectDataBot() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(BOT_UPLOAD_CANCEL_BTN), 30);
        } catch (Throwable ex) {
            try {
                Assert.fail("Looks like that something when wrong as after click Upload bot dialog not close when user tried to upload new bot with correct data bot. Check video and logs, or something was changed on Prizma DD side.");
            } catch (Throwable ex2) {
                //ignore
            }
        }
    }

    public static void verifyThatUploadBotDialogCloseAfterClickOnCancelBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(BOT_UPLOAD_CANCEL_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on cancel btn on Upload bot dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatCreatorSplashScreenPresentOnCreatorPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(START_CREATING_BTN_ON_CREATOR_PAGE), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user open Creator page and was not land on Creator Splash Screen but should. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatCreatorSplashScreenNotPresentOnCreatorPage() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(START_CREATING_BTN_ON_CREATOR_PAGE), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as user open Creator page and was land on Creator Splash Screen but should be no Creator Splash Screen as user has correct role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void getAndStoreBotPositionOnCreatorPage(String botName) {
        int count = getBotNameLabelInUploadedBotsCount();

        for (int i = 1; i <= count; i++) {
            String name = getTextFromBotNameLabelInUploadedBotsSectionOnPosition(i);
            if (name.contains(botName)) {
                CREATOR_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = i;
                break;
            }
        }
    }

    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getElementByXpath(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.xpath(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.xpath(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }
}

package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaSigninPage.*;

public class PrizmaHomePage {

    //Elements with XPath, Tag and CCS selector
    public static final String CREATOR_BTN = ".main-menu > a:nth-child(3)";
    public static final String INVESTOR_BTN = ".main-menu > a:nth-child(1)";
    public static final String STORE_BTN = ".main-menu > a:nth-child(2)";
    public static final String ACCOUNT_BTN = ".account-button";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getCreatorBtn() { return getElementByCssSelector(CREATOR_BTN); }

    public static WebElement getInvestorBtn() {
        return getElementByCssSelector(INVESTOR_BTN);
    }

    public static WebElement getStoreBtn() {
        return getElementByCssSelector(STORE_BTN);
    }

    public static WebElement getAccountBtn() { return getElementByCssSelector(ACCOUNT_BTN); }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnCreatorBtnOnHomePage() {
        PrizmaDriver.waitFor(PrizmaConstants.LOG_OUT_TIME);
        getCreatorBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnInvestorBtnOnHomePage() {
        PrizmaDriver.waitFor(PrizmaConstants.LOG_OUT_TIME);
        getInvestorBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnStoreBtnOnHomePage() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        getStoreBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnAccountButton() {
        PrizmaDriver.waitFor(PrizmaConstants.LOG_OUT_TIME);
        getAccountBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Asserts methods
     */
    public static void verifyThatUserRedirectFromHomePageToSignInPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.xpath(SIGN_IN_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user from home page not redirect to sign in page. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }
}

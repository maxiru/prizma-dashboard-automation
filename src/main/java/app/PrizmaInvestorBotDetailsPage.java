package app;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import prizma.maxiru.framework.common.PrizmaConstants;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

import static app.PrizmaInvestorPage.*;
import static prizma.maxiru.framework.common.PrizmaUtils.*;

public class PrizmaInvestorBotDetailsPage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOT_NAME_LABEL = ".bot-subscription-details .bot-subscription-row__name";
    public static final String EXCHANGE_LABEL = ".bot-subscription-details .bot-subscription-row__info__exchange";
    public static final String SUBSCRIPTION_DATE_LABEL = ".bot-subscription-details .bot-subscription-row__info__date";
    public static final String RATE_FEE_LABEL = ".bot-subscription-details .bot-subscription-row__info__rate";
    public static final String STATE_LABEL = ".bot-subscription-details .bot-subscription-row__state";
    public static final String AVATAR_IMAGE = ".bot-subscription-details .bot-subscription-row__avatar";
    public static final String BOT_PROFIT_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(2) > div:nth-child(1) > .bot-subscription-row__characteristic__value > span:nth-child(1)";
    public static final String BOT_PERFORMANCE_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(2) > div:nth-child(1) > .bot-subscription-row__characteristic__value > span:nth-child(2)";
    public static final String INVESTED_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(2) > div:nth-child(2) > .bot-subscription-row__characteristic__value";
    public static final String EXIT_AMOUNT_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(2) > div:nth-child(3) > .bot-subscription-row__characteristic__value > span:nth-child(1)";
    public static final String ASSETS_STABLECOIN_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(3) > div:nth-child(1) > .bot-subscription-row__characteristic__value > div:nth-child(1) > .bot-subscription-row__asset_size";
    public static final String ASSETS_BOUGHT_SYMBOL_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span:nth-child(1)";
    public static final String ASSETS_BOUGHT_SYMBOL_USD_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(3) > div:nth-child(1) > .bot-subscription-row__characteristic__value > div:nth-child(2) > .bot-subscription-row__asset__amount";
    public static final String BOT_TOTAL_ASSETS_LABEL = ".bot-subscription-details > .bot-subscription-row > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)";

    public static final String SETTINGS_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(1)";
    public static final String START_STOP_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(2)";
    public static final String DELETE_BTN = ".bot-subscription-details .bot-subscription-row__manage button:nth-child(3)";

    public static final String SETTINGS_FORM = ".edit-botsubscription__form";
    public static final String CANCEL_BTN_ON_SETTINGS_FORM = ".edit-botsubscription__buttons > button:nth-child(2)";

    public static final String POSITION_BLOCK = ".position-log";
    public static final String PAIR_IN_POSITION = ".position-log-summary__pair";
    public static final String TYPE_IN_POSITION = ".position-log-summary__type";
    public static final String CURRENT_SIZE_IN_POSITION = ".position-log-summary__box .position-log-summary__size";
    public static final String BOUGHT_SYMBOL_USD_AMOUNT_IN_POSITION = ".position-log-summary > div:nth-child(2) .position-log-summary__amount";
    public static final String AVERAGE_PROFIT_IN_POSITION = ".position-log-summary > div:nth-child(3) > div:nth-child(2)";
    public static final String AVERAGE_PERFORMANCE_IN_POSITION = ".position-log-summary > div:nth-child(3) > div:nth-child(3)";
    public static final String TOTAL_PROFIT_IN_POSITION = ".position-log-summary > div:nth-child(4) > div:nth-child(2)";
    public static final String TOTAL_PERFORMANCE_IN_POSITION = ".position-log-summary > div:nth-child(4) > div:nth-child(3)";
    public static final String ORDERS_IN_POSITION = ".position-log-summary > div:nth-child(5) > div:nth-child(2)";

    public static final String ORDERS_TABLE_ROWS = ".data-table__body";
    public static final String ORDERS_TABLE_RAW = ".data-table__body__row";
    public static final String ORDERS_TABLE_FILLED_ORDER = ".data-table__body__row td:nth-child(1) path[d='M2 8l3 3 7-8']";
    public static final String ORDERS_TABLE_OPEN_ORDER = ".data-table__body__row td:nth-child(1) path[d='M7 12A5 5 0 107 2a5 5 0 000 10z']";
    public static final String ORDERS_TABLE_CANCELED_ORDER = ".data-table__body__row td:nth-child(1) path[d='']"; //TODO
    public static final String ORDERS_TABLE_REJECTED_ORDER = ".data-table__body__row td:nth-child(1) path[d='M6.018 2.532l-4.864']";
    public static final String ORDERS_TABLE_SIDE = ".data-table__body__row td:nth-child(2)";
    public static final String ORDERS_TABLE_MARKET_ORDER = ".data-table__body__row td:nth-child(3) path[d='M1 7.658h10M8 2.658l5 5-5 5']";
    public static final String ORDERS_TABLE_LIMIT_ORDER = ".data-table__body__row td:nth-child(3) path[d='M7 13.658v-8M2 1.658h10M2 8.658l5-5 5 5']";
    public static final String ORDERS_TABLE_STOP_LIMIT_ORDER = ".data-table__body__row td:nth-child(3) path[d='M4.516 1.658h4.968L13 5.174v4.968l-3.516 3.516H4.516L1 10.142V5.174l3.516-3.516z']";
    public static final String ORDERS_TABLE_DATE_ORDER = ".data-table__body__row td:nth-child(4)";
    public static final String ORDERS_TABLE_SIZE_CELL = ".data-table__body__row td:nth-child(5)";
    public static final String ORDERS_TABLE_LIMIT_PRICE_CELL = ".data-table__body__row td:nth-child(6)";
    public static final String ORDERS_TABLE_FILL_PRICE_CELL = ".data-table__body__row td:nth-child(7)";
    public static final String ORDERS_TABLE_TOTAL_CELL = ".data-table__body__row td:nth-child(8)";
    public static final String ORDERS_TABLE_PROFIT_USD_CELL = ".data-table__body__row td:nth-child(9)";
    public static final String ORDERS_TABLE_PROFIT_PERCENTAGE_CELL = ".data-table__body__row td:nth-child(10)";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotNameLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOT_NAME_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOT_NAME_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + BOT_NAME_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getAvatar() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(AVATAR_IMAGE));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(AVATAR_IMAGE));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + AVATAR_IMAGE + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getExchangeLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(EXCHANGE_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(EXCHANGE_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + EXCHANGE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getSubscriptionDateLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(SUBSCRIPTION_DATE_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(SUBSCRIPTION_DATE_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + SUBSCRIPTION_DATE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getRateFeeLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(RATE_FEE_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(RATE_FEE_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + RATE_FEE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getStateLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(STATE_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(STATE_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + STATE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBotProfitLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOT_PROFIT_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOT_PROFIT_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + BOT_PROFIT_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBotPerformanceLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOT_PERFORMANCE_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOT_PERFORMANCE_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + BOT_PERFORMANCE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getInvestedAmountLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(INVESTED_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(INVESTED_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + INVESTED_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getExitAmountLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(EXIT_AMOUNT_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(EXIT_AMOUNT_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + EXCHANGE_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getAssetsStableCoinLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(ASSETS_STABLECOIN_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(ASSETS_STABLECOIN_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + ASSETS_STABLECOIN_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBoughtSymbolAssetsLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(ASSETS_BOUGHT_SYMBOL_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(ASSETS_BOUGHT_SYMBOL_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + ASSETS_BOUGHT_SYMBOL_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBoughtSymbolInUSDAssetsLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(ASSETS_BOUGHT_SYMBOL_USD_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(ASSETS_BOUGHT_SYMBOL_USD_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + ASSETS_BOUGHT_SYMBOL_USD_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBotTotalAssetsLabel() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOT_TOTAL_ASSETS_LABEL));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOT_TOTAL_ASSETS_LABEL));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + BOT_TOTAL_ASSETS_LABEL + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getSettingsBtn() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(SETTINGS_BTN));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(SETTINGS_BTN));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + SETTINGS_BTN + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getStartStopBtn() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(START_STOP_BTN));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(START_STOP_BTN));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + START_STOP_BTN + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getDeleteBtn() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(DELETE_BTN));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(DELETE_BTN));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + DELETE_BTN + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(SETTINGS_FORM));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(SETTINGS_FORM));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + SETTINGS_FORM + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getCancelBtnInSettingsForm() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CANCEL_BTN_ON_SETTINGS_FORM));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(CANCEL_BTN_ON_SETTINGS_FORM));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + CANCEL_BTN_ON_SETTINGS_FORM + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getPositionBlock() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(POSITION_BLOCK));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(POSITION_BLOCK));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + POSITION_BLOCK + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getPairLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(PAIR_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(PAIR_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + PAIR_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getTypeLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TYPE_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TYPE_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TYPE_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getCurrentSizeLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CURRENT_SIZE_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(CURRENT_SIZE_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + CURRENT_SIZE_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getBoughtSymbolLabelInUSDInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(BOUGHT_SYMBOL_USD_AMOUNT_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(BOUGHT_SYMBOL_USD_AMOUNT_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + BOUGHT_SYMBOL_USD_AMOUNT_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getAverageProfitLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(AVERAGE_PROFIT_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(AVERAGE_PROFIT_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + AVERAGE_PROFIT_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getAveragePerformanceLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(AVERAGE_PERFORMANCE_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(AVERAGE_PERFORMANCE_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + AVERAGE_PERFORMANCE_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getTotalProfitLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TOTAL_PROFIT_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TOTAL_PROFIT_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TOTAL_PROFIT_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getTotalPerformanceLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(TOTAL_PERFORMANCE_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(TOTAL_PERFORMANCE_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + TOTAL_PERFORMANCE_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getOrdersLabelInPosition() {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(ORDERS_IN_POSITION));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(ORDERS_IN_POSITION));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + ORDERS_IN_POSITION + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getOrdersTableRows() {
        try {
            PrizmaDriver.waitDriver(By.cssSelector(ORDERS_TABLE_ROWS), 60);
        } catch (Throwable ex) {
            //ignore
        }

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ORDERS_TABLE_ROWS));
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ORDERS_TABLE_ROWS));
        }
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as List of elements with locator: " + ORDERS_TABLE_ROWS + " is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0);
    }

    public static int getOrdersCountInTable() {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_RAW));
        return list.size();
    }

    public static WebElement getFilledOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_FILLED_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static int getFilledOrdersCountInTable() {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_FILLED_ORDER));
        return list.size();
    }

    public static WebElement getOpenOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_OPEN_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getCanceledOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_CANCELED_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getRejectedOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_REJECTED_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getSideInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_SIDE));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static int getSellOrdersCountInTable() {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_SIDE));
        List<String> strings = new ArrayList<String>();
        for(WebElement e : list) {
            strings.add(e.getText());
        }

        while (strings.contains("Buy")) {
            strings.remove("Buy");
        }

        return strings.size();
    }

    public static WebElement getMarketOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_MARKET_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getLimitOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_LIMIT_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getStopLimitOrderInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_STOP_LIMIT_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getDateInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_DATE_ORDER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getBoughtSizeInTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_SIZE_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getLimitPriceInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_LIMIT_PRICE_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getFillPriceInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_FILL_PRICE_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getTotalInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_TOTAL_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static WebElement getProfitUsdInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_PROFIT_USD_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static List<WebElement> getProfitsUsdListInTable() {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_PROFIT_USD_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list;
    }

    public static WebElement getProfitPercentageInOrdersTableByPosition(int position) {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_PROFIT_PERCENTAGE_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(position - 1);
    }

    public static List<WebElement> getProfitsPercentageListInTable() {
        List<WebElement> list;
        list = getOrdersTableRows().findElements(By.cssSelector(ORDERS_TABLE_PROFIT_PERCENTAGE_CELL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Investor table bot info label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list;
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnSettingsBtn() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getSettingsBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStartStopBtn() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getStartStopBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnDeleteBotButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getDeleteBtn().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getDeleteBtn());
            getDeleteBtn().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCancelBtnInSettingsScreen() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getCancelBtnInSettingsForm().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnPositionBlock() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getPositionBlock().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getBotName() {
        try {
            return getBotNameLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getAvatarURL() {
        try {
            return getAvatar().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getExchange() {
        try {
            return getExchangeLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getDateOfActivation() {
        try {
            return getSubscriptionDateLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getRate() {
        try {
            return getRateFeeLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getState() {
        try {
            return getStateLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBotProfitUsd() {
        try {
            return Float.parseFloat(getBotProfitLabel().getText().substring(4));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getBotPerformance() {
        try {
            //String performance = getExitAmountLabel().getText();
            //return Float.parseFloat(performance.substring(0;performance.length()-1));
            return getBotPerformanceLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBotInvestedAmount() {
        try {
            return Float.parseFloat(getInvestedAmountLabel().getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getExitAmount() {
        try {
            return Float.parseFloat(getExitAmountLabel().getText().substring(2));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getCurrentAssetsInStablecoin() {
        try {
            String currentAssets = getAssetsStableCoinLabel().getText();
            if (currentAssets.contains("USDT")) {
                return Float.parseFloat(currentAssets.replace(" USDT", ""));
            } else {
                return Float.parseFloat(currentAssets.replace(" USD", ""));
            }
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getBoughtSymbolAssets() {
        try {
            return getBoughtSymbolAssetsLabel().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBoughtSymbolAssetsInUsd() {
        try {
            return Float.parseFloat(getBoughtSymbolInUSDAssetsLabel().getText().replace("($ ", "")
                    .replace(")", ""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getCalculatedTotalAssets() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
        Float calculatedTotalAssets = Float.sum(getBoughtSymbolAssetsInUsd(), getCurrentAssetsInStablecoin());
        return df.format(calculatedTotalAssets);
    }

    public static float getBotTotalAssets() {
        try {
            return Float.parseFloat(getBotTotalAssetsLabel().getText().replace("$ ", ""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getPairInPosition() {
        try {
            return getPairLabelInPosition().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTypeInPosition() {
        try {
            return getTypeLabelInPosition().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getCurrentSizeInPosition() {
        try {
            return getCurrentSizeLabelInPosition().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getBoughtSymbolInUSDInPosition() {
        try {
            return Float.parseFloat(getBoughtSymbolLabelInUSDInPosition().getText().replace("$", ""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getAverageProfitInPosition() {
        try {
            return Float.parseFloat(getAverageProfitLabelInPosition().getText().replace("$",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getAveragePerformanceInPosition() {
        try {
            String currentSize = getAveragePerformanceLabelInPosition().getText();
            return Float.parseFloat(currentSize.substring(0,currentSize.length()-1));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getTotalProfitInPosition() {
        try {
            return Float.parseFloat(getTotalProfitLabelInPosition().getText().replace("$",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getTotalPerformanceInPosition() {
        try {
            //String currentSize = getAveragePerformanceLabelInPosition().getText();
            //return Float.parseFloat(currentSize.replace("%","");
            return getTotalPerformanceLabelInPosition().getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getFilledOrdersInPosition() {
        try {
            String orders = getOrdersLabelInPosition().getText();
            String[] separated = orders.split(" / ");
            return separated[1];
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getAllOrdersInPosition() {
        try {
            String orders = getOrdersLabelInPosition().getText();
            String[] separated = orders.split(" / ");
            return separated[0];
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getOrderSideInTableByPosition(int position) {
        try {
            return getSideInOrdersTableByPosition(position).getText();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getOrderDateInTableByPosition(int position) {
        try {
            return getDateInOrdersTableByPosition(position).getText().substring(0, 10);
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getSizeInTableByPosition(int position) {
        try {
            return Float.parseFloat(getBoughtSizeInTableByPosition(position).getText());
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getLimitPriceInTableByPosition(int position) {
        try {
            return Float.parseFloat(getLimitPriceInOrdersTableByPosition(position).getText().replace("$",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getFillPriceInTableByPosition(int position) {
        try {
            return Float.parseFloat(getFillPriceInOrdersTableByPosition(position).getText().replace("$",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getTotalInTableByPosition(int position) {
        try {
            return Float.parseFloat(getTotalInOrdersTableByPosition(position).getText().replace("$",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static float getProfitUsdInTableByPosition(int position) {
        try {
            return Float.parseFloat(getProfitUsdInOrdersTableByPosition(position).getText().replace("$","")); //TODO
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getTotalProfitUsdSumInTable() {
        try {
            List<Float> totalProfitsUSDList = amountWebElementsToFloat(getProfitsUsdListInTable());
            float totalProfit = floatListSum(totalProfitsUSDList);
            DecimalFormat df = new DecimalFormat("#.##");
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
            return df.format(totalProfit);
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getAverageProfitUsdInTable() {
        try {
            List<Float> totalProfitsUSDList = amountWebElementsToFloat(getProfitsUsdListInTable());
            float totalProfit = floatListSum(totalProfitsUSDList)/getSellOrdersCountInTable();
            DecimalFormat df = new DecimalFormat("#.##");
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
            return df.format(totalProfit);
        } catch (Throwable ex) {
            return "";
        }
    }

    public static float getProfitPercentageInTableByPosition(int position) {
        try {
            return Float.parseFloat(getProfitPercentageInOrdersTableByPosition(position).getText().replace("%",""));
        } catch (Throwable ex) {
            return 0;
        }
    }

    public static String getAverageProfitPercentageInTable() {
        try {
            List<Float> totalProfitsPercentageList = percentageWebElementsToFloat(getProfitsPercentageListInTable());
            float averageProfit = floatListSum(totalProfitsPercentageList)/getSellOrdersCountInTable();
            DecimalFormat df = new DecimalFormat("#.##");
            df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
            return df.format(averageProfit);
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatBotAvatarEqualsAvatarInBotsList(int position) {

        Assert.assertEquals(getAvatarURLInBotsList(position), getAvatarURL());

    }

    public static void verifyThatBotExchangeEqualsExchangeInBotsList(int position) {

        Assert.assertEquals(getExchangeInBotsList(position), getExchange());

    }

    public static void verifyThatBotActivationDateEqualsDateInBotsList(int position) {

        Assert.assertEquals(getDateOfActivationInBotsList(position),getDateOfActivation());

    }

    public static void verifyThatBotRateEqualsRateInBotsList(int position) {

        Assert.assertEquals(getRateInBotsList(position),getRate());

    }

    public static void verifyThatStateEqualsStateInBotsList(int position) {

        Assert.assertEquals(getStateInBotsList(position), getState());
    }

    public static void verifyThatTotalProfitInDetailsEqualsProfitInPositions() {

        Assert.assertEquals(getBotProfitUsd(), getTotalProfitInPosition(), 0);

    }

    public static void verifyThatPerformanceInDetailsEqualsPerformanceInPositions() {

        Assert.assertEquals(getBotPerformance(), getTotalPerformanceInPosition());

    }

    public static void verifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositions() {

        Assert.assertEquals(getBoughtSymbolAssets(), getCurrentSizeInPosition());

    }

    public static void verifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositions() {

        Assert.assertEquals(getBoughtSymbolAssetsInUsd(), getBoughtSymbolInUSDInPosition(),0);

    }

    public static void verifyThatTotalAssetsInDetailsIsCalculatedCorrectly() {

        Assert.assertEquals(getCalculatedTotalAssets(), String.valueOf(getBotTotalAssets()));

    }

    public static void verifyThatPairInPositionIsCorrect(String pair) {

        Assert.assertEquals(pair, getPairInPosition());

    }

    public static void verifyThatTypeInPositionIsCorrect(String type) {

        Assert.assertEquals(type, getTypeInPosition());

    }

    public static void verifyThatCurrentSizeInPositionIsCorrect() {

        Assert.assertEquals(getSizeInTableByPosition(1), Float.parseFloat(getCurrentSizeInPosition().
                replace(" BTC", "")), 0);

    }

    public static void verifyThatCurrentSizeInUsdInPositionIsNotEmpty() {

        Assert.assertNotNull(getBoughtSymbolInUSDInPosition());

    }

    public static void verifyThatAverageProfitInPositionIsCalculatedCorrectly() {

        Assert.assertEquals(getAverageProfitUsdInTable(), String.valueOf(getAverageProfitInPosition()));

    }

    public static void verifyThatAveragePerformanceInPositionIsCorrect() {

        Assert.assertEquals(getAverageProfitPercentageInTable(), String.valueOf(getAveragePerformanceInPosition()));

    }

    public static void verifyThatTotalProfitInPositionIsCalculatedCorrectly() {

        Assert.assertEquals(getTotalProfitUsdSumInTable(), String.valueOf(getTotalProfitInPosition()));

    }

    public static void verifyThatTotalPerformanceInPositionIsCalculatedCorrectly() {

        Assert.assertEquals("...%", getTotalPerformanceInPosition());

    }

    public static void verifyThatAllOrdersValueIsCorrect() {

        Assert.assertEquals(String.valueOf(getOrdersCountInTable()), getAllOrdersInPosition());


    }

    public static void verifyThatFilledAllOrdersValueIsCorrect() {

        Assert.assertEquals(String.valueOf(getFilledOrdersCountInTable()), getFilledOrdersInPosition());

     }

    public static void verifyThatFilledStatusIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getFilledOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatOpenStatusIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getOpenOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatCanceledStatusIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getCanceledOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatRejectedStatusIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getRejectedOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatOrderSideIsCorrectInPositionInOrdersTable(int position, String side) {

        Assert.assertEquals(side, getOrderSideInTableByPosition(position));

    }

    public static void verifyThatMarketOrderIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getMarketOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatLimitOrderIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getLimitOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatStopLimitOrderIsDisplayedInPositionInOrdersTable(int position) {

        Assert.assertTrue(getStopLimitOrderInOrdersTableByPosition(position).isDisplayed());

    }

    public static void verifyThatProfitUsdIsCorrectInPositionInOrdersTable(int position) {

        Assert.assertEquals((getTotalInTableByPosition(position+1) - getTotalInTableByPosition(position)),
                getProfitUsdInTableByPosition(position), 0);

    }

    public static void verifyThatProfitPercentageIsCorrectInPositionInOrdersTable(int position) {

        Assert.assertEquals(getProfitUsdInTableByPosition(position)/getFillPriceInTableByPosition(position)*100,
                getProfitPercentageInTableByPosition(position), 0);

    }

}

package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.*;

import org.openqa.selenium.support.ui.WebDriverWait;
import prizma.maxiru.framework.common.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PrizmaActivateBotPage {

    //Elements with XPath, Tag and CCS selector
    public static final String ACTIVATE_BOT_CLOSE_DIALOG_BTN = "#modal-portal > div:nth-child(2) > div.modal-new__container > .modal-new__close-button";
    public static final String ACTIVATE_BOT_CANCEL_DIALOG_BTN = ".bot-upload__button._light";
    public static final String ACTIVATE_BOT_NEXT_DIALOG_BTN = ".activate-bot__buttons button:nth-child(1)";
    public static final String CONNECT_BOT_DIALOG_BTN = ".store-details__connect";
    public static final String ACTIVATE_BOT_CONNECT_DIALOG_BTN = ".activate-bot__buttons button:nth-child(1)";
    public static final String ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN = "#select-exchange";
    public static final String ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN_BINANCE_OPTION = "#select-exchange > option:nth-child(2)";
    public static final String ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN_COINBASE_OPTION = "#select-exchange > option:nth-child(3)";
    public static final String ACTIVATE_BOT_OPEN_USER_AGREEMENT_OPEN_BTN = ".activate-bot__agreement-link";
    public static final String ACTIVATE_BOT_USER_AGREEMENT_CLOSE_BTN = ".modal-new__close-button";
    public static final String ACTIVATE_BOT_API_KEY_INPUT = "#modal-portal label:nth-child(1) > input";
    public static final String ACTIVATE_BOT_API_SECRET_KEY_INPUT = "#modal-portal label:nth-child(2) > input";
    public static final String ACTIVATE_BOT_PASSPHRASE_INPUT = "#modal-portal label:nth-child(3) > input";
    public static final String ACTIVATE_BOT_ERROR_MESSAGE_TEXT = "//div[@class='activate-bot__warnings warning']";
    public static final String ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_UNCHECKED = ".activate-bot__agreement label:nth-child(1)";
    public static final String ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_CHECKED = "//label[@class='input-checkbox activate-bot__checkbox _checked']";
    public static final String ACTIVATE_BOT_EXCHANGE_VALUE_LABEL = "//span[@class='activate-bot__exchange-balance-value']";
    public static final String ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_UNCHECKED = "//label[@class='input-checkbox activate-bot__checkbox']";
    public static final String ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_CHECKED = "//label[@class='input-checkbox activate-bot__checkbox _checked']";
    public static final String NOTIFY_IF_LOWER_CHECKBOX_UNCHECKED = "//label[@class='input-checkbox activate-bot__checkbox classic']";
    public static final String NOTIFY_IF_LOWER_CHECKBOX_CHECKED = "//label[@class='input-checkbox activate-bot__checkbox _checked classic']";
    public static final String ACTIVATE_BOT_BOT_BALANCE_INPUT = "#modal-portal div:nth-child(3) label > input";
    public static final String ACTIVATE_BOT_BOT_STOP_LOST_INPUT = "#modal-portal div:nth-child(4) label > input";
    public static final String BOT_NAME_DIALOG_STORE = ".store-details__name";
    public static final String BOT_INVESTORS_DIALOG_STORE = ".store-details__stats > div:nth-child(1) > div:nth-child(1) > dl:nth-child(1) > dd";
    public static final String BOT_RATE_DIALOG_STORE = ".store-details__stats > div:nth-child(1) > div:nth-child(1) > dl:nth-child(2) > dd";
    public static final String BOT_EXCHANGES_DIALOG_STORE = ".store-details__stats > div:nth-child(2) > div:nth-child(1) > dl:nth-child(1) > dd";
    public static final String BOT_AUTHOR_DIALOG_STORE = ".store-details__stats > div:nth-child(2) > div:nth-child(1) > dl:nth-child(2) > dd";
    public static final String BOT_DESCRIPTION_DIALOG_STORE = ".store-details__description__text";

    //Other elements
    public static String BINANCE_ERROR_MESSAGE = "Invalid API Key";
    public static String COINBASE_ERROR_MESSAGE = "Something went wrong, please try again";
    public static String BINANCE_EXCHANGE_VALUE = "0.13528018";
    public static String COINBASE_EXCHANGE_VALUE = "0";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getActivateBotCloseDialogBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_CLOSE_DIALOG_BTN);
    }

    public static WebElement getActivateBotCancelDialogBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_CANCEL_DIALOG_BTN);
    }

    public static WebElement getActivateBotNextDialogBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_NEXT_DIALOG_BTN);
    }

    public static WebElement getConnectBotDialogBtn() {
        return getElementByCssSelector(CONNECT_BOT_DIALOG_BTN);
    }

    public static WebElement getActivateBotConnectDialogBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_CONNECT_DIALOG_BTN);
    }

    public static WebElement getActivateBotSelectExchangeDropDown() {
        return getElementByCssSelector(ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN);
    }

    public static WebElement getActivateBotSelectExchangeDropDownBinanceOption() {
        return getElementByCssSelector(ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN_BINANCE_OPTION);
    }

    public static WebElement getActivateBotSelectExchangeDropDownCoinbaseOption() {
        return getElementByCssSelector(ACTIVATE_BOT_SELECT_EXCHANGE_DROP_DOWN_COINBASE_OPTION);
    }

    public static WebElement getActivateBotOpenUserAgreementBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_OPEN_USER_AGREEMENT_OPEN_BTN);
    }

    public static WebElement getActivateBotUserAgreementCloseBtn() {
        return getElementByCssSelector(ACTIVATE_BOT_USER_AGREEMENT_CLOSE_BTN);
    }

    public static WebElement getActivateBotApiKeyInput() {
        return getElementByCssSelector(ACTIVATE_BOT_API_KEY_INPUT);
    }

    public static WebElement getActivateBotApiSecretKeyInput() {
        return getElementByCssSelector(ACTIVATE_BOT_API_SECRET_KEY_INPUT);
    }

    public static WebElement getActivateBotPassphraseInput() {
        return getElementByCssSelector(ACTIVATE_BOT_PASSPHRASE_INPUT);
    }

    public static WebElement getActivateBotErrorMessageText() {
        return getElementByXpath(ACTIVATE_BOT_ERROR_MESSAGE_TEXT);
    }

    public static WebElement getActivateBotUserAgreementCheckboxChecked() {
        return getElementByCssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_CHECKED);
    }

    public static WebElement getActivateBotUserAgreementCheckboxUnchecked() {
        return getElementByCssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_UNCHECKED);
    }

    public static int getActivateBotUserAgreementCheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static int getActivateBotUserAgreementUncheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static WebElement getActivateBotExchangeValueLabel() {
        return getElementByXpath(ACTIVATE_BOT_EXCHANGE_VALUE_LABEL);
    }

    public static WebElement getActivateBotReinvestProfitCheckboxChecked() {
        return getElementByXpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_CHECKED);
    }

    public static WebElement getNotifyIfLowerCheckboxChecked() {
        return getElementByXpath(NOTIFY_IF_LOWER_CHECKBOX_CHECKED);
    }

    public static WebElement getNotifyIfLowerCheckboxUnchecked() {
        return getElementByXpath(NOTIFY_IF_LOWER_CHECKBOX_UNCHECKED);
    }

    public static WebElement getActivateBotReinvestProfitCheckboxUnchecked() {
        return getElementByXpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_UNCHECKED);
    }

    public static int getNotifyIfLowerCheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(NOTIFY_IF_LOWER_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(NOTIFY_IF_LOWER_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static int getActivateBotReinvestProfitCheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_CHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static int getNotifyIfLowerUncheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(NOTIFY_IF_LOWER_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(NOTIFY_IF_LOWER_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static int getActivateBotReinvestProfitUncheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.xpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.xpath(ACTIVATE_BOT_REINVEST_PROFIT_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static WebElement getActivateBotBotBalanceInput() {
        return getElementByCssSelector(ACTIVATE_BOT_BOT_BALANCE_INPUT);
    }

    public static WebElement getActivateBotBotStopLostInput() {
        return getElementByCssSelector(ACTIVATE_BOT_BOT_STOP_LOST_INPUT);
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnActivateBotCloseDialogBtn() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        getActivateBotCloseDialogBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnActivateBotCancelBtn() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        getActivateBotCancelDialogBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnActivateBotNextBtn() {

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        getActivateBotNextDialogBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnConnectBotBtn() {
        try {
            PrizmaDriver.waitToBeClickable(getConnectBotDialogBtn());
            getConnectBotDialogBtn().click();
            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnActivateBotConnectBtn() {
        try {
            PrizmaDriver.waitToBeClickable(getActivateBotConnectDialogBtn());
            getActivateBotConnectDialogBtn().click();
        } catch (Throwable ex) {
            //ignore
        }

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnActivateBotSelectExchangeDropDownAndSelectBinanceOption() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getActivateBotSelectExchangeDropDown().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getActivateBotSelectExchangeDropDown());
            getActivateBotSelectExchangeDropDown().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getActivateBotSelectExchangeDropDownBinanceOption().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotSelectExchangeDropDownAndSelectCoinbaseOption() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            getActivateBotSelectExchangeDropDown().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getActivateBotSelectExchangeDropDown());
            getActivateBotSelectExchangeDropDown().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getActivateBotSelectExchangeDropDownCoinbaseOption().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnActivateBotOpenUserAgreementBtn() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotOpenUserAgreementBtn());
        PrizmaDriver.waitToBeClickable(getActivateBotOpenUserAgreementBtn());
        getActivateBotOpenUserAgreementBtn().click();

        try {
            PrizmaDriver.waitDriver(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CLOSE_BTN), 30);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as after click Open User Agreement btn not open User Agreement dialog. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotUserAgreementCloseBtn() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotUserAgreementCloseBtn());
        PrizmaDriver.waitToBeClickable(getActivateBotUserAgreementCloseBtn());
        getActivateBotUserAgreementCloseBtn().click();

        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(ACTIVATE_BOT_USER_AGREEMENT_CLOSE_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on User Agreement dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void clickOnActivateBotApiKeyInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotApiKeyInput());
        PrizmaDriver.waitToBeClickable(getActivateBotApiKeyInput());
        getActivateBotApiKeyInput().click();
    }

    public static void clickOnActivateBotApiSecretKeyInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotApiSecretKeyInput());
        PrizmaDriver.waitToBeClickable(getActivateBotApiSecretKeyInput());
        getActivateBotApiSecretKeyInput().click();
    }

    public static void clickOnActivateBotPassphraseInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotPassphraseInput());
        PrizmaDriver.waitToBeClickable(getActivateBotPassphraseInput());
        getActivateBotPassphraseInput().click();
    }

    public static void clickOnActivateBotUserAgreementCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotUserAgreementUncheckedCount();

        if (size > 0) {
            getActivateBotUserAgreementCheckboxUnchecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotUserAgreementCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotUserAgreementCheckedCount();

        if (size > 0) {
            getActivateBotUserAgreementCheckboxChecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotReinvestProfitCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotReinvestProfitUncheckedCount();

        if (size > 0) {
            getActivateBotReinvestProfitCheckboxUnchecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotReinvestProfitCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotReinvestProfitCheckedCount();

        if (size > 0) {
            getActivateBotReinvestProfitCheckboxChecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnNotifyIfLowerCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getNotifyIfLowerUncheckedCount();

        if (size > 0) {
            getNotifyIfLowerCheckboxUnchecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnNotifyIfLowerCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getNotifyIfLowerCheckedCount();

        if (size > 0) {
            getNotifyIfLowerCheckboxChecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnActivateBotBotBalanceInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotBotBalanceInput());
        PrizmaDriver.waitToBeClickable(getActivateBotBotBalanceInput());
        getActivateBotBotBalanceInput().click();
    }

    public static void clickOnActivateBotBotStopLostInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotBotStopLostInput());
        PrizmaDriver.waitToBeClickable(getActivateBotBotStopLostInput());
        getActivateBotBotStopLostInput().click();
    }

    /***
     * Methods to clear the elements
     *
     */
    public static void clearActivateBotApiKeyInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotApiKeyInput());
        PrizmaDriver.waitToBeClickable(getActivateBotApiKeyInput());
        getActivateBotApiKeyInput().clear();
    }

    public static void clearActivateBotApiSecretKeyInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotApiSecretKeyInput());
        PrizmaDriver.waitToBeClickable(getActivateBotApiSecretKeyInput());
        getActivateBotApiSecretKeyInput().clear();
    }

    public static void clearActivateBotPassphraseInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotPassphraseInput());
        PrizmaDriver.waitToBeClickable(getActivateBotPassphraseInput());
        getActivateBotPassphraseInput().clear();
    }

    public static void clearActivateBotBotBalanceInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotBotBalanceInput());
        PrizmaDriver.waitToBeClickable(getActivateBotBotBalanceInput());
        getActivateBotBotBalanceInput().clear();
    }

    public static void clearActivateBotBotStopLostInput() {
        PrizmaUtils.scrollToElementOnPage(getActivateBotBotStopLostInput());
        PrizmaDriver.waitToBeClickable(getActivateBotBotStopLostInput());
        getActivateBotBotStopLostInput().clear();
    }

    /***
     * Methods to set the value into element
     */
    public static void setApiKeyOnActivateBotDialog(String apiKey) {
        if (apiKey.isEmpty()) {
            clickOnActivateBotApiKeyInput();
            clearActivateBotApiKeyInput();
        } else {
            clickOnActivateBotApiKeyInput();
            clearActivateBotApiKeyInput();
            getActivateBotApiKeyInput().sendKeys(apiKey);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setApiSecretKeyOnActivateBotDialog(String apiSecretKey) {
        if (apiSecretKey.isEmpty()) {
            clickOnActivateBotApiSecretKeyInput();
            clearActivateBotApiSecretKeyInput();
        } else {
            clickOnActivateBotApiSecretKeyInput();
            clearActivateBotApiSecretKeyInput();
            getActivateBotApiSecretKeyInput().sendKeys(apiSecretKey);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setPassphraseOnActivateBotDialog(String passphrase) {
        if (passphrase.isEmpty()) {
            clickOnActivateBotPassphraseInput();
            clearActivateBotPassphraseInput();
        } else {
            clickOnActivateBotPassphraseInput();
            clearActivateBotPassphraseInput();
            getActivateBotPassphraseInput().sendKeys(passphrase);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotBalanceOnActivateBotDialog(String botBalanceValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        if (botBalanceValue.isEmpty()) {
            clickOnActivateBotBotBalanceInput();
            clearActivateBotBotBalanceInput();
        } else {
            clickOnActivateBotBotBalanceInput();
            clearActivateBotBotBalanceInput();
            getActivateBotBotBalanceInput().sendKeys(botBalanceValue);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    public static void setBotStopLostOnActivateBotDialog(String botStopLostValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        if (botStopLostValue.isEmpty()) {
            clickOnActivateBotBotStopLostInput();
            clearActivateBotBotStopLostInput();
        } else {
            clickOnActivateBotBotStopLostInput();
            clearActivateBotBotStopLostInput();
            getActivateBotBotStopLostInput().sendKeys(botStopLostValue);

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        }
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromActivateBotErrorMessageText() {
        try {
            return getActivateBotErrorMessageText().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getTextFromActivateBotExchangeValueLabel() {
        try {
            return getActivateBotExchangeValueLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatActivateBotDialogCloseAfterClickOnCloseBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(ACTIVATE_BOT_CLOSE_DIALOG_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on Activate Bot Details dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThanNextBtnIsDisabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        WebElement button = getActivateBotNextDialogBtn();
        Assert.assertEquals("Next button is enabled",false, button.isEnabled());
    }

    public static void verifyThatActivateBotDialogCloseAfterClickOnCancelBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(ACTIVATE_BOT_CANCEL_DIALOG_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on cancel btn on Activate Bot Details dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatUserNotRedirectToSecondPageOfActivateBotFlow() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(ACTIVATE_BOT_OPEN_USER_AGREEMENT_OPEN_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on next btn on activate flow first page without select exchange user redirect to activate flow second page when next btn should be disabled. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotErrorMessageForBinanceExchange() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromActivateBotErrorMessageText(), containsString(BINANCE_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as binance error message not appear on Activate bot dialog when user try to pass flow with invalid test data. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotErrorMessageForCoinbaseExchange() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromActivateBotErrorMessageText(), containsString(COINBASE_ERROR_MESSAGE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as coinbase error message not appear on Activate bot dialog when user try to pass flow with invalid test data. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotExchangeValueForBinance() {
        try {
            PrizmaDriver.waitDriver(By.xpath(ACTIVATE_BOT_EXCHANGE_VALUE_LABEL), 60);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user not land on last Activate bot flow page. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromActivateBotExchangeValueLabel(), containsString(BINANCE_EXCHANGE_VALUE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Binance Exchange Value was " + getTextFromActivateBotExchangeValueLabel() + " when tests expect " + BINANCE_EXCHANGE_VALUE + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotExchangeValueForCoinbase() {
        try {
            PrizmaDriver.waitDriver(By.xpath(ACTIVATE_BOT_EXCHANGE_VALUE_LABEL), 60);
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as user not land on last Activate bot flow page. Check video and logs, or something was changed on Prizma DD side.");
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromActivateBotExchangeValueLabel(), containsString(COINBASE_EXCHANGE_VALUE));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Coinbase Exchange Value was " + getTextFromActivateBotExchangeValueLabel() + " when tests expect " + COINBASE_EXCHANGE_VALUE + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotUserAgreementCheckboxIsEnabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotUserAgreementCheckedCount();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as Activate Bot User Agreement checkbox was not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotUserAgreementCheckboxIsDisabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotUserAgreementUncheckedCount();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as Activate Bot User Agreement checkbox checkbox was not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotReinvestProfitCheckboxIsEnabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotReinvestProfitCheckedCount();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as Activate Bot Reinvest Profit checkbox was not enabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatActivateBotReinvestProfitCheckboxIsDisabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getActivateBotReinvestProfitUncheckedCount();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as Activate Bot Reinvest Profit checkbox checkbox was not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotDetailsOnStorePageAreCorrect(String botName, String investors, String rate,
                                                                 String exchanges, String author, String description) {
        String botNameActual = getElementByCssSelector(BOT_NAME_DIALOG_STORE).getText();
        String investorsActual = getElementByCssSelector(BOT_INVESTORS_DIALOG_STORE).getText();
        String rateActual = getElementByCssSelector(BOT_RATE_DIALOG_STORE).getText();
        String exchangesActual = getElementByCssSelector(BOT_EXCHANGES_DIALOG_STORE).getText();
        String authorActual = getElementByCssSelector(BOT_AUTHOR_DIALOG_STORE).getText();
        String descriptionActual = getElementByCssSelector(BOT_DESCRIPTION_DIALOG_STORE).getText();

        Assert.assertEquals(botName, botNameActual);
        Assert.assertEquals(investors, investorsActual);
        Assert.assertEquals(rate, rateActual);
        Assert.assertEquals(exchanges, exchangesActual);
        Assert.assertEquals(author, authorActual);
        Assert.assertEquals(description, descriptionActual);

    }

    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getElementByXpath(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.xpath(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.xpath(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }
}

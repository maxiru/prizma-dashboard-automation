package app;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import prizma.maxiru.framework.common.PrizmaConstants;
import prizma.maxiru.framework.common.PrizmaDriver;

public class PrizmaAccountPage {

    //Elements with XPath, Tag and CCS selector
    public static final String LOG_OUT_BTN = ".page__content button:nth-child(3)";
    public static final String FULL_NAME = "div:nth-child(1) > div > div:nth-child(1) > .user-edit-value-box__value";
    public static final String EMAIL = "div:nth-child(1) > div > div:nth-child(2) > .user-edit-value-box__value";
    public static final String PASSWORD = "div:nth-child(1) > div > div:nth-child(3) > .user-edit-value-box__value";
    public static final String DISPLAY_NAME = "div:nth-child(2) > div > div:nth-child(1) > .user-edit-value-box__value";
    public static final String USDC_ADDRESS = "div:nth-child(2) > div > div:nth-child(2) > .user-edit-value-box__value";
    public static final String USER_EDIT_MODAL = ".user-edit-value-editor";
    public static final String USER_EDIT_MODAL_CLOSE_BTN = ".modal-new__close-button";
    public static final String USER_EDIT_MODAL_CANCEL_BTN = ".user-edit__modal button:nth-child(2)";
    public static final String USER_EDIT_MODAL_OK_BTN = ".user-edit__modal button:nth-child(1)";
    public static final String USER_EDIT_MODAL_TEXT_INPUT = "input[type='text']";
    public static final String USER_EDIT_MODAL_PASSWORD_INPUT = "input[type='password']";
    public static final String USER_EDIT_MODAL_CURRENT_PASSWORD_INPUT = ".user-edit__modal label:nth-child(2) > input";
    public static final String USER_EDIT_MODAL_NEW_PASSWORD_INPUT = ".user-edit__modal label:nth-child(3) > input";
    public static final String USER_EDIT_MODAL_CONFIRM_PASSWORD_INPUT = ".user-edit__modal label:nth-child(4) > input";
    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getLogOutButton() {
        return getElementByCssSelector(LOG_OUT_BTN);
    }

    public static WebElement getFullName() {
        return getElementByCssSelector(FULL_NAME);
    }

    public static WebElement getEmail() {
        return getElementByCssSelector(EMAIL);
    }

    public static WebElement getPassword() {
        return getElementByCssSelector(PASSWORD);
    }

    public static WebElement getDisplayName() {
        return getElementByCssSelector(DISPLAY_NAME);
    }

    public static WebElement getUSDCAddress() {
        return getElementByCssSelector(USDC_ADDRESS);
    }

    public static WebElement getUserEditModal() {
        return getElementByCssSelector(USER_EDIT_MODAL);
    }

    public static WebElement getUserEditModalCloseBtn() {
        return getElementByCssSelector(USER_EDIT_MODAL_CLOSE_BTN);
    }

    public static WebElement getUserEditModalCancelBtn() {
        return getElementByCssSelector(USER_EDIT_MODAL_CANCEL_BTN);
    }

    public static WebElement getUserEditModalOkBtn() {
        return getElementByCssSelector(USER_EDIT_MODAL_OK_BTN);
    }

    public static WebElement getUserEditModalTextInput() {
        return getElementByCssSelector(USER_EDIT_MODAL_TEXT_INPUT);
    }

    public static WebElement getUserEditModalPasswordInput() {
        return getElementByCssSelector(USER_EDIT_MODAL_PASSWORD_INPUT);
    }

    public static WebElement getUserEditModalCurrentPasswordInput() {
        return getElementByCssSelector(USER_EDIT_MODAL_CURRENT_PASSWORD_INPUT);
    }

    public static WebElement getUserEditModalNewPasswordInput() {
        return getElementByCssSelector(USER_EDIT_MODAL_NEW_PASSWORD_INPUT);
    }

    public static WebElement getUserEditModalConfirmPasswordInput() {
        return getElementByCssSelector(USER_EDIT_MODAL_CONFIRM_PASSWORD_INPUT);
    }
    /***
     * Methods to click on the elements
     */

    public static void clickOnLogOutButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getLogOutButton().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnFullName() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getFullName().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnEmail() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getEmail().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnPassword() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getPassword().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnDisplayName() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getDisplayName().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnUSDCAddress() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUSDCAddress().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCloseModalButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalCloseBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCancelModalButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalCancelBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnOkModalButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalOkBtn().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to set values into the elements
     */

    public static void setValueInUserEditModalTextInput(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalTextInput().click();
        getUserEditModalTextInput().clear();
        getUserEditModalTextInput().sendKeys(value);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void setValueInUserEditModalPasswordInput(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalPasswordInput().click();
        getUserEditModalPasswordInput().clear();
        getUserEditModalPasswordInput().sendKeys(value);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void setValueInUserEditModalCurrentPasswordInput(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalCurrentPasswordInput().click();
        getUserEditModalCurrentPasswordInput().clear();
        getUserEditModalCurrentPasswordInput().sendKeys(value);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void setValueInUserEditModalNewPasswordInput(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalNewPasswordInput().click();
        getUserEditModalNewPasswordInput().clear();
        getUserEditModalNewPasswordInput().sendKeys(value);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void setValueInUserEditModalConfirmPasswordInput(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getUserEditModalConfirmPasswordInput().click();
        getUserEditModalConfirmPasswordInput().clear();
        getUserEditModalConfirmPasswordInput().sendKeys(value);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to verify elements on Page
     */

    public static void verifyElementsOnAccountPage() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        Assert.assertTrue("Full name is not displayed", getFullName().isDisplayed());
        Assert.assertTrue("Email is not displayed", getEmail().isDisplayed());
        Assert.assertTrue("Password is not displayed", getPassword().isDisplayed());
        Assert.assertTrue("Display name is not displayed", getDisplayName().isDisplayed());
        Assert.assertTrue("USDC address is not displayed", getUSDCAddress().isDisplayed());
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void verifyUserEditModalIsDisplayed() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        Assert.assertTrue("User edit modal is not displayed", getUserEditModal().isDisplayed());
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void verifyUserEditModalIsNotDisplayed() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        Boolean flag;
        try {
            PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(USER_EDIT_MODAL));
            flag = true;
        }catch (Exception e){
            flag = false;
        }

        Assert.assertFalse("User edit modal is displayed", flag);
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }


    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }
}

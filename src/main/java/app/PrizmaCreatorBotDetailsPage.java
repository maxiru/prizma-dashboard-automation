package app;

import org.junit.Assert;
import org.openqa.selenium.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaCreatorPage.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class PrizmaCreatorBotDetailsPage {

    //Elements with XPath, Tag and CCS selector
    public static final String BOT_DETAILS_CLOSE_DIALOG_BTN = ".modal-new__close-button";
    public static final String BOT_DETAILS_DELETE_BTN = "//button[text()='Delete']";
    public static final String BOT_DETAILS_DOWNLOAD_BTN = "//button[text()='Download']";
    public static final String BOT_DETAILS_PRIVATE_CHECKBOX = "//label[contains(@class,'input-checkbox bot-details')]";
    public static final String BOT_DETAILS_EMPTY_STATISTICS_LABEL = "//label[contains(@class,'input-checkbox')]";
    public static final String BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN = ".bot-row__error-details";

    //Other elements
    public static String BOT_DETAILS_EMPTY_STATISTICS_TEXT = "STATISTICS NOT AVAILABLE";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotDetailsCloseDialogBtn() {
        return getElementByCssSelector(BOT_DETAILS_CLOSE_DIALOG_BTN);
    }

    public static WebElement getBotDetailsViewErrorDetailsDialogBtn() {
        return getElementByCssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN);
    }

    public static WebElement getBotDetailsDeleteBtn() {
        return getElementByXpath(BOT_DETAILS_DELETE_BTN);
    }

    public static WebElement getBotDetailsDownloadBtn() {
        return getElementByXpath(BOT_DETAILS_DOWNLOAD_BTN);
    }

    public static WebElement getBotDetailsPrivateCheckbox() {
        return getElementByXpath(BOT_DETAILS_PRIVATE_CHECKBOX);
    }

    public static WebElement getBotDetailsEmptyStatisticsLabel() {
        return getElementByXpath(BOT_DETAILS_EMPTY_STATISTICS_LABEL);
    }

    /***
     * Methods to click on the elements
     *
     */
    public static void clickOnBotDetailsCloseDialogBtn() {
        PrizmaUtils.scrollToElementOnPage(getBotDetailsCloseDialogBtn());
        PrizmaDriver.waitToBeClickable(getBotDetailsCloseDialogBtn());
        getBotDetailsCloseDialogBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotDetailsViewErrorDetailsDialogBtn() {
        PrizmaUtils.scrollToElementOnPage(getBotDetailsViewErrorDetailsDialogBtn());
        PrizmaDriver.waitToBeClickable(getBotDetailsViewErrorDetailsDialogBtn());
        getBotDetailsViewErrorDetailsDialogBtn().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotDetailsDeleteBtn() {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotDetailsDeleteBtn());
            PrizmaDriver.waitToBeClickable(getBotDetailsDeleteBtn());
            getBotDetailsDeleteBtn().click();

            PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static void clickOnBotDetailsDownloadBtn(int timer) {
        try {
            PrizmaUtils.scrollToElementOnPage(getBotDetailsDownloadBtn());
            getBotDetailsDownloadBtn().click();

            PrizmaUtils.waitTillFileDownload(BOT_NAME, timer);
        } catch (Throwable ex) {
            PrizmaUtils.waitTillFileDownload(BOT_NAME, timer);
        }
    }

    public static void clickOnBotDetailsPrivateCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotDetailsPrivateCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotDetailsPrivateCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotDetailsPrivateCheckbox().click();

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getTextFromBotDetailsEmptyStatisticsLabel() {
        try {
            return getBotDetailsEmptyStatisticsLabel().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void verifyThatBotDetailsDialogCloseAfterClickOnCloseBtn() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_DETAILS_CLOSE_DIALOG_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as after click on close btn on Bot Details dialog -> dialog not close. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotDetailsEmptyStatisticsTextPresentOnBotDetailsDialog(String fileName) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getTextFromBotDetailsEmptyStatisticsLabel(), containsString(BOT_DETAILS_EMPTY_STATISTICS_TEXT));
        } catch (Throwable ex) {
            try {
                Assert.fail("Looks like that something when wrong as bot empty statistics text not present on bot details dialog but should. Check video and logs, or something was changed on Prizma DD side.");
            } catch (Throwable ex2) {
                PrizmaDriver.getScreenshot(fileName);
            }
        }
    }

    public static void verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN), 10);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (!flag) {
            Assert.fail("Looks like that something when wrong as bot details view error details dialog button was hidden when should be present as per user role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotDetailsViewErrorDetailsDialogBtnIsNotPresent() {
        boolean flag;
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            PrizmaDriver.waitDriver(By.cssSelector(BOT_DETAILS_VIEW_ERROR_DETAILS_DIALOG_BTN), 5);
            flag = true;
        } catch (Throwable ex) {
            flag = false;
        }

        if (flag) {
            Assert.fail("Looks like that something when wrong as bot details view error details dialog button was present when should be hidden as per user role. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getElementByXpath(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.xpath(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.xpath(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

}

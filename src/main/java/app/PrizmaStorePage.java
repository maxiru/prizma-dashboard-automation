package app;

import org.junit.*;
import org.openqa.selenium.*;

import java.util.*;

import prizma.maxiru.framework.common.*;

import static app.PrizmaCreatorPage.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PrizmaStorePage {

    //Elements with XPath, Tag and CCS selector
    public static final String STORE_LABEL = "//h1[text()='Store']";
    public static final String STORE_TABLE_ASC_DESC_ICON = ".store-filters__sort-direction";
    public static final String STORE_TABLE_SORT_SELECT = "select.store-filters__sort";
    public static final String STORE_MAIN_TABLE = ".store-catalog__items";
    public static final String STORE_TABLE_ROW = ".store-catalog__item";
    public static final String STORE_MAIN_TABLE_NAME_CONTAINER = ".store-card__header__title";
    public static final String STORE_MAIN_TABLE_NAME_LABEL = "span";
    public static final String STORE_MAIN_TABLE_NAME_LABEL_CSS = ".store-card__name";
    public static final String INVESTOR_MAIN_TABLE_NAME_LABEL_CSS = ".bot-subscription-row";
    public static final String STORE_MAIN_TABLE_INVESTORS_COUNT_LABEL = "td";
    public static final String STORE_MAIN_TABLE_RISK_LEVEL_LABEL = "td";
    public static final String STORE_MAIN_TABLE_PUBLISHED_CONTAINER = "td";
    public static final String STORE_MAIN_TABLE_PUBLISHED_LABEL = "span";
    public static final String STORE_MAIN_TABLE_PERFOMANCE_BACKTEST_LABEL = "td";
    public static final String STORE_MAIN_TABLE_PERFOMANCE_ALL_TIME_LABEL = "td";
    public static final String CREATED_BY_ME_CHECKBOX = ".store-filters__item__checkbox";
    public static final String CREATED_BY_ME_CHECKBOX_UNCHECKED = ".store-filters__item__checkbox";
    public static final String CREATED_BY_ME_CHECKBOX_CHECKED = ".store-filters__item__checkbox._checked";
    public static final String BOTS_PER_PAGE_SELECTOR = "//select[contains(@class,'data-table__pagination__select')]";
    public static final String BOTS_PER_PAGE_SELECTOR_OPTION_1 = "option";
    public static final String BOTS_PER_PAGE_SELECTOR_OPTION_2 = "option";
    public static final String BOTS_PER_PAGE_SELECTOR_OPTION_3 = "option";
    public static final String BOTS_PER_PAGE_SELECTOR_OPTION_4 = "option";
    public static final String BOTS_NEXT_PAGE = "//button[contains(@class,'data-table__pagination__button')]";
    public static final String BOTS_PREVIOUS_PAGE = "//button[contains(@class,'data-table__pagination__button')]";
    public static final String BOTS_THIRD_PAGE = "//button[contains(@class,'data-table__pagination__button')]";
    public static final String BOTS_SECOND_PAGE = "//button[contains(@class,'data-table__pagination__button')]";
    public static final String SEARCH_INPUT = ".store-filters__search input";
    public static final String SEARCH_HIGHLIGHTED_RESULT = ".store-card__name__text_hightlighted";
    public static final String SEARCH_NO_RESULT = ".store-catalog__empty-text";
    public static final String STORE_TABLE_SWITCHER_ICON = ".store-catalog__view-switcher button:nth-child(2)";
    public static final String STORE_GRID_SWITCHER_ICON = ".store-catalog__view-switcher button:nth-child(1)";
    public static final String SHOW_MORE_BTN = ".store-catalog__more-button";

    //Other elements
    public static int STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = 0;
    public static String STORE_BOT_NAME_FOR_FUTURE_TESTS_1 = "cnggflqkxeiqayvrywpz";
    public static String STORE_BOT_NAME_FOR_FUTURE_TESTS_2 = "cnggflqkxeiqayvryw...";

    /***
     * Methods to get the elements
     *
     * @return WebElement
     */
    public static WebElement getBotsNextPageButton() {
        return getElementByXpath(BOTS_NEXT_PAGE);
    }

    public static WebElement getBotsPreviousPageButton() {
        return getElementByXpath(BOTS_PREVIOUS_PAGE);
    }

    public static WebElement getBotsThirdPageButton() {
        return getElementByXpath(BOTS_THIRD_PAGE);
    }

    public static WebElement getBotsSecondPageButton() {
        return getElementByXpath(BOTS_SECOND_PAGE);
    }

    public static WebElement getBotsPerPageSelector() {
        return getElementByXpath(BOTS_PER_PAGE_SELECTOR);
    }

    public static WebElement getBotsPerPageSelectorOption1() {
        return getElementByXpath(BOTS_PER_PAGE_SELECTOR_OPTION_1);
    }

    public static WebElement getBotsPerPageSelectorOption2() {
        return getElementByXpath(BOTS_PER_PAGE_SELECTOR_OPTION_2);
    }

    public static WebElement getBotsPerPageSelectorOption3() {
        return getElementByXpath(BOTS_PER_PAGE_SELECTOR_OPTION_3);
    }

    public static WebElement getBotsPerPageSelectorOption4() {
        return getElementByXpath(BOTS_PER_PAGE_SELECTOR_OPTION_4);
    }

    public static WebElement getStoreMainTable() {
        return getElementByCssSelector(STORE_MAIN_TABLE);
    }

    public static WebElement getStoreTableSwitcherIcon() {
        return getElementByCssSelector(STORE_TABLE_SWITCHER_ICON);
    }

    public static WebElement getStoreGridSwitcherIcon() {
        return getElementByCssSelector(STORE_GRID_SWITCHER_ICON);
    }

    public static WebElement getStoreTableAscDescIcon() {
        return getElementByCssSelector(STORE_TABLE_ASC_DESC_ICON);
    }

    public static WebElement getStoreTableSortSelect() {
        return getElementByCssSelector(STORE_TABLE_SORT_SELECT);
    }

    public static WebElement getStoreTableSortSelectOptionByValue(String value) {
        return getElementByCssSelector(".store-filters__sort[value="+value+"]");
    }

    public static WebElement getStoreMainTableRowByPosition(int position) {
        return getElementByCssSelector(STORE_TABLE_ROW);
    }

    public static WebElement getStoreMainTableRowByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTable().findElements(By.cssSelector(STORE_TABLE_ROW));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS - 1);
    }

    public static int getStoreMainTableRowsCount() {
        List<WebElement> list;
        list = getStoreMainTable().findElements(By.cssSelector(STORE_TABLE_ROW));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table row is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.size();
    }

    public static WebElement getStoreMainTableNameLabelByPosition(int position) {
        List<WebElement> list;
        list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(STORE_MAIN_TABLE_NAME_LABEL_CSS));
        int size = list.size();
        WebElement webElement = null;
        if (size == 0) {
            Assert.fail("Looks like that something when wrong as Store is empty. Check video and logs, or something was changed on DD side.");
        } else {
            webElement = list.get(position);
        }
        return webElement;
    }

    public static WebElement getStoreMainTableNameLabelByName(String name) {
        List<WebElement> list;
        list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(STORE_MAIN_TABLE_NAME_LABEL_CSS));
        int size = list.size();
        WebElement webElement = null;
        if (size == 0) {
            Assert.fail("Looks like that something when wrong as Investor main table name label is empty. Check video and logs, or something was changed on DD side.");
        } else {
            for (int i = 0; i < size; i++) {
                webElement = list.get(i);
                String text = webElement.getText();
                if (text.contains(name)) {
                    return webElement;
                }
            }
        }
        return webElement;
    }

    public static WebElement getInvestorMainTableNameLabelByName(String name) {
        List<WebElement> list;
        list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(INVESTOR_MAIN_TABLE_NAME_LABEL_CSS));
        int size = list.size();
        WebElement webElement = null;
        if (size == 0) {
            Assert.fail("Looks like that something when wrong as Investor main table name label is empty. Check video and logs, or something was changed on DD side.");
        } else {
            for (int i = 0; i < size; i++) {
                webElement = list.get(i);
                String text = webElement.getText();
                if (text.contains(name)) {
                    return webElement;
                }
            }
        }
        return webElement;
    }

    public static WebElement getStoreMainTableNameLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.cssSelector(STORE_MAIN_TABLE_NAME_CONTAINER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table name label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(0).findElement(By.tagName(STORE_MAIN_TABLE_NAME_LABEL));
    }

    public static WebElement getStoreMainTableInvestorCountLabelByPosition(int position) {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(position).findElements(By.xpath(STORE_MAIN_TABLE_INVESTORS_COUNT_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table investor count label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(1);
    }

    public static WebElement getStoreMainTableInvestorCountLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.xpath(STORE_MAIN_TABLE_INVESTORS_COUNT_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table investor count label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(1);
    }

    public static WebElement getStoreMainTableRiskLevelLabelByPosition(int position) {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(position).findElements(By.xpath(STORE_MAIN_TABLE_RISK_LEVEL_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table risk level label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(2);
    }

    public static WebElement getStoreMainTableRiskLevelLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.xpath(STORE_MAIN_TABLE_RISK_LEVEL_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table risk level label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(2);
    }

    public static WebElement getStoreMainTablePublishedLabelByPosition(int position) {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(position).findElements(By.xpath(STORE_MAIN_TABLE_PUBLISHED_CONTAINER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table published label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(3).findElement(By.tagName(STORE_MAIN_TABLE_PUBLISHED_LABEL));
    }

    public static WebElement getStoreMainTablePublishedLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.xpath(STORE_MAIN_TABLE_PUBLISHED_CONTAINER));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table published label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(3).findElement(By.tagName(STORE_MAIN_TABLE_PUBLISHED_LABEL));
    }

    public static WebElement getStoreMainTablePerfomanceBacktestLabelByPosition(int position) {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(position).findElements(By.xpath(STORE_MAIN_TABLE_PERFOMANCE_BACKTEST_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table perfomance backtest label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(4);
    }

    public static WebElement getStoreMainTablePerfomanceBacktestLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.xpath(STORE_MAIN_TABLE_PERFOMANCE_BACKTEST_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table perfomance backtest label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(4);
    }

    public static WebElement getStoreMainTablePerfomanceAllTimeLabelByPosition(int position) {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(position).findElements(By.xpath(STORE_MAIN_TABLE_PERFOMANCE_ALL_TIME_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table perfomance all time label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(4);
    }

    public static WebElement getStoreMainTablePerfomanceAllTimeLabelByPositionFromVariable() {
        List<WebElement> list;
        list = getStoreMainTableRowByPosition(STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS).findElements(By.xpath(STORE_MAIN_TABLE_PERFOMANCE_ALL_TIME_LABEL));
        if (list.size() == 0) {
            Assert.fail("Looks like that something when wrong as Store main table perfomance all time label is empty. Check video and logs, or something was changed on DD side.");
        }
        return list.get(4);
    }

    public static WebElement getCreatedByMeCheckboxChecked() {
        return getElementByCssSelector(CREATED_BY_ME_CHECKBOX_CHECKED);
    }

    public static WebElement getCreatedByMeCheckboxUnchecked() {
        return getElementByCssSelector(CREATED_BY_ME_CHECKBOX_UNCHECKED);
    }

    public static WebElement getSearchInput() {
        return getElementByCssSelector(SEARCH_INPUT);
    }

    public static WebElement getSearchHighlightedResult() {
        return getElementByCssSelector(SEARCH_HIGHLIGHTED_RESULT);
    }

    public static WebElement getSearchNoResult() {
        return getElementByCssSelector(SEARCH_NO_RESULT);
    }

    public static WebElement getShowMoreButton() {
        return getElementByCssSelector(SHOW_MORE_BTN);
    }

    public static int getCreatedByMeCheckboxUncheckedCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(CREATED_BY_ME_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(CREATED_BY_ME_CHECKBOX_UNCHECKED));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    public static int getCreatedByMeCheckboxCount() {
        List<WebElement> list;
        if (PrizmaDriver.isSelenoid()) {
            list = PrizmaDriver.getCurrentDriver().findElements(By.cssSelector(CREATED_BY_ME_CHECKBOX));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        } else {
            list = PrizmaDriver.getCurrentRemoveWebDriver().findElements(By.cssSelector(CREATED_BY_ME_CHECKBOX));
            if (list.size() == 0) {
                return 0;
            }
            return list.size();
        }
    }

    /***
     * Methods to click on the elements
     */

    public static void clickOnStoreShowMoreButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getShowMoreButton().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStoreTableSwitcherIcon() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getStoreTableSwitcherIcon().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStoreGridSwitcherIcon() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getStoreGridSwitcherIcon().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnStoreTableAscDescIcon() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getStoreTableAscDescIcon().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void sortByValue(String value) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getStoreTableSortSelect().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getStoreTableSortSelectOptionByValue(value).click();
    }

    public static void clickOnCreatedByMeCheckboxToEnable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxUncheckedCount();

        if (size > 0) {
            getCreatedByMeCheckboxUnchecked().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnCreatedByMeCheckboxToDisable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        getCreatedByMeCheckboxChecked().click();
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotsPerPageSelectorAndSelectOption1() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            getBotsPerPageSelector().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getBotsPerPageSelector());
            getBotsPerPageSelector().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotsPerPageSelectorOption1().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnBotsPerPageSelectorAndSelectOption2() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            getBotsPerPageSelector().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getBotsPerPageSelector());
            getBotsPerPageSelector().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotsPerPageSelectorOption2().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnBotsPerPageSelectorAndSelectOption3() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            getBotsPerPageSelector().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getBotsPerPageSelector());
            getBotsPerPageSelector().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotsPerPageSelectorOption3().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnBotsPerPageSelectorAndSelectOption4() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);

        try {
            getBotsPerPageSelector().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getBotsPerPageSelector());
            getBotsPerPageSelector().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        getBotsPerPageSelectorOption4().click();

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void clickOnBotsNextPageButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getBotsNextPageButton().click();
        } catch (Throwable ex) {
            try {
                PrizmaUtils.scrollToElement(getBotsNextPageButton());
                getBotsNextPageButton().click();
            } catch (Throwable ex2) {
                //ignore
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotsPreviousPageButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getBotsPreviousPageButton().click();
        } catch (Throwable ex) {
            try {
                PrizmaUtils.scrollToElement(getBotsPreviousPageButton());
                getBotsPreviousPageButton().click();
            } catch (Throwable ex2) {
                //ignore
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotsThirdPageButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getBotsThirdPageButton().click();
        } catch (Throwable ex) {
            try {
                PrizmaUtils.scrollToElement(getBotsThirdPageButton());
                getBotsThirdPageButton().click();
            } catch (Throwable ex2) {
                //ignore
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotsSecondPageButton() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getBotsSecondPageButton().click();
        } catch (Throwable ex) {
            try {
                PrizmaUtils.scrollToElement(getBotsSecondPageButton());
                getBotsSecondPageButton().click();
            } catch (Throwable ex2) {
                //ignore
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getStoreMainTableNameLabelByPosition(position).click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getStoreMainTableNameLabelByPosition(position));
            getStoreMainTableNameLabelByPosition(position).click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotOnStorePageToOpenActivateBotDialogByName(String name) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getStoreMainTableNameLabelByName(name).click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getStoreMainTableNameLabelByName(name));
            getStoreMainTableNameLabelByName(name).click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    public static void clickOnBotOnStorePageToOpenActivateBotDialogOnPositionFromVariable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            getStoreMainTableNameLabelByPositionFromVariable().click();
        } catch (Throwable ex) {
            PrizmaUtils.scrollToElement(getStoreMainTableNameLabelByPositionFromVariable());
            getStoreMainTableNameLabelByPositionFromVariable().click();
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    /***
     * Methods to get the texts from element
     *
     * @return String
     */
    public static String getBotNameFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTableNameLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotNameFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTableNameLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotInvestorCountFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTableInvestorCountLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotInvestorCountFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTableInvestorCountLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotRiskLevelFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTableRiskLevelLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotRiskLevelFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTableRiskLevelLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPublishedDateFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTablePublishedLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPublishedDateFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTablePublishedLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPerfomanceBacktestValueFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTablePerfomanceBacktestLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPerfomanceBacktestValueFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTablePerfomanceBacktestLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPerfomanceAllTimeValueFromStoreMainTableByPosition(int position) {
        try {
            return getStoreMainTablePerfomanceAllTimeLabelByPosition(position).getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    public static String getBotPerfomanceAllTimeValueFromStoreMainTableByPositionFromVariable() {
        try {
            return getStoreMainTablePerfomanceAllTimeLabelByPositionFromVariable().getText().toString();
        } catch (Throwable ex) {
            return "";
        }
    }

    /***
     * Asserts methods
     */
    public static void saveBotFromPositionOneForFutureTests() {
        BOT_NAME = "";
        BOT_NAME = getBotNameFromStoreMainTableByPosition(1);
    }

    public static void verifyThatBotPresentInListOfBotsOnStorePage(String botName, String fileName) {
        boolean flag = false;

        int count = getStoreMainTableRowsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameFromStoreMainTableByPosition(i);
            if (name.contains(botName)) {
                flag = true;
                STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = i;
                break;
            }
        }

        if (!flag) {
            PrizmaDriver.getScreenshot(fileName);
        }
    }

    public static void verifyThatBotFromVariablePresentInListOfBotsOnStorePage() {
        boolean flag = false;

        int count = getStoreMainTableRowsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameFromStoreMainTableByPosition(i);
            if (name.contains(BOT_NAME)) {
                flag = true;
                STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = i;
                break;
            }
        }

        if (!flag) {
            Assert.fail("Looks like that something when wrong as bot was not present in bots list on Store page when should be there. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatBotNotPresentInListOfBotsOnStorePage(String botName, String fileName) {
        boolean flag = false;

        int count = getStoreMainTableRowsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameFromStoreMainTableByPosition(i);
            if (name.contains(botName)) {
                flag = true;
                STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = i;
                break;
            }
        }

        if (flag) {
            PrizmaDriver.getScreenshot(fileName);
        }
    }

    public static void verifyThatBotFromVariableNotPresentInListOfBotsOnStorePage(String fileName) {
        boolean flag = false;

        int count = getStoreMainTableRowsCount();

        for (int i = 1; i <= count; i++) {
            String name = getBotNameFromStoreMainTableByPosition(i);
            if (name.contains(BOT_NAME)) {
                flag = true;
                STORE_TABLE_ROW_POSITION_FOR_FUTURE_TESTS = i;
                break;
            }
        }

        if (flag) {
            PrizmaDriver.getScreenshot(fileName);
        }
    }

    public static void verifyThatCreatedByMeCheckboxIsEnabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        WebElement checkbox = getCreatedByMeCheckboxChecked();
        Assert.assertTrue("Looks like that something when wrong as created by me checkbox was not enabled when should be. Check video and logs, or something was changed on Prizma DD side.", checkbox.isDisplayed());
    }

    public static void verifyThatCreatedByMeCheckboxIsDisabled() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        int size = getCreatedByMeCheckboxUncheckedCount();

        if (size <= 0) {
            Assert.fail("Looks like that something when wrong as created by me checkbox was not disabled when should be. Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatCreatedByMeCheckboxIsPresent() {
        Assert.assertTrue("Element is not displayed",PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(CREATED_BY_ME_CHECKBOX)).isDisplayed());
    }

    public static void verifyThatCreatedByMeCheckboxIsNotPresent() {
        int size = getCreatedByMeCheckboxCount();
        Assert.assertTrue("Element is displayed", (size == 0));
    }

    public static void verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(int validationCount) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        int count = getStoreMainTableRowsCount();

        if (count != validationCount) {
            Assert.fail("Looks like that something when wrong as bots count on the page was incorrect after apply filter. Count was: " + count + ", when tests expect count: " + validationCount + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilterAndIsNotMoreThenMax(int validationCount) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        int count = getStoreMainTableRowsCount();

        if (count > validationCount) {
            Assert.fail("Looks like that something when wrong as bots count on the page was incorrect after apply filter. Count was: " + count + ", when tests expect count: " + validationCount + ". Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameInStoreMainTableByPosition(String botName, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotNameFromStoreMainTableByPosition(position), containsString(botName));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position was: " + getBotNameFromStoreMainTableByPosition(position) + " but expected result should be: " + botName + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameFromVariableInStoreMainTableByPositionFromVariable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotNameFromStoreMainTableByPositionFromVariable(), containsString(BOT_NAME));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position was: " + getBotNameFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + BOT_NAME + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotNameInStoreMainTableByPositionFromVariable(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotNameFromStoreMainTableByPositionFromVariable(), containsString(botName));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot name on position was: " + getBotNameFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botName + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotInvestorCountInStoreMainTableByPosition(String botInvestorCount, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotInvestorCountFromStoreMainTableByPosition(position), containsString(botInvestorCount));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot investor count on position was: " + getBotInvestorCountFromStoreMainTableByPosition(position) + " but expected result should be: " + botInvestorCount + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotInvestorCountInStoreMainTableByPositionFromVariable(String botInvestorCount) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotInvestorCountFromStoreMainTableByPositionFromVariable(), containsString(botInvestorCount));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot investor count on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botInvestorCount + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotRiskLevelInStoreMainTableByPosition(String botRiskLevel, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotRiskLevelFromStoreMainTableByPosition(position), containsString(botRiskLevel));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot risk level on position was: " + getBotInvestorCountFromStoreMainTableByPosition(position) + " but expected result should be: " + botRiskLevel + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotRiskLevelInStoreMainTableByPositionFromVariable(String botRiskLevel) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotRiskLevelFromStoreMainTableByPositionFromVariable(), containsString(botRiskLevel));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot risk level on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botRiskLevel + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPublishedDateInStoreMainTableByPosition(String botPublishedDate, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPublishedDateFromStoreMainTableByPosition(position), containsString(botPublishedDate));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot published date on position was: " + getBotInvestorCountFromStoreMainTableByPosition(position) + " but expected result should be: " + botPublishedDate + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPublishedDateAutogeneratedInStoreMainTableByPositionFromVariable() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        String temp = PrizmaUtils.getTodayDateAsString();

        try {
            assertThat(getBotPublishedDateFromStoreMainTableByPositionFromVariable(), containsString(temp));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot published date on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + temp + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPublishedDateInStoreMainTableByPositionFromVariable(String botPublishedDate) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPublishedDateFromStoreMainTableByPositionFromVariable(), containsString(botPublishedDate));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot published date on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botPublishedDate + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPerfomanceBacktestInStoreMainTableByPosition(String botPerfomanceBacktestValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPerfomanceBacktestValueFromStoreMainTableByPosition(position), containsString(botPerfomanceBacktestValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot perfomance backtest value on position was: " + getBotInvestorCountFromStoreMainTableByPosition(position) + " but expected result should be: " + botPerfomanceBacktestValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPerfomanceBacktestInStoreMainTableByPositionFromVariable(String botPerfomanceBacktestValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPerfomanceBacktestValueFromStoreMainTableByPositionFromVariable(), containsString(botPerfomanceBacktestValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot perfomance backtest value on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botPerfomanceBacktestValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPerfomanceAllTimeInStoreMainTableByPosition(String botAllTimeValue, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPerfomanceAllTimeValueFromStoreMainTableByPosition(position), containsString(botAllTimeValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot perfomance all time value on position was: " + getBotInvestorCountFromStoreMainTableByPosition(position) + " but expected result should be: " + botAllTimeValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void verifyBotPerfomanceAllTimeInStoreMainTableByPositionFromVariable(String botAllTimeValue) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);

        try {
            assertThat(getBotPerfomanceAllTimeValueFromStoreMainTableByPositionFromVariable(), containsString(botAllTimeValue));
        } catch (Throwable ex) {
            Assert.fail("Looks like that something when wrong as Bot perfomance all time value on position was: " + getBotInvestorCountFromStoreMainTableByPositionFromVariable() + " but expected result should be: " + botAllTimeValue + " Check video and logs, or something was changed on Prizma DD side.");
        }
    }

    public static void typeSearchRequest(String searchRequest) {
        getSearchInput().click();
        getSearchInput().sendKeys(searchRequest);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public static void verifySearchResultIsDisplayed(String searchResult) {
        String result = getSearchHighlightedResult().getText();
        Assert.assertEquals("Search result is not equal to search request", searchResult, result);
    }

    public static void verifySearchNoResultDisplayed() {
        String result = getSearchNoResult().getText();
        Assert.assertEquals("Search result is not equal to No result", "NO SEARCH RESULTS", result);
    }

    public static WebElement getElementByCssSelector(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.cssSelector(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.cssSelector(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }

    public static WebElement getElementByXpath(String locator) {
        WebElement webElement;
        if (PrizmaDriver.isSelenoid()) {
            webElement = PrizmaDriver.getCurrentDriver().findElement(By.xpath(locator));
        } else {
            webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.xpath(locator));
        }
        if (webElement.isDisplayed()) {
            //
        } else {
            Assert.fail("Looks like that something when wrong as element with locator: " + locator + " is not displayed. Check video and logs, or something was changed on DD side.");
        }
        return webElement;
    }
}

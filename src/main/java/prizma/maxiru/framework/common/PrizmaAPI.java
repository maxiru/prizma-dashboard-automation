package prizma.maxiru.framework.common;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PrizmaAPI {

    private static HttpURLConnection conn;

    public static String sendRequest(String method, String requestUrl, String body) {
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();

        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();

            // Request setup
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setConnectTimeout(50000);// 5000 milliseconds = 5 seconds
            conn.setReadTimeout(50000);
            conn.setDoOutput(true);

            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            // Test if the response from the server is successful
            int status = conn.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            } else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            }
            System.out.println(responseContent.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return responseContent.toString();
    }

    public static String getToken() {
        String json = "{\"email\":\"aaa@aaa.com\",\"password\":\"aaa\"}";
        String requestUrl = "https://dev.prizma-api.simis.ai/auth/signin/";

        String response = sendRequest("POST", requestUrl, json);
        JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
        String token = String.valueOf(jsonObject.get("token"));
        System.out.println(token);
        return token;
    }


    public static void deleteAutotestUsers() throws IOException {
        String url = "https://dev.prizma-api.simis.ai/auth/delete-autotest/?token=46f52d83-2b2c-4521-9ab8-cc5921b186d0";
        sendRequest("DELETE", url, "");
    }


}


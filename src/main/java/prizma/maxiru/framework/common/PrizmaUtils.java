package prizma.maxiru.framework.common;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static prizma.maxiru.framework.common.PrizmaDriver.waitTillPageLoaded;

public class PrizmaUtils {

    //Method to generate random word
    public static String generateRandomWord() {
        String randomString;
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        randomString = new String(sb);
        return randomString;
    }

    //Method to get current date as String
    public static String getCurrentDateAsString(String format) {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = dateFormat.format(currentDate);

        return data;
    }

    //Method to get today date as String
    public static String getTodayDateAsString() {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = dateFormat.format(currentDate);

        return data;
    }


    //Method to get next date as String
    public static String getNextDayAsString(String format) {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 1);
        currentDate = c.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = dateFormat.format(currentDate);

        return data;
    }

    //Add emoji reg exp
    private static boolean isEmoji(String message) {
        return message.matches("(?:[\uD83C\uDF00-\uD83D\uDDFF]|[\uD83E\uDD00-\uD83E\uDDFF]|" +
                "[\uD83D\uDE00-\uD83D\uDE4F]|[\uD83D\uDE80-\uD83D\uDEFF]|" +
                "[\u2600-\u26FF]\uFE0F?|[\u2700-\u27BF]\uFE0F?|\u24C2\uFE0F?|" +
                "[\uD83C\uDDE6-\uD83C\uDDFF]{1,2}|" +
                "[\uD83C\uDD70\uD83C\uDD71\uD83C\uDD7E\uD83C\uDD7F\uD83C\uDD8E\uD83C\uDD91-\uD83C\uDD9A]\uFE0F?|" +
                "[\u0023\u002A\u0030-\u0039]\uFE0F?\u20E3|[\u2194-\u2199\u21A9-\u21AA]\uFE0F?|[\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55]\uFE0F?|" +
                "[\u2934\u2935]\uFE0F?|[\u3030\u303D]\uFE0F?|[\u3297\u3299]\uFE0F?|" +
                "[\uD83C\uDE01\uD83C\uDE02\uD83C\uDE1A\uD83C\uDE2F\uD83C\uDE32-\uD83C\uDE3A\uD83C\uDE50\uD83C\uDE51]\uFE0F?|" +
                "[\u203C\u2049]\uFE0F?|[\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE]\uFE0F?|" +
                "[\u00A9\u00AE]\uFE0F?|[\u2122\u2139]\uFE0F?|\uD83C\uDC04\uFE0F?|\uD83C\uDCCF\uFE0F?|" +
                "[\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA]\uFE0F?)+");
    }

    //Method to detect emojis
    public static int detectEmojis(String message) {
        int len = message.length(), NumEmoji = 0;
        if (isEmoji(message)) {
            for (int i = 0; i < len; i++) {
                if (isEmoji(message.charAt(i) + "")) {
                    NumEmoji++;
                } else {
                    if (i < (len - 1)) {
                        if (Character.isSurrogatePair(message.charAt(i), message.charAt(i + 1))) {
                            i += 1;
                            NumEmoji++;
                        }
                    }
                }
            }
            return NumEmoji;
        }
        return 0;
    }

    //Method to get next month as String
    public static String getNextMonthAsString(String format) {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.MONTH, 1);
        currentDate = c.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = dateFormat.format(currentDate);

        return data;
    }

    //Method to get future date as String
    public static String getFutureDayAsString(String format, int count) {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, count);
        currentDate = c.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = dateFormat.format(currentDate);

        return data;
    }

    //Method to generate random number
    public static int generateRandomNumber(int number) {
        Random random = new Random();
        return 0 + random.nextInt(number);
    }

    //Pattern to extract text between parenthesis
    public static List<String> extractTextBetweenParenthesis(String text) {
        List<String> matchList = new ArrayList<String>();
        Pattern regex = Pattern.compile("\\((.*?)\\)");
        Matcher regexMatcher = regex.matcher(text);

        while (regexMatcher.find()) {
            matchList.add(regexMatcher.group(1));
        }
        return matchList;
    }

    public static void openWebsite(String link) {
        try {
            if (PrizmaDriver.isSelenoid()) {
                PrizmaDriver.getCurrentDriver().navigate().to(link);
            } else {
                PrizmaDriver.getCurrentRemoveWebDriver().navigate().to(link);
            }

            waitTillPageLoaded(120);
        } catch (Throwable ex) {
            //ignore
        }
    }

    public static boolean checkIsBtnEnabled(WebElement element, boolean status) {
        boolean flag = element.isEnabled();
        assertThat(flag, equalTo(status));
        return flag;
    }

    public static boolean justCheckBtnStatus(WebElement element) {
        boolean flag = element.isEnabled();
        return flag;
    }

    public static String switchToWorkedWindow() {
        if (PrizmaDriver.isSelenoid()) {
            // Store the current window handle
            String winHandleBefore = PrizmaDriver.getCurrentDriver().getWindowHandle();

            // Switch to new window opened
            for (String winHandle : PrizmaDriver.getCurrentDriver().getWindowHandles()) {
                PrizmaDriver.getCurrentDriver().switchTo().window(winHandle);
            }

            if (PrizmaDriver.isSafariBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                PrizmaDriver.getCurrentDriver().manage().window().maximize();
            }

            return winHandleBefore;
        } else {
            // Store the current window handle
            String winHandleBefore = PrizmaDriver.getCurrentRemoveWebDriver().getWindowHandle();

            // Switch to new window opened
            for (String winHandle : PrizmaDriver.getCurrentRemoveWebDriver().getWindowHandles()) {
                PrizmaDriver.getCurrentRemoveWebDriver().switchTo().window(winHandle);
            }

            if (PrizmaDriver.isSafariBrowser()) {
                PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
                PrizmaDriver.getCurrentRemoveWebDriver().manage().window().maximize();
            }

            return winHandleBefore;
        }
    }

    public static void switchToFrame(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            //Switch to Default Frame
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
            PrizmaDriver.getCurrentDriver().switchTo().defaultContent();

            //Switch to frame
            PrizmaDriver.getCurrentDriver().switchTo().frame(element);
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        } else {
            //Switch to Default Frame
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
            PrizmaDriver.getCurrentRemoveWebDriver().switchTo().defaultContent();

            //Switch to frame
            PrizmaDriver.getCurrentRemoveWebDriver().switchTo().frame(element);
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        }
    }

    public static void switchToInnerFrame(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            //Switch to frame
            PrizmaDriver.getCurrentDriver().switchTo().frame(element);
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        } else {
            //Switch to frame
            PrizmaDriver.getCurrentRemoveWebDriver().switchTo().frame(element);
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        }
    }

    public static void switchBackToDefaultFrame() {
        PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);

        if (PrizmaDriver.isSelenoid()) {
            PrizmaDriver.getCurrentDriver().switchTo().defaultContent();
        } else {
            PrizmaDriver.getCurrentRemoveWebDriver().switchTo().defaultContent();
        }

        PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
    }

    public static void switchToBackWorkedWindow(String winHandleBefore) {
        if (PrizmaDriver.isSelenoid()) {
            // Close the new window, if that window no more required
            PrizmaDriver.getCurrentDriver().close();

            // Switch back to original browser (first window)
            PrizmaDriver.getCurrentDriver().switchTo().window(winHandleBefore);
        } else {
            // Close the new window, if that window no more required
            PrizmaDriver.getCurrentRemoveWebDriver().close();

            // Switch back to original browser (first window)
            PrizmaDriver.getCurrentRemoveWebDriver().switchTo().window(winHandleBefore);
        }
    }

    public static void scrollToElement(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].scrollIntoView();", element);
        } else {
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].scrollIntoView();", element);
        }
    }

    public static void moveToElement(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            Actions actions = new Actions(PrizmaDriver.getCurrentDriver());
            actions.moveToElement(element);
            actions.perform();
        } else {
            Actions actions = new Actions(PrizmaDriver.getCurrentRemoveWebDriver());
            actions.moveToElement(element);
            actions.perform();
        }
    }

    //Method to create file
    public static void createFile(String email) {
        try {
            FileWriter writer = new FileWriter("Email.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(email);
            bufferedWriter.newLine();

            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Method to send text into input field for Firefox
    public static void setValue(WebElement element, String value) {
        if (PrizmaDriver.isSelenoid()) {
            WebElement wb = element;
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentDriver();
            jse.executeScript("arguments[0].setAttribute('value', arguments[1])", wb, value);
        } else {
            WebElement wb = element;
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver();
            jse.executeScript("arguments[0].setAttribute('value', arguments[1])", wb, value);
        }

        PrizmaDriver.waitFor(PrizmaConstants.LESS_WAIT_TIME);
    }

    //Method to make upload file btn visible
    public static void makeUploadButtonVisible(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.visibility = 'visible';", element);
        } else {
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.visibility = 'visible';", element);
        }
    }

    //Method to scroll up
    public static void scrollUp() {
        if (PrizmaDriver.isSelenoid()) {
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentDriver();
            jse.executeScript("window.scrollBy(0,-250)");
        } else {
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver();
            jse.executeScript("window.scrollBy(0,-250)");
        }
    }

    //Method to make upload file btn visible
    public static void makeElementHidden(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.display = 'none';", element);
        } else {
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.display = 'none';", element);
        }
    }

    //Method to make menu btn visible
    public static void makeMenuVisible(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.display = 'block';", element);
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.overflow = 'visible';", element);
        } else {
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.display = 'block';", element);
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.overflow = 'visible';", element);
        }
    }

    //Method to make menu btn hidden
    public static void makeMenuHidden(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.display = 'none';", element);
            ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].style.overflow = 'hidden';", element);
        } else {
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.display = 'none';", element);
            ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].style.overflow = 'hidden';", element);

        }
    }

    //Method to make upload file btn visible
    public static void scrollToElementOnPage(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            try {
                ((JavascriptExecutor) PrizmaDriver.getCurrentDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
                Thread.sleep(500);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                ((JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
                Thread.sleep(500);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to scrollDown
    public static void scrollDown(int pixel) {
        if (PrizmaDriver.isSelenoid()) {
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver();
            jse.executeScript("window.scrollBy(0," + pixel + ")");
        } else {
            JavascriptExecutor jse = (JavascriptExecutor) PrizmaDriver.getCurrentRemoveWebDriver();
            jse.executeScript("window.scrollBy(0," + pixel + ")");
        }
    }

    //Method to handle
    public static void handlePopUpAboutUnsavedChanges() {
        if (PrizmaDriver.isSelenoid()) {
            try {
                Alert alert = PrizmaDriver.getCurrentDriver().switchTo().alert();
                alert.accept();
                PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
            } catch (Throwable ex) {
                //ignore
            }
        } else {
            try {
                Alert alert = PrizmaDriver.getCurrentRemoveWebDriver().switchTo().alert();
                alert.accept();
                PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
            } catch (Throwable ex) {
                //ignore
            }
        }
    }

    //Method for hold click
    public static void clickHold(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            Actions action = new Actions(PrizmaDriver.getCurrentDriver());
            action.clickAndHold(element).build().perform();
        } else {
            Actions action = new Actions(PrizmaDriver.getCurrentRemoveWebDriver());
            action.clickAndHold(element).build().perform();
        }

        PrizmaDriver.waitFor(PrizmaConstants.HOLD_WAIT_TIME);
    }

    //Method to refresh page
    public static void refreshPage() {
        if (PrizmaDriver.isSelenoid()) {
            PrizmaDriver.getCurrentDriver().navigate().refresh();
        } else {
            PrizmaDriver.getCurrentRemoveWebDriver().navigate().refresh();
        }

        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    // Operating systems.
    public enum OS {
        WINDOWS, LINUX, MAC, SOLARIS
    }

    public static OS os = null;

    public static OS getOS() {
        if (os == null) {
            String operSys = System.getProperty("os.name").toLowerCase();
            if (operSys.contains("win")) {
                os = OS.WINDOWS;
            } else if (operSys.contains("nix") || operSys.contains("nux") || operSys.contains("aix")) {
                os = OS.LINUX;
            } else if (operSys.contains("mac")) {
                os = OS.MAC;
            } else if (operSys.contains("sunos")) {
                os = OS.SOLARIS;
            }
        }
        return os;
    }

    //Method to set focus in Firefox browser
    public static void setFocusInFirefoxBrowser() {
        switch (PrizmaUtils.getOS()) {
            case WINDOWS:
                break;
            case LINUX:
                break;
            case MAC:
                try {
                    Runtime.getRuntime().exec("osascript " + "src/main/java/scripts/activateFirefox2.scpt");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
        }

        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to set focus in Chrome browser
    public static void setFocusInChromeBrowser() {
        switch (PrizmaUtils.getOS()) {
            case WINDOWS:
                break;
            case LINUX:
                break;
            case MAC:
                try {
                    Runtime.getRuntime().exec("osascript " + "src/main/java/scripts/activateChrome2.scpt");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to set focus in Safari browser
    public static void setFocusInSafariBrowser() {
        switch (PrizmaUtils.getOS()) {
            case WINDOWS:
                break;
            case LINUX:
                break;
            case MAC:
                try {
                    Runtime.getRuntime().exec("osascript " + "src/main/java/scripts/activateSafari2.scpt");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
        }
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to set focus after open browser and video recorder
    public static void setFocusInBrowser() {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
    }

    //Method to set focus after open browser and video recorder
    public static void setFocusInBrowserSelenoid() {
        //ignore
    }

    //Method to get current URL
    public static String getCurrentURL() {
        if (PrizmaDriver.isSelenoid()) {
            return PrizmaDriver.getCurrentDriver().getCurrentUrl().toString();
        } else {
            return PrizmaDriver.getCurrentRemoveWebDriver().getCurrentUrl().toString();
        }
    }

    //Method to the delete the download folder
    public static void deleteTheDownloadFolder() {
        try {
            FileUtils.deleteDirectory(new File(PrizmaConstants.DOWNLOAD_FILES_FOLDER_PATH));
        } catch (Throwable ex) {
            //ignore
        }
    }

    //Method to the create the download folder
    public static void createTheDownloadFolder() {
        new File(PrizmaConstants.DOWNLOAD_FILES_FOLDER_PATH).mkdir();
    }

    //Method to wait till file download
    public static void waitTillFileDownload(String fileName, long timeoutSeconds) {
        FluentWait<WebDriver> wait = new FluentWait<>(PrizmaDriver.getCurrentDriver())
                .withTimeout(timeoutSeconds, SECONDS)
                .pollingEvery(10, SECONDS)
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((x) -> {
            File[] files = new File(PrizmaConstants.DOWNLOAD_FILES_FOLDER_PATH).listFiles();
            for (File file : files) {
                if (file.getName().equals(fileName)) {
                    return true;
                }
            }
            return false;
        });
    }

    //Method to the check that folder is not empty after download some files
    public static void checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaDashboard() {
        boolean flag;
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(PrizmaConstants.DOWNLOAD_FILES_FOLDER_PATH))) {
            flag = dirStream.iterator().hasNext();
        } catch (IOException e) {
            flag = true;
        }
        if (!flag) {
            Assert.fail("Looks like that something when wrong as download folder was empty after download from KWLocPortal some files. Check video and logs.");
        }
    }

    //Method to convert Profit WebElements list to Float list
    public static List<Float> profitWebElementsToFloat(List<WebElement> elementList){
        List<Float> floatList = new ArrayList<Float>();
        for (WebElement element : elementList ) {
            floatList.add(Float.parseFloat(element.getText().replace(" $ ", "")));
        }
        return floatList;
    }

    //Method to convert Amounts WebElements list to Float list
    public static List<Float> amountWebElementsToFloat(List<WebElement> elementList){
        List<String> strings = new ArrayList<String>();
        for(WebElement e : elementList) {
            strings.add(e.getText().replace("$",""));
        }

        while (strings.contains("")) {
            strings.remove("");
        }

        List<Float> floatList = new ArrayList<Float>();
        for (String string : strings ) {
            floatList.add(Float.parseFloat(string));
        }

        return floatList;

    }

    //Method to convert Percentage WebElements list to Float list
    public static List<Float> percentageWebElementsToFloat(List<WebElement> elementList){
        List<String> strings = new ArrayList<String>();
        for(WebElement e : elementList) {
            strings.add(e.getText().replace("%",""));
        }

        while (strings.contains("")) {
            strings.remove("");
        }

        List<Float> floatList = new ArrayList<Float>();
        for (String string : strings ) {
            floatList.add(Float.parseFloat(string));
        }

        return floatList;
    }

    //Method to calculate sum of all float elements in List
    public static float floatListSum(List<Float> list) {
        float sum = 0;
        for (float i: list) {
            sum += i;
        }

        return sum;
    }
}

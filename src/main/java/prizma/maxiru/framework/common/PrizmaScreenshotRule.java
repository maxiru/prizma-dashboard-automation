package prizma.maxiru.framework.common;

import org.junit.AssumptionViolatedException;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import static java.lang.System.out;

public class PrizmaScreenshotRule implements MethodRule {

    public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, Object o) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    statement.evaluate();
                } catch (AssumptionViolatedException t) {
                    //ignore
                } catch (TimeoutException t) {
                    captureScreenshot(frameworkMethod.getName().toString());
                    throw t;
                } catch (AssertionError t) {
                    captureScreenshot(frameworkMethod.getName().toString());
                    throw t;
                } catch (Exception t) {
                    captureScreenshot(frameworkMethod.getName().toString());
                    throw t;
                }
            }
        };
    }

    public void captureScreenshot(String methodName) {
        try {
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
            new File(System.getProperty("user.dir") + "/screenshots").mkdirs();
            FileOutputStream outPutFile = new FileOutputStream(System.getProperty("user.dir") + "/screenshots/" + methodName + "_" + new Date().getTime() + ".jpg");
            if (PrizmaDriver.isSelenoid()) {
                outPutFile.write(((TakesScreenshot) PrizmaDriver.getCurrentDriver()).getScreenshotAs(OutputType.BYTES));
            } else {
                outPutFile.write(((TakesScreenshot) PrizmaDriver.getCurrentRemoveWebDriver()).getScreenshotAs(OutputType.BYTES));
            }
            out.close();
            PrizmaDriver.waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        } catch (Exception ex) {
            //ignore
        }
    }
}

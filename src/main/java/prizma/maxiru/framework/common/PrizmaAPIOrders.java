package prizma.maxiru.framework.common;

import app.PrizmaInvestorBotDetailsPage;
import io.restassured.RestAssured;
import io.restassured.response.ResponseBody;
import org.testng.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static app.PrizmaInvestorBotDetailsPage.*;
import static io.restassured.RestAssured.given;
import static prizma.maxiru.framework.common.PrizmaConstants.EMAIL_FOR_INVESTOR_STATISTICS_TESTS;
import static prizma.maxiru.framework.common.PrizmaConstants.PASSWORD_FOR_INVESTOR_STATISTICS_TESTS;
import static prizma.maxiru.framework.common.PrizmaUtils.refreshPage;

public class PrizmaAPIOrders {

    static String investorBotConfiguration = "27";
    static String botToken = "39522850-2224-45fa-9feb-14712c780daa";

    public static void sendBuyAndSellMarketOrdersAndVerifyThatDataIsCorrect() {
        RestAssured.baseURI = "https://dev.prizma-api.simis.ai";

        //Send Buy Market order request
        Map<String, Object> map = new HashMap<>();
        map.put("orderType", "B");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("indicativePrice", 22330.0);
        map.put("size", "0.001");

        ResponseBody buyMarketResponse = given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(map)
                .log().all()
                .when()
                .post("/v1/trader/real/"+ investorBotConfiguration + "/market-order/")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response().body();

        String size =String.valueOf(Float.parseFloat(buyMarketResponse.path("size")));
        String date = buyMarketResponse.path("fulfilled_at").toString().substring(0, 10);

        SimpleDateFormat actualFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat expectedFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date1 = null;
        try {
            date1 = actualFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expectedDate = expectedFormat.format(date1);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(expectedDate, getOrderDateInTableByPosition(1), "Buy Dates are not the same");
        Assert.assertEquals(size, String.valueOf(getSizeInTableByPosition(1)), "Buy Sizes are not the same");

        //Send Sell Market order request
        Map<String, Object> mapSell = new HashMap<>();
        map.put("orderType", "S");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("indicativePrice", 22330.0);
        map.put("size", size);

        ResponseBody sellMarketResponse = given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(mapSell)
                .log().all()
                .when()
                .post("/v1/trader/real/"+ investorBotConfiguration + "/market-order/")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response().body();

        String sizeSell = String.valueOf(Float.parseFloat(sellMarketResponse.path("size")));
        String dateSell = sellMarketResponse.path("fulfilled_at").toString().substring(0, 10);

        Date date2 = null;
        try {
            date2 = actualFormat.parse(dateSell);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expectedDateSell = expectedFormat.format(date2);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(expectedDateSell, getOrderDateInTableByPosition(1), "Sell Dates are not the same");
        Assert.assertEquals(sizeSell,String.valueOf(getSizeInTableByPosition(1)), "Sell Sizes are not the same");

    }

    public static void sendBuyAndSellLimitOrdersAndVerifyThatDataIsCorrect() {
        RestAssured.baseURI = "https://dev.prizma-api.simis.ai";
        String limitPrice = "40000";

        //Send Buy Limit order request
        Map<String, Object> map = new HashMap<>();
        map.put("orderType", "lb");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("limitPrice", limitPrice);
        map.put("size", "0.001");

        ResponseBody buyLimitResponse = given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(map)
                .log().all()
                .when()
                .post("/v1/trader/real/"+ investorBotConfiguration + "/limit-order/")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response().body();

        String size = String.valueOf(Float.parseFloat(buyLimitResponse.path("size")));
        String date = buyLimitResponse.path("fulfilled_at").toString().substring(0, 10);

        SimpleDateFormat actualFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat expectedFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date1 = null;
        try {
            date1 = actualFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expectedDate = expectedFormat.format(date1);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(expectedDate, getOrderDateInTableByPosition(1), "Buy Dates are not the same");
        Assert.assertEquals(size, String.valueOf(getSizeInTableByPosition(1)), "Buy Sizes are not the same");
        Assert.assertEquals(limitPrice, String.valueOf(getLimitPriceInTableByPosition(1)), "Limit Prices are not the same");

        //Send Sell Market order request
        Map<String, Object> mapSell = new HashMap<>();
        map.put("orderType", "S");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("indicativePrice", 22330.0);
        map.put("size", size);

        ResponseBody sellResponse = given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(mapSell)
                .log().all()
                .when()
                .post("/v1/trader/real/"+ investorBotConfiguration + "/market-order/")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response().body();

        String sizeSell = String.valueOf(Float.parseFloat(sellResponse.path("size")));
        String dateSell = sellResponse.path("fulfilled_at").toString().substring(0, 10);

        Date date2 = null;
        try {
            date2 = actualFormat.parse(dateSell);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expectedDateSell = expectedFormat.format(date2);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(expectedDateSell, getOrderDateInTableByPosition(1), "Sell Dates are not the same");
        Assert.assertEquals(sizeSell,String.valueOf(getSizeInTableByPosition(1)), "Sell Sizes are not the same");

    }

    public static void sendBuyAndCancelStopLimitOrdersAndVerifyThatDataIsCorrect() {
        RestAssured.baseURI = "https://dev.prizma-api.simis.ai";
        String stopLimitPrice = "30000";

        //Send Buy Stop Limit order request
        Map<String, Object> map = new HashMap<>();
        map.put("orderType", "sb");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("limitPrice", stopLimitPrice);
        map.put("stopLimitPrice", stopLimitPrice);
        map.put("size", "0.001");

        ResponseBody buyStopLimitResponse = given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(map)
                .log().all()
                .when()
                .post("/v1/trader/real/" + investorBotConfiguration + "/stop-limit-order/")
                .then()
                .log().all()
                .statusCode(200)
                .extract().response().body();

        String size = String.valueOf(Float.parseFloat(buyStopLimitResponse.path("size")));
        String date = buyStopLimitResponse.path("fulfilled_at").toString().substring(0, 10);
        String orderId = buyStopLimitResponse.path("order_id");

        SimpleDateFormat actualFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat expectedFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date1 = null;
        try {
            date1 = actualFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expectedDate = expectedFormat.format(date1);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(size, String.valueOf(getSizeInTableByPosition(1)), "Buy Sizes are not the same");
        Assert.assertEquals(stopLimitPrice, String.valueOf(getLimitPriceInTableByPosition(1)), "Limit Prices are not the same");
        PrizmaInvestorBotDetailsPage.verifyThatOpenStatusIsDisplayedInPositionInOrdersTable(1);
        PrizmaInvestorBotDetailsPage.verifyThatOrderSideIsCorrectInPositionInOrdersTable(1, "Buy");
        PrizmaInvestorBotDetailsPage.verifyThatStopLimitOrderIsDisplayedInPositionInOrdersTable(1);

        //Send Cancel order request
         given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .header("order_id", orderId)
                .header("pair", "BTCUSDT")
                .log().all()
                .when()
                .get("/v1/trader/real/"+ investorBotConfiguration + "/cancel-order/")
                .then()
                .log().all()
                .statusCode(200);

        refreshPage();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        clickOnPositionBlock();

        Assert.assertEquals(expectedDate, getOrderDateInTableByPosition(1), "Cancel Dates are not the same");
        Assert.assertEquals(size,String.valueOf(getSizeInTableByPosition(1)), "Cancel Sizes are not the same");
        PrizmaInvestorBotDetailsPage.verifyThatCanceledStatusIsDisplayedInPositionInOrdersTable(1);
        PrizmaInvestorBotDetailsPage.verifyThatOrderSideIsCorrectInPositionInOrdersTable(1, "Buy");
        PrizmaInvestorBotDetailsPage.verifyThatCanceledStatusIsDisplayedInPositionInOrdersTable(1);

    }

    public static void sendRejectedOrderAndVerifyThatDataIsCorrect() {
        RestAssured.baseURI = "https://dev.prizma-api.simis.ai";

        //Send Buy Market order with small size for receiving Rejected request
        Map<String, Object> map = new HashMap<>();
        map.put("orderType", "B");
        map.put("positionType", "l");
        map.put("pair", "BTCUSDT");
        map.put("recommendation", "20");
        map.put("indicativePrice", 22330.0);
        map.put("size", "0.0000001");

        given()
                .auth().preemptive().basic(EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS)
                .contentType("application/json")
                .accept("application/json")
                .header("token", botToken)
                .body(map)
                .log().all()
                .when()
                .post("/v1/trader/real/"+ investorBotConfiguration + "/market-order/")
                .then()
                .log().all()
                .statusCode(200);

    }

}


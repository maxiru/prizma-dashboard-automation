package prizma.maxiru.framework.common;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static java.lang.System.out;

public class PrizmaDriver {

    private static WebDriver driver = null;
    private static RemoteWebDriver remoteWebDriver = null;

    public static String websitelink;
    public static String browser;
    public static String selenoid;

    //Definite wait period
    public static void waitFor(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    //Method for implicit wait
    public static void implicitWait(int sec) {
        if (PrizmaDriver.isSelenoid()) {
            driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
        } else {
            remoteWebDriver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
        }
    }

    //Method for implicit wait
    public static void waitToBeClickable(WebElement element) {
        if (PrizmaDriver.isSelenoid()) {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } else {
            WebDriverWait wait = new WebDriverWait(remoteWebDriver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
    }

    public static void waitUntilPrizmaLogoNotDismiss() {
        boolean flag = true;
        WebElement webElement = null;


        if (PrizmaDriver.isSelenoid()) {
            try {
                webElement = PrizmaDriver.getCurrentDriver().findElement(By.id("Spinner"));
            } catch (Throwable ex) {
                //ignore
            }
        } else {
            try {
                webElement = PrizmaDriver.getCurrentRemoveWebDriver().findElement(By.id("Spinner"));
            } catch (Throwable ex) {
                //ignore
            }
        }

        while (flag) {
            try {
                webElement.isDisplayed();
            } catch (Throwable ex) {
                flag = false;
            }
        }
    }

    //Explicit wait function
    public static void explicitWait(By by, int sec) {
        if (PrizmaDriver.isSelenoid()) {
            WebDriverWait wait = new WebDriverWait(driver, sec);
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } else {
            WebDriverWait wait = new WebDriverWait(remoteWebDriver, sec);
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        }
    }

    //Wait driver for toast message
    public static void waitDriver(By by, int sec) {
        if (PrizmaDriver.isSelenoid()) {
            WebDriverWait wait = new WebDriverWait(driver, sec);
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } else {
            WebDriverWait wait = new WebDriverWait(remoteWebDriver, sec);
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
        }
    }

    //Wait till the page is loaded
    public static void waitTillPageLoaded(int sec){
        if (PrizmaDriver.isSelenoid()) {
            try {
                driver.manage().timeouts().pageLoadTimeout(sec, TimeUnit.SECONDS);
            } catch (Throwable ex) {
                Assert.fail("Looks like that something when wrong as webpage not load. Check video and logs, or something was changed on DD side.");
            }
        } else {
            try {
                remoteWebDriver.manage().timeouts().pageLoadTimeout(sec, TimeUnit.SECONDS);
            } catch (Throwable ex) {
                Assert.fail("Looks like that something when wrong as webpage not load. Check video and logs, or something was changed on DD side.");
            }
        }
    }

    //Method for setting capabilities and launching the app on the device/emulator
    public static WebDriver setup(String platform, String link) throws MalformedURLException {
        websitelink = link;
        browser = platform;

        selenoid = "selenium";

        //Set Webdriver
        if (platform.equals(PrizmaConstants.PLATFORM_FIREFOX)) {
            System.setProperty("webdriver.gecko.driver", "src/main/java/drivers/geckodriver");
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("marionette", true);
            driver = new FirefoxDriver(capabilities);
            driver.manage().window().setSize(new Dimension(1920, 1080));
            //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } else if (platform.equals(PrizmaConstants.PLATFORM_CHROME)) {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            switch (PrizmaUtils.getOS()) {
                case WINDOWS:
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver.exe");
                    break;
                case LINUX:
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/linux/chromedriver");
                    break;
                case MAC:
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver");
                    break;
            }
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--window-size=1920,1080");
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--disable-dev-shm-usage");
            chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));

            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            LoggingPreferences logPrefs = new LoggingPreferences();
            logPrefs.enable(LogType.BROWSER, Level.ALL);
            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
            chromeOptions.setCapability("goog:loggingPrefs", logPrefs);

            String logPath = PrizmaConstants.DOWNLOAD_FILES_FOLDER_PATH;
            PrizmaUtils.deleteTheDownloadFolder();
            PrizmaUtils.createTheDownloadFolder();

            HashMap<String, Object> chromePref = new HashMap<>();
            chromePref.put("download.default_directory", logPath);
            chromePref.put("download.prompt_for_download", "false");
            chromePref.put("credentials_enable_service", false);
            chromePref.put("profile.password_manager_enabled", false);
            chromeOptions.setExperimentalOption("prefs", chromePref);

            capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
            driver = new ChromeDriver(capabilities);
            //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } else if (platform.equals(PrizmaConstants.PLATFORM_SAFARI)) {
            driver = new SafariDriver();
            PrizmaDriver.getCurrentDriver().manage().window().maximize();
            //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
            //Also, OS should be >= 10.11 + 'Safari WebDriver extension' should be installed + 'Automation option' allowed in Safari's Develop menu
        }

        waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        return driver;
    }

    //Method for setting Selenoid capabilities and launching the browser
    public static WebDriver setupSelenoid() throws MalformedURLException {
        //for Jenkins
        //reading Gradle parameters specified with "-D"
        browser = System.getProperty("gs.platform");
        websitelink = System.getProperty("gs.link");

        //reading env variables
        String selenoidURL = System.getenv("SELENOID_URL") + "/wd/hub";
        String buildNumber = System.getenv("BUILD_NUMBER");
        String jobName = System.getenv("JOB_NAME");
        String stageName = System.getenv("STAGE_NAME"); 

        Map<String,String> labels = new HashMap<String, String>();
        labels.put("buildNumber", buildNumber);
        labels.put("jobName", jobName);

        //String selenoidURL = "http://localhost:4444/wd/hub";
        //String buildNumber = "1";
        //String jobName = "First";

        //for local testing
        //browser = "Firefox";
        //browser = "Safari";
        //browser = "Chrome";

        selenoid = "selenoid";

        String videoName = jobName+"/"+buildNumber+"/"+browser+".mp4";
        String screenSize = "1664x936";
        int videoFrameRate = 6;

        //Set Webdriver
        if (browser.equals(PrizmaConstants.PLATFORM_FIREFOX)) {
            System.setProperty("webdriver.gecko.driver","src/main/java/drivers/geckodriver");
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("marionette", true);
            capabilities.setCapability("enableVNC", true);
            capabilities.setCapability("enableVideo", true);
            capabilities.setCapability("videoScreenSize", screenSize);
            capabilities.setCapability("videoFrameRate", videoFrameRate);
            capabilities.setCapability("videoName", videoName);
            capabilities.setCapability("screenResolution", screenSize);
            capabilities.setCapability("labels", labels);
            remoteWebDriver = new RemoteWebDriver(
                    URI.create(selenoidURL).toURL(),
                    capabilities
            );
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            remoteWebDriver.manage().window().setSize(new Dimension(1664, 936));
        } else if (browser.equals(PrizmaConstants.PLATFORM_CHROME)) {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            switch (PrizmaUtils.getOS()) {
                case WINDOWS:
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver.exe");
                    break;
                case LINUX:
                    break;
                case MAC:
                    System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver");
                    break;
            }
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--kiosk");
            chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            chromeOptions.setExperimentalOption("useAutomationExtension", false);
            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
            LoggingPreferences logPrefs = new LoggingPreferences();
            logPrefs.enable(LogType.BROWSER, Level.ALL);
            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
            chromeOptions.setCapability("goog:loggingPrefs", logPrefs );
            capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
            capabilities.setCapability("enableVNC", true);
            capabilities.setCapability("enableVideo", true);
            capabilities.setCapability("videoScreenSize", screenSize);
            capabilities.setCapability("videoFrameRate", videoFrameRate);
            capabilities.setCapability("videoName", videoName);
            capabilities.setCapability("labels", labels);
            remoteWebDriver = new RemoteWebDriver(
                    URI.create(selenoidURL).toURL(),
                    capabilities
            );
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            remoteWebDriver.manage().window().setSize(new Dimension(1664, 936));
        }

        waitFor(PrizmaConstants.SHORT_WAIT_TIME);
        return remoteWebDriver;
    }

    //Method to get the current platform
    public static String getBrowserPlatform() {
        if (driver instanceof FirefoxDriver) {
            return PrizmaConstants.PLATFORM_FIREFOX;
        } else if (driver instanceof ChromeDriver) {
            return PrizmaConstants.PLATFORM_CHROME;
        } else if (driver instanceof SafariDriver) {
            return PrizmaConstants.PLATFORM_SAFARI;
        } else {
            return "";
        }
    }

    //Method to get the current platform for Selenoid tests
    public static String getBrowserPlatformForSelenoid() {
        if (browser.contains("Firefox")) {
            return PrizmaConstants.PLATFORM_FIREFOX;
        } else if (browser.contains("Chrome")) {
            return PrizmaConstants.PLATFORM_CHROME;
        } else if (browser.contains("Safari")) {
            return PrizmaConstants.PLATFORM_SAFARI;
        } else {
            return "";
        }
    }

    //Method to get the screenshots
    public static void getScreenshot(String fileName) {
        if (PrizmaDriver.isSelenoid()) {
            try {
                new File(System.getProperty("user.dir") + "/screenshots").mkdirs();
                FileOutputStream outPutFile = new FileOutputStream(System.getProperty("user.dir") + "/screenshots/" + fileName + "_validation" + ".jpg");
                outPutFile.write(((TakesScreenshot) PrizmaDriver.getCurrentDriver()).getScreenshotAs(OutputType.BYTES));
                out.close();
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                new File(System.getProperty("user.dir") + "/screenshots").mkdirs();
                FileOutputStream outPutFile = new FileOutputStream(System.getProperty("user.dir") + "/screenshots/" + fileName + "_validation" + ".jpg");
                outPutFile.write(((TakesScreenshot) PrizmaDriver.getCurrentRemoveWebDriver()).getScreenshotAs(OutputType.BYTES));
                out.close();
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }
    }

    //Method to check whether the current browser under test is Firefox
    public static Boolean isFirefoxBrowser() {
        return (getBrowserPlatform().equals(PrizmaConstants.PLATFORM_FIREFOX));
    }

    //Method to check whether the current browser under test is Firefox
    public static Boolean isFirefoxBrowserForSelenoid() {
        return (getBrowserPlatformForSelenoid().equals(PrizmaConstants.PLATFORM_FIREFOX));
    }

    //Method to check whether the current browser under test is Chrome
    public static Boolean isChromeBrowser() {
        return (getBrowserPlatform().equals(PrizmaConstants.PLATFORM_CHROME));
    }

    //Method to check whether the current browser under test is Chrome
    public static Boolean isChromeBrowserForSelenoid() {
        return (getBrowserPlatformForSelenoid().equals(PrizmaConstants.PLATFORM_CHROME));
    }

    //Method to check whether the current browser under test is Safari
    public static Boolean isSafariBrowser() {
        return (getBrowserPlatform().equals(PrizmaConstants.PLATFORM_SAFARI));
    }

    //Method to check whether the current browser under test is Safari
    public static Boolean isSafariBrowserForSelenoid() {
        return (getBrowserPlatformForSelenoid().equals(PrizmaConstants.PLATFORM_SAFARI));
    }

    //Method to check driver type
    public static Boolean isSelenoid() {
        return (!selenoid.equals(PrizmaConstants.SELENOID));
    }

    //Method to quit
    public static void quit() {
        driver.quit();
    }

    //Method to quit
    public static void quitRemoveWebDriver() {
        remoteWebDriver.quit();
    }

    //Method to get current driver
    public static WebDriver getCurrentDriver(){
        return driver;
    }

    //Method to get current driver
    public static WebDriver getCurrentRemoveWebDriver(){
        return remoteWebDriver;
    }
}

package prizma.maxiru.framework.common;

public class PrizmaConstants {
    public static final String PLATFORM_FIREFOX = "Firefox";
    public static final String PLATFORM_CHROME = "Chrome";
    public static final String PLATFORM_SAFARI = "Safari";
    public static final String SELENOID = "selenoid";
    public static final String DOWNLOAD_FILES_FOLDER_PATH = System.getProperty("user.dir") + "/downloads/";
    public static final String EMAIL_FOR_SIGN_IN_TESTS = "aaa@aaa.com";
    public static final String PASSWORD_FOR_SIGN_IN_TESTS = "Aaa123!@#";
    public static final String EMAIL_FOR_SIGN_IN_TESTS_2 = "zzzz@zzz.com";
    public static final String PASSWORD_FOR_SIGN_IN_TESTS_2 = "Aaa123!@#";
    public static final String BOT_NAME_FOR_FAILED_BOT_TESTS_1 = "failed_bot";
    public static final String BOT_NAME_FOR_FAILED_BOT_TESTS_2 = "failed_bot_2";
    public static final String BOT_NAME_FOR_ORDERS_TESTS = "Autotest_orders";
    public static final String EMAIL_FOR_BACKTEST_TESTS_USER_A = "backtest@gmail.com";
    public static final String PASSWORD_FOR_BACKTEST_TESTS_USER_A = "backtest";
    public static final String EMAIL_FOR_BACKTEST_TESTS_USER_B = "backtest2@gmail.com";
    public static final String PASSWORD_FOR_BACKTEST_TESTS_USER_B = "backtest2";
    public static final String EMAIL_FOR_PRIZMA_USER = "system-user@maxiru.com";
    public static final String PASSWORD_FOR_PRIZMA_USER = "systemuser123!@#";
    public static final String EMAIL_FOR_INVESTOR_STATISTICS_TESTS = "livetest@gmail.com";
    public static final String PASSWORD_FOR_INVESTOR_STATISTICS_TESTS = "Livetest1!";
    public static final String API_KEY = "1Pz5L9ZgZ0zHw25apRGiUvL0L0hQSe4j89TayXJlSBTQICdaWnQ5wReK2DwFzPBT";
    public static final String SECRET_KEY = "Kz6rzSuONePTnwbBfOc97GuuxiLShmVpJAybEi1VY9HI1EngNmQJ6AKvhzf5DrD9";

    public static final Integer WAIT_TIME = 2000;
    public static final Integer LESS_WAIT_TIME = 1500;
    public static final Integer SHORT_WAIT_TIME = 1000;
    public static final Integer LONG_WAIT_TIME = 4000;
    public static final Integer REFRESH_WAIT_TIME = 3000;
    public static final Integer HOLD_WAIT_TIME = 4000;
    public static final Integer LOG_OUT_TIME = 2000;
    public static final Integer DOWNLOAD_WAIT_TIME = 80000;
    public static final Integer BOTS_PER_PAGE_OPTION_1 = 15;
    public static final Integer BOTS_PER_PAGE_OPTION_2 = 30;
    public static final Integer BOTS_PER_PAGE_OPTION_3 = 45;
    public static final Integer BOTS_PER_PAGE_OPTION_4 = 60;
}
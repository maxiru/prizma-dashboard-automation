package prizma.maxiru.testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.net.MalformedURLException;

import prizma.maxiru.framework.common.*;
import prizma.maxiru.tests.*;

@RunWith(Suite.class)
// Specify an array of test classes
@Suite.SuiteClasses({
        SignupTests.class,
        SigninTests.class,
        LogoutTests.class,
        DownloadWebSDKTests.class,
        UploadNewBotTests.class,
        CreatorTotalSectionTests.class,
        CreatorSplashScreenTests.class,
        InvestorSplashScreenTests.class,
        CreatorInvestorNoSplashScreenTests.class,
        CreatorBotDetailsTests.class,
        AccessToOnlyCreatedByMeToggleTests.class,
        CheckCreatedByMeToggleFilterTests.class,
        StoreTableSortingTests.class,
        StoreTests.class,
        StoreBotsPerPageTests.class,
        StoreSwitchPagesTests.class,
        FailedBotDetailsErrorDialogTests.class,
        ActivateBotTests.class
})

// The actual class should be empty
public class PrizmaTestSuiteAll {

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        //for local testing
        String platform = "Chrome";
        //String platform = "Safari";
        //String platform = "Firefox";

        //for local testing
        String link = "https://dev.prizma.simis.ai/";

        PrizmaDriver.setup(platform, link);
    }

    @AfterClass
    public static void afterClass() {
        PrizmaDriver.quit();
    }
}
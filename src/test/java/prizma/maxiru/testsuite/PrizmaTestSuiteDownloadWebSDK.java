package prizma.maxiru.testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.net.MalformedURLException;

import prizma.maxiru.framework.common.*;
import prizma.maxiru.tests.*;

@RunWith(Suite.class)
// Specify an array of test classes
@Suite.SuiteClasses({
        DownloadWebSDKTests.class
})

// The actual class should be empty
public class PrizmaTestSuiteDownloadWebSDK {

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        //for local testing
        String platform = "Chrome";
        //works only on Chrome browser

        //for local testing
        String link = "https://dev.prizma.simis.ai/";

        PrizmaDriver.setup(platform, link);
    }

    @AfterClass
    public static void afterClass() {
        PrizmaDriver.quit();
    }
}
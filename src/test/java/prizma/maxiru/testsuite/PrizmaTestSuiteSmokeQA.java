package prizma.maxiru.testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.tests.*;

import java.net.MalformedURLException;

@RunWith(Suite.class)
// Specify an array of test classes
@Suite.SuiteClasses({
        SignupTests.class,
        SigninTests.class,
        LogoutTests.class,
        DownloadWebSDKTests.class,
        UploadNewBotTests.class,
        CreatorTotalSectionTests.class,
        CreatorSplashScreenTests.class,
        InvestorSplashScreenTests.class,
        CreatorInvestorNoSplashScreenTests.class,
        CreatorBotDetailsTests.class,
        BotDetailsOnStorePageTests.class,
        AccessToOnlyCreatedByMeToggleTests.class,
        CheckCreatedByMeToggleFilterTests.class,
        StoreTableSortingTests.class,
        StoreTests.class,
        StoreBotsPerPageTests.class,
        StoreSwitchPagesTests.class,
        FailedBotDetailsErrorDialogTests.class,
        ActivateBotTests.class
})

// The actual class should be empty
public class PrizmaTestSuiteSmokeQA {

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        //for local testing
        String platform = "Chrome";
        //String platform = "Safari";
        //String platform = "Firefox";

        //for local testing
        String link = "https://dev.prizma.simis.ai/";

        PrizmaDriver.setup(platform, link);
    }

    @AfterClass
    public static void afterClass() {
        PrizmaDriver.quit();
    }
}
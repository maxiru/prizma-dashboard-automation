package prizma.maxiru.tests;

import org.junit.*;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UploadActivateDeactivateDeleteBotTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002DeleteAllBotsBeforeTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase002DeleteAllBotsBeforeTests");
    }

    @Test
    public void testcase002DeleteAllBotsBeforeTestsDataSet1() {
        testcase002DeleteAllBotsBeforeTests(websitelink);
    }

    public void testcase003UploadBot(String link, String fileName, String name, String rate, String description, int position, String botStatus) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.setBotNameOnUploadANewBotDialog(name);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.getScreenshot("testcase003UploadBotFormIsFilled");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(name, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase003BotIsUploaded");
    }

    @Test
    public void testcase003UploadBot() {
        testcase003UploadBot(websitelink, "bot_sample.zip", "autotest-bot", "5", "Uploading bot for auto tests", 1, "Preparing validation");
    }

    public void testcase004WaitUntilBotIsPublished(String link, String name) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(name);
        actionwords.getScreenshot("testcase004BotIsPublished");
    }

    @Test
    public void testcase004WaitUntilBotIsPublished() {
        testcase004WaitUntilBotIsPublished(websitelink, "autotest-bot");
    }

    public void testcase005ActivateBot(String link, String name) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogByName(name);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase005ActivateBotDialogIsOpened");
        actionwords.clickOnActivateBotSelectExchangeDropDownAndSelectBinanceOption();
        actionwords.getScreenshot("testcase005APIKeyDialogIsOpened");
        actionwords.clickOnActivateBotUserAgreementCheckboxToEnable();
        actionwords.setApiKeyOnActivateBotDialog(API_KEY);
        actionwords.setApiSecretKeyOnActivateBotDialog(SECRET_KEY);
        //actionwords.verifyThatActivateBotUserAgreementCheckboxIsEnabled();
        actionwords.getScreenshot("testcase005APIKeysAreEntered");
        actionwords.clickOnActivateBotNextBtn();
        actionwords.getScreenshot("testcase005BotBalanceDialogIsOpened");
        actionwords.setBotBalanceOnActivateBotDialog("1000");
        actionwords.setBotStopLostOnActivateBotDialog("100");
        actionwords.clickOnNotifyIfLowerCheckboxToDisable();
        actionwords.clickOnActivateBotConnectBtn();
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(name);
    }

    @Test
    public void testcase005ActivateBot() {
        testcase005ActivateBot(websitelink, "autotest-bot");
    }

    public void testcase006DeactivateBot(String link, String name, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(name);
        actionwords.getScreenshot("testcase006CheckIfBotIsOnInvestorPage");
        actionwords.clickOnBotOnInvestorPageToOpenBotDialogByName(name);
        actionwords.clickOnDeactivateBotButton(position);
        actionwords.clickOnBotOkBtnOnDeactivateBotDialog();
        actionwords.verifyThatBotNotPresentInListOfBotsOnInvestorPage(name);
        actionwords.getScreenshot("testcase006BotIsNotDisplayedOnInvestorPage");
    }

    @Test
    public void testcase006DeactivateBot() {
        testcase006DeactivateBot(websitelink, "autotest-bot", 1);
    }

    public void testcase010DeleteAllBotsAfterTests(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase010SignInIntoPrizmaDashboard");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase010DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase010DeleteAllBotsAfterTestsDataSet1() {
        testcase010DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase011LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_3");
    }

    @Test
    public void testcase011LogoutFromAccountDataSet1() {
        testcase011LogoutFromAccount(websitelink);
    }
}
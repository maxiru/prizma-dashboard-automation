package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UploadNewBotTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001PassSignUpWithValidTestData(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestData_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestData_2");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestData_3");
    }

    @Test
    public void testcase001PassSignUpWithValidTestDataDataSet1() {
        testcase001PassSignUpWithValidTestData(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty(String link, String message) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty_2");
        actionwords.clickOnBotUploadCancelBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseAfterClickOnCancelBtn();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty_3");
        actionwords.verifyBotBotsEmptyLabelInUploadedBotsSection(message);
        actionwords.getScreenshot("testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty_4");
    }

    @Test
    public void testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmptyDataSet1() {
        testcase002CheckAbilityToOpenUploadNewBotDialogAndCloseAndCheckThatBotUploadSectionIsEmpty(websitelink, "NO ANY BOTS");
    }

    public void testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose(String link, String message) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose_2");
        actionwords.clickOnBotUploadCancelBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseAfterClickOnCancelBtn();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose_3");
        actionwords.verifyBotBotsEmptyLabelInUploadedBotsSection(message);
        actionwords.getScreenshot("testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose_4");
    }

    @Test
    public void testcase003CheckAbilityToOpenUploadAnotherBotDialogAndCloseDataSet1() {
        testcase003CheckAbilityToOpenUploadAnotherBotDialogAndClose(websitelink, "NO ANY BOTS");
    }

    public void testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(String link, String botName, String fileName1, String fileName2, String fileName3) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName3);
    }

    @Test
    public void testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1() {
        testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "a", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_1", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_2", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_3");
    }

    @Test
    public void testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2() {
        testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "aa", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_1", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_2", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_3");
    }

    @Test
    public void testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3() {
        testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "aaaaaaaaaaaaaaaaaaaaa", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_1", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_2", "testcase004CheckThatErrorLimitMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_3");
    }

    public void testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(String link, String botName, String fileName1, String fileName2, String fileName3) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotNameErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName3);
    }

    @Test
    public void testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1() {
        testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "@@@", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_1", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_2", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_3");
    }

    @Test
    public void testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2() {
        testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "@@@aaa@@@", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_1", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_2", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_3");
    }

    @Test
    public void testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3() {
        testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "aaa@@@", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_1", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_2", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet3_3");
    }

    @Test
    public void testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet4() {
        testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "@@@aaa", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet4_1", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet4_2", "testcase005CheckThatErrorMessageAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet4_3");
    }

    public void testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(String link, String botName, String fileName1, String fileName2, String fileName3) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot(fileName1);
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot(fileName2);
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotNameLimitErrorMessageNotAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName3);
    }

    @Test
    public void testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1() {
        testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "aa1", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_1", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_2", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet1_3");
    }

    @Test
    public void testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2() {
        testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialog(websitelink, "aaaaaaaaaaaaaaaaaaaa", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_1", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_2", "testcase006CheckThatErrorMessageNotAppearWhenTriedToSetIncorrectBotNameAsPerLimitOnBotNameInputOnUploadBotDialogDataSet2_3");
    }

    public void testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog(String link, String botName, String fileName) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog_3");
    }

    @Test
    public void testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialogDataSet1() {
        testcase007CheckThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog(websitelink, "Autotest_orders", "1.zip");
    }

    public void testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialog(String link, String botName) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotUploadMandatoryErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialog_3");
    }

    @Test
    @Ignore
    public void testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialogDataSet1() {
        testcase008CheckThatBotFileMandatoryErrorMessageAppearOnUploadBotDialog(websitelink, "ddd1992");
    }

    public void testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialog(String link, String botName, String fileName1, String fileName2, String fileName3, String fileName4) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot(fileName2);
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot(fileName3);
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.uploadNewBotZipFile(fileName1);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotUploadInvalidFileErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot(fileName4);
    }

    @Test
    public void testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialogDataSet1() {
        testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialog(websitelink, "ddd1992", "3.ipa", "testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialogDataSet1_1", "testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialogDataSet1_2", "testcase009CheckThatBotInvalidFileErrorMessageAppearOnUploadBotDialogDataSet1_3");
    }

    public void testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot_2_enabled");
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogToDisable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot_3_disabled");
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot_4_enabled");
    }

    @Test
    public void testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBotDataSet1() {
        testcase010CheckAbilityToDisableAndEnablePrivateToggleOnUploadBot(websitelink);
    }

    public void testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialog(String link, String botName) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialogWithEnter(botName);
        actionwords.verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialog_3");
    }

    @Test
    @Ignore
    public void testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialogDataSet1() {
        testcase011CheckThatErrorLimitMessageAppearWhenOnBotNameInputHitEnterOnKeyboardOnUploadBotDialog(websitelink, "");
    }

    public void testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialog(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialog_2");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialog_3");
    }

    @Test
    @Ignore
    public void testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialogDataSet1() {
        testcase012CheckThatErrorLimitMessageAppearWhenWithBotNameInputHitConfirmBtnOnKeyboardOnUploadBotDialog(websitelink);
    }

    public void testcase013CheckThatBotInvalidFileSizeErrorMessageAppearOnUploadBotDialog(String link, String botName, String rate, String description, String fileName) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase013CheckThatBotInvalidFileSizeErrorMessageAppearOnUploadBotDialog_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.verifyThatBotUploadInvalidFileSizeErrorMessageAppearOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase013CheckThatBotInvalidFileSizeErrorMessageAppearOnUploadBotDialog_3");
    }

    @Test
    public void testcase013CheckThatBotInvalidFileSizeErrorMessageAppearOnUploadBotDialogDataSet1() {
        testcase013CheckThatBotInvalidFileSizeErrorMessageAppearOnUploadBotDialog(websitelink, "ddd1992", "10", "Uploading bot for auto tests", "5.zip");
    }

    public void testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog(String link, String fileName, int position, String botStatus) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialogWhichIsAutoGenerated();
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogToDisable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_3");
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_4");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_5");
        actionwords.verifyBotNameLabelFromVariableInUploadedBotsSectionOnPosition(position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog_6");
    }

    @Test
    public void testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialogDataSet1() {
        testcase014CheckThatPossibleToUploadPrivateBotOnUploadBotDialog(websitelink, "1.zip", 1, "Preparing validation");
    }

    public void testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog(String link, String fileName, int position, String botStatus,
                                                                               String rate,
                                                                               String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialogWhichIsAutoGenerated();
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog_3");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog_4");
        actionwords.verifyBotNameLabelFromVariableInUploadedBotsSectionOnPosition(position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog_5");
    }

    @Test
    public void testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialogDataSet1() {
        testcase015CheckThatPossibleToUploadPublicBotOnUploadBotDialog(websitelink, "1.zip", 1,
                "Preparing validation", "10", "Uploading bot for auto tests");
    }

    public void testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbers(String link,
                                                                                                                            String botName,
                                                                                                                            String fileName,
                                                                                                                            String rate,
                                                                                                                            String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbers_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbers_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbers_3");
    }

    @Test
    public void testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbersDataSet1() {
        testcase016CheckThatBotValidationFailedErrorMessageAppearOnUploadBotDialogWhenTryToUploadBotOnlyWithNumbers(websitelink, "1992",
                "1.zip", "10", "Uploading bot for auto tests");
    }

    public void testcase017CheckThatPrivacyToggleIsOn(String link, String fileName, String rate, String description) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase017CheckThatPrivacyToggleIsOn_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase017CheckThatPrivacyToggleIsOn_2");
        actionwords.setBotNameOnUploadANewBotDialogWhichIsAutoGenerated();
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.getScreenshot("testcase017CheckThatPrivacyToggleIsOn_3");
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase017CheckThatPrivacyToggleIsOn_4");
    }

    @Test
    public void testcase017CheckThatPrivacyToggleIsOnDataSet1() {
        testcase017CheckThatPrivacyToggleIsOn(websitelink, "1.zip", "10", "Uploading bot for auto tests");
    }

    public void testcase016DeleteAllBotsAfterTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase016DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase016DeleteAllBotsAfterTestsDataSet1() {
        testcase016DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase017LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase017LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase017LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase017LogoutFromAccount_3");
    }

    @Test
    public void testcase017LogoutFromAccountDataSet1() {
        testcase017LogoutFromAccount(websitelink);
    }
}
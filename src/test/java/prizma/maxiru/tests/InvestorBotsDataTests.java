package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaConstants.PASSWORD_FOR_PRIZMA_USER;
import static prizma.maxiru.framework.common.PrizmaDriver.waitFor;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InvestorBotsDataTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    String botNameForStatistics = "e1_bsample_01";
    int botPositionInList = 1;

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_PRIZMA_USER, PASSWORD_FOR_PRIZMA_USER);
    }

    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed(String botName) {
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(botName);
        actionwords.getScreenshot("testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed");
    }

    @Test
    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayedSet1() {
        testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed(botNameForStatistics);
    }

    public void testcase003VerifyThatBotAvatarInBotsListIsCorrect(int position) {
        actionwords.verifyThatBotAvatarInBotsListOnInvestorPageIsCorrect(position);
        actionwords.getScreenshot("testcase003VerifyThatBotAvatarInBotsListIsCorrect");
    }

    @Test
    public void testcase003VerifyThatBotAvatarInBotsListIsCorrectSet1() {
        testcase003VerifyThatBotAvatarInBotsListIsCorrect(botPositionInList);
    }

    public void testcase004VerifyThatBotExchangeInBotsListCorrect(String exchange, int position) {
        actionwords.verifyThatBotExchangeInBotsListOnInvestorPageIsCorrect(exchange, position);
        actionwords.getScreenshot("testcase004VerifyThatBotExchangeIsCorrectInBotsListCorrect");
    }

    @Test
    public void testcase004VerifyThatBotExchangeIsCorrectInBotsListCorrectSet1() {
        testcase004VerifyThatBotExchangeInBotsListCorrect("Paper Trading", botPositionInList);
    }

    public void testcase005VerifyThatBotActivationDateInBotsListCorrect(int position) {
        actionwords.verifyThatBotActivationDateInBotsListOnInvestorPageIsCorrect(position);
        actionwords.getScreenshot("testcase005VerifyThatBotActivationDateIsCorrectInBotsListCorrect");
    }

    @Test
    public void testcase005VerifyThatBotActivationDateInBotsListCorrectSet1() {
        testcase005VerifyThatBotActivationDateInBotsListCorrect(botPositionInList);
    }

    public void testcase006VerifyThatBotRateInBotsListCorrect(int position) {
        actionwords.verifyThatBotRateInBotsListOnInvestorPageIsCorrect(position);
        actionwords.getScreenshot("testcase005VerifyThatBotRateCorrectInBotsListIsCorrect");
    }

    @Test
    public void testcase006VerifyThatBotRateInBotsListCorrectSet1() {

        testcase006VerifyThatBotRateInBotsListCorrect(botPositionInList);
    }

    public void testcase007VerifyThatStateInBotsListCorrect(String state, int position) {
        actionwords.verifyThatStateInBotsListOnInvestorPageIsCorrect(state, position);
        actionwords.getScreenshot("testcase007VerifyThatStateIsCorrectInBotsListCorrect");
    }

    @Test
    public void testcase007VerifyThatStateInBotsListCorrectSet1() {
        testcase007VerifyThatStateInBotsListCorrect("Running...", botPositionInList);
    }

    public void testcase008VerifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.verifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase008VerifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPage");
    }

    @Test
    @Ignore
    public void testcase008VerifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPageSet1() {
        testcase008VerifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase009VerifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.verifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase009VerifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase009VerifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPageSet1() {
        testcase009VerifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase010VerifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.verifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase010VerifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase010VerifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPageSet1() {
        testcase010VerifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase012VerifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.verifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase012VerifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase012VerifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPageSet1() {
        testcase012VerifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase013VerifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.refreshPage();
        waitFor(5000);
        actionwords.verifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase013VerifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase013VerifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPageSet1() {
        testcase013VerifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase014VerifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.refreshPage();
        waitFor(5000);
        actionwords.verifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase014VerifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase014VerifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPageSet1() {
        testcase014VerifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase015VerifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.refreshPage();
        waitFor(5000);
        actionwords.verifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase015VerifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase015VerifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPageSet1() {
        testcase015VerifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase016VerifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        actionwords.refreshPage();
        waitFor(5000);
        actionwords.verifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(botName, position);
        actionwords.getScreenshot("testcase016VerifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage");
    }

    @Test
    public void testcase016VerifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPageSet1() {
        testcase016VerifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase017LogOutFromAccount() {
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase017LogOutFromAccount");
    }

    @Test
    public void testcase017LogOutFromAccountDataSet1() {
        testcase017LogOutFromAccount();
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaSignupPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InvestorSplashScreenTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001PassSignUpWithValidTestDataAsCreator(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_3");
    }

    @Test
    public void testcase001PassSignUpWithValidTestDataAsCreatorDataSet1() {
        testcase001PassSignUpWithValidTestDataAsCreator(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenDataSet1() {
        testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreenDataSet1() {
        testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreement(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreement_1");
        actionwords.clickOnUserAgreementOpenBtnOnInvestorPage();
        actionwords.getScreenshot("testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreement_2");
        actionwords.clickOnUserAgreementCloseBtnOnInvestorPage();
        actionwords.getScreenshot("testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreement_3");
    }

    @Test
    public void testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreementDataSet1() {
        testcase004ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAncCheckAbilityOpenCloseInvestorUserAgreement(websitelink);
    }

    public void testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenTryToClickOnStartInvestingBtnOnInvestorPageWhenUserAgreementNotApprove(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenTryToClickOnStartInvestingBtnOnInvestorPageWhenUserAgreementNotApprove_1");
        actionwords.clickOnStartInvestingBtnOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenTryToClickOnStartInvestingBtnOnInvestorPageWhenUserAgreementNotApprove_2");
    }

    @Test
    public void testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenTryToClickOnStartInvestingBtnOnInvestorPageWhenUserAgreementNotApproveDataSet1() {
        testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenTryToClickOnStartInvestingBtnOnInvestorPageWhenUserAgreementNotApprove(websitelink);
    }

    public void testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreement(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreement_1");
        actionwords.clickOnUserAgreementToggleOnInvestorPageToEnable();
        actionwords.getScreenshot("testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreement_2");
        actionwords.clickOnUserAgreementToggleOnInvestorPageToDisable();
        actionwords.getScreenshot("testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreement_3");
    }

    @Test
    public void testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreementDataSet1() {
        testcase006ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableDisableUserAgreement(websitelink);
    }

    public void testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_1");
        actionwords.clickOnUserAgreementToggleOnInvestorPageToEnable();
        actionwords.getScreenshot("testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_2");
        actionwords.clickOnStartInvestingBtnOnInvestorPage();
        actionwords.getScreenshot("testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_3");
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_4");
        actionwords.refreshPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_5");
    }

    @Test
    public void testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRoleDataSet1() {
        testcase007ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole(websitelink);
    }

    public void testcase008LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase008LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase008LogoutFromAccount_2");
    }

    @Test
    public void testcase008LogoutFromAccountDataSet1() {
        testcase008LogoutFromAccount(websitelink);
    }

    public void testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(String link, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(LP_EMAIL_FOR_FUTURE_TESTS);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_1");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_2");
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_3");
        actionwords.refreshPage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_4");
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_5");
    }

    @Test
    public void testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPageDataSet1() {
        testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(websitelink, "Romank_1992");
    }

    public void testcase010LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase010LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase010LogoutFromAccount_2");
    }

    @Test
    public void testcase010LogoutFromAccountDataSet1() {
        testcase010LogoutFromAccount(websitelink);
    }
}
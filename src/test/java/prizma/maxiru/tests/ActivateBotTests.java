package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.EMAIL_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaConstants.PASSWORD_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ActivateBotTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtn(String link, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtn_1");
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtn_2");
        actionwords.clickOnActivateBotCloseDialogBtn();
        actionwords.verifyThatActivateBotDialogCloseAfterClickOnCloseBtn();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtn_3");
    }

    @Test
    public void testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtnDataSet1() {
        testcase002CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCloseBtn(websitelink, 1);
    }

    public void testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtn(String link, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();


        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtn_1");
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtn_2");
        actionwords.clickOnActivateBotCancelBtn();
        actionwords.verifyThatActivateBotDialogCloseAfterClickOnCancelBtn();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtn_3");
    }

    @Test
    public void testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtnDataSet1() {
        testcase003CheckAbilityToOpenCloseActivateBotDialogAfterClickOnCancelBtn(websitelink, 1);
    }

    public void testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStep(String link, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStep_1");
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStep_2");
        actionwords.verifyThanNextBtnIsDisabled();
        actionwords.verifyThatUserNotRedirectToSecondPageOfActivateBotFlow();
        actionwords.getScreenshot("testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStep_3");
    }

    @Test
    public void testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStepDataSet1() {
        testcase004CheckAbilityToClickOnActivateBtnNextBtnWhenExchangeNotSelectAndValidateThatUserNotGoToNextStep(websitelink, 1);
    }

    public void testcase005CheckAbilityToSelectBinanceExchangeAndFieldsOfActivateBotForBinance(String link, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase005CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithBinanceInstruction_1");
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase005CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithBinanceInstruction_2");
        actionwords.clickOnActivateBotSelectExchangeDropDownAndSelectBinanceOption();
        actionwords.getScreenshot("testcase005CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithBinanceInstruction_3");
    }

    @Test
    public void testcase005CheckAbilityToSelectBinanceExchangeAndFieldsOfActivateBotForBinanceDataSet1() {
        testcase005CheckAbilityToSelectBinanceExchangeAndFieldsOfActivateBotForBinance(websitelink, 1);
    }

    public void testcase006CheckAbilityToSelectCoinbaseExchangeAndFieldsOfActivateBotForCoinbaseInstruction(String link, int position) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase006CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithCoinbaseInstruction_1");
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
        actionwords.clickOnConnectBotBtn();
        actionwords.getScreenshot("testcase006CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithCoinbaseInstruction_2");
        actionwords.clickOnActivateBotSelectExchangeDropDownAndSelectCoinbaseOption();
        actionwords.getScreenshot("testcase006CheckAbilityToSelectBinanceExchangeAndRedirectToSecondPageOfActivateBotWithCoinbaseInstruction_3");
    }

    @Test
    public void testcase006CheckAbilityToSelectCoinbaseExchangeAndFieldsOfActivateBotForCoinbaseInstructionDataSet1() {
        testcase006CheckAbilityToSelectCoinbaseExchangeAndFieldsOfActivateBotForCoinbaseInstruction(websitelink, 1);
    }

    public void testcase007LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_2");
    }

    @Test
    public void testcase007LogOutFromAccountDataSet1() {
        testcase007LogOutFromAccount(websitelink);
    }
}
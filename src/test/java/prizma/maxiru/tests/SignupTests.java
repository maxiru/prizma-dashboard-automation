package prizma.maxiru.tests;

import org.junit.*;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaSignupPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SignupTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();


    public void testcase001TryToHitEnterOnKeyboardOnFirstNameInputWithEmptyDataAndCheckThatErrorAppear(String link, String fullName) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001TryToHitEnterOnKeyboardOnFirstNameInputWithEmptyDataAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPageWithEnter(fullName);
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase001TryToHitEnterOnKeyboardOnFirstNameInputWithEmptyDataAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase001TryToHitEnterOnKeyboardOnFirstNameInputWithEmptyDataAndCheckThatErrorAppearDataSet1() {
        testcase001TryToHitEnterOnKeyboardOnFirstNameInputWithEmptyDataAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase002TryToHitEnterOnKeyboardOnEmailInputWithEmptyDataAndCheckThatErrorAppear(String link, String email) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase002TryToHitEnterOnKeyboardOnEmailInputWithEmptyDataAndCheckThatErrorAppear_1");
        actionwords.setEmailOnSignUpPageWithoutTimestampWithEnter(email);
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase002TryToHitEnterOnKeyboardOnEmailInputWithEmptyDataAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase002TryToHitEnterOnKeyboardOnEmailInputWithEmptyDataAndCheckThatErrorAppearDataSet1() {
        testcase002TryToHitEnterOnKeyboardOnEmailInputWithEmptyDataAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase003TryToHitEnterOnKeyboardOnUsernameInputWithEmptyDataAndCheckThatErrorAppear(String link, String displayName) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase003TryToHitEnterOnKeyboardOnUsernameInputWithEmptyDataAndCheckThatErrorAppear_1");
        actionwords.setDisplayNameOnSignUpPageWithoutTimestampWithEnter(displayName);
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase003TryToHitEnterOnKeyboardOnUsernameInputWithEmptyDataAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase003TryToHitEnterOnKeyboardOnUsernameInputWithEmptyDataAndCheckThatErrorAppearDataSet1() {
        testcase003TryToHitEnterOnKeyboardOnUsernameInputWithEmptyDataAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase004TryToHitEnterOnKeyboardOnPasswordInputWithEmptyDataAndCheckThatErrorAppear(String link, String password) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase004TryToHitEnterOnKeyboardOnPasswordInputWithEmptyDataAndCheckThatErrorAppear_1");
        actionwords.setPasswordOnSignUpPageWithEnter(password);
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase004TryToHitEnterOnKeyboardOnPasswordInputWithEmptyDataAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase004TryToHitEnterOnKeyboardOnPasswordInputWithEmptyDataAndCheckThatErrorAppearDataSet1() {
        testcase004TryToHitEnterOnKeyboardOnPasswordInputWithEmptyDataAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase005TryToHitEnterOnKeyboardOnRepeatPasswordInputWithEmptyDataAndCheckThatErrorAppear(String link, String repeatPassword) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase005TryToHitEnterOnKeyboardOnRepeatPasswordInputWithEmptyDataAndCheckThatErrorAppear_1");
        actionwords.setPasswordOnSignUpPageWithEnter(repeatPassword);
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase005TryToHitEnterOnKeyboardOnRepeatPasswordInputWithEmptyDataAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase005TryToHitEnterOnKeyboardOnRepeatPasswordInputWithEmptyDataAndCheckThatErrorAppearDataSet1() {
        testcase005TryToHitEnterOnKeyboardOnRepeatPasswordInputWithEmptyDataAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_1");
        actionwords.setFullNameOnSignUpPageWithTab(fullName);
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_2");
        actionwords.checkCreatorCheckboxOnSignUpPageWithTab();
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_4");
        actionwords.checkInvestorCheckboxOnSignUpPageWithTab();
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_5");
        actionwords.setEmailOnSignUpPageWithoutTimestampWithTab(email);
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_6");
        actionwords.setDisplayNameOnSignUpPageWithoutTimestampWithTab(displayName);
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_7");
        actionwords.setPasswordOnSignUpPageWithTab(password);
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_8");
        actionwords.setRepeatPasswordOnSignUpPageWithTab(repeatPassword);
        actionwords.getScreenshot("testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage_9");
    }

    @Test
    public void testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPageDataSet1() {
        testcase006CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignUpBtnRoleCheckboxesOnSignUpPage(websitelink, "", "", "", "", "");
    }

    public void testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPageWithEnter(repeatPassword);
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.getScreenshot("testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn_2");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn_3");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn_4");
    }

    @Test
    public void testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtnDataSet1() {
        testcase007PassSignUpWithValidTestDataButClickOnEnterBtnInsteadOnSignUpBtn(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase008SignInWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToHomePage(String link, String password) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(LP_EMAIL_FOR_FUTURE_TESTS);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase008SignInWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToHomePage_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase008SignInWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToHomePage_2");
    }

    @Test
    public void testcase008SignInWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToHomePageDataSet1() {
        testcase008SignInWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToHomePage(websitelink, "Romank_1992");
    }

    public void testcase009LogoutFromAccount(String link) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_3");
    }

    @Test
    public void testcase009LogoutFromAccountDataSet1() {
        testcase009LogoutFromAccount(websitelink);
    }

    public void testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.getScreenshot("testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage_2");
        actionwords.clickOnShowPasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage_3");
        actionwords.clickOnHidePasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage_4");
        actionwords.clickOnShowPasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage_5");
    }

    @Test
    public void testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPageDataSet1() {
        testcase010CheckHowWorksShowHidePasswordFeatureOnSignUpPage(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.getScreenshot("testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage_2");
        actionwords.clickOnShowRepeatPasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage_3");
        actionwords.clickOnHideRepeatPasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage_4");
        actionwords.clickOnShowRepeatPasswordBtnOnSignUpPage();
        actionwords.getScreenshot("testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage_5");
    }

    @Test
    public void testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPageDataSet1() {
        testcase011CheckHowWorksShowHideRepeatPasswordFeatureOnSignUpPage(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword, String fileName1, String fileName2) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot(fileName1);
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPageWithoutModifications(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatEmailErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot(fileName1);
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet1() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "r@autotest.com", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet1_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet1_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet2() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "roman@i.ua", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet2_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet2_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet3() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "roman@gmailcom", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet3_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet3_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet4() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "roman@@autotest.com", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet4_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet4_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet5() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "roman@gmail..com", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet5_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet5_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet6() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "romangmail.com", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet6_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet6_2");
    }

    @Test
    public void testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet7() {
        testcase012TryToSignUpWithInvalidEmailSoErrorAppear(websitelink, "Aaaa Aaaa", "роман@autotest.com", "aaa", "aaa", "aaa", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet7_1", "testcase012TryToSignUpWithInvalidEmailSoErrorAppearDataSet7_2");
    }

    public void testcase013TryToSignUpWithUserAndEmailThatRegisterBeforeWithTheSameUsernameAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase013TryToSignUpWithUserAndEmailThatRegisterBeforeWithTheSameUsernameAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPageWithoutModifications(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.getScreenshot("testcase013TryToSignUpWithUserAndEmailThatRegisterBeforeWithTheSameUsernameAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase013TryToSignUpWithUserAndEmailThatRegisterBeforeWithTheSameUsernameAndCheckThatErrorAppearDataSet1() {
        testcase013TryToSignUpWithUserAndEmailThatRegisterBeforeWithTheSameUsernameAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "aaa@aaa.com", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase014SwitchFromSignInPageToSignUpPageAndBack(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase014SwitchFromSignInPageToSignUpPageAndBack_1");
        actionwords.clickOnSignInBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToSignInPage();
        actionwords.getScreenshot("testcase014SwitchFromSignInPageToSignUpPageAndBack_2");
    }

    @Test
    public void testcase014SwitchFromSignInPageToSignUpPageAndBackDataSet1() {
        testcase014SwitchFromSignInPageToSignUpPageAndBack(websitelink);
    }

    public void testcase015TryToSignUpWithAllEmptyFieldsAndCheckThatErrorAppear(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase015TryToSignUpWithAllEmptyFieldsAndCheckThatErrorAppear_1");
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase015TryToSignUpWithAllEmptyFieldsAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase015TryToSignUpWithAllEmptyFieldsAndCheckThatErrorAppearDataSet1() {
        testcase015TryToSignUpWithAllEmptyFieldsAndCheckThatErrorAppear(websitelink);
    }

    public void testcase016TryToSignUpWithoutFirstNameAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase016TryToSignUpWithoutFirstNameAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase016TryToSignUpWithoutFirstNameAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase016TryToSignUpWithoutFirstNameAndCheckThatErrorAppearDataSet1() {
        testcase016TryToSignUpWithoutFirstNameAndCheckThatErrorAppear(websitelink, "", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase017TryToSignUpWithoutSelectUserRoleAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase017TryToSignUpWithoutSelectUserRoleAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatRolesErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase017TryToSignUpWithoutSelectUserRoleAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase017TryToSignUpWithoutSelectUserRoleAndCheckThatErrorAppearDataSet1() {
        testcase017TryToSignUpWithoutSelectUserRoleAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase018TryToSignUpWithoutEmailAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase018TryToSignUpWithoutEmailAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatEmailErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase018TryToSignUpWithoutEmailAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase018TryToSignUpWithoutEmailAndCheckThatErrorAppearDataSet1() {
        testcase018TryToSignUpWithoutEmailAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase019TryToSignUpWithoutDisplayNameAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase019TryToSignUpWithoutDisplayNameAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatDisplayNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase019TryToSignUpWithoutDisplayNameAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase019TryToSignUpWithoutDisplayNameAndCheckThatErrorAppearDataSet1() {
        testcase019TryToSignUpWithoutDisplayNameAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "roman", "", "Romank_1992", "Romank_1992");
    }

    public void testcase020TryToSignUpWithoutPasswordAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase020TryToSignUpWithoutPasswordAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase020TryToSignUpWithoutPasswordAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase020TryToSignUpWithoutPasswordAndCheckThatErrorAppearDataSet1() {
        testcase020TryToSignUpWithoutPasswordAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "", "Romank_1992");
    }

    public void testcase021TryToSignUpWithoutRepeatPasswordAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase021TryToSignUpWithoutRepeatPasswordAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase021TryToSignUpWithoutRepeatPasswordAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase021TryToSignUpWithoutRepeatPasswordAndCheckThatErrorAppearDataSet1() {
        testcase021TryToSignUpWithoutRepeatPasswordAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "");
    }

    public void testcase022TryToSignUpWithFirstNameWhichHas1SymbolAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase022TryToSignUpWithFirstNameWhichHas1SymbolAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase022TryToSignUpWithFirstNameWhichHas1SymbolAndCheckThatErrorAppear_2");
    }


    public void testcase022TryToSignUpWithFirstNameWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase022TryToSignUpWithFirstNameWhichHas1SymbolAndCheckThatErrorAppear(websitelink, "a", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase023TryToSignUpWithFirstNameWhichHas2SymbolsAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase023TryToSignUpWithFirstNameWhichHas2SymbolsAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase023TryToSignUpWithFirstNameWhichHas2SymbolsAndCheckThatErrorAppear_2");
    }


    public void testcase023TryToSignUpWithFirstNameWhichHas2SymbolsAndCheckThatErrorAppear_1DataSet1() {
        testcase023TryToSignUpWithFirstNameWhichHas2SymbolsAndCheckThatErrorAppear(websitelink, "aa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase024TryToSignUpWithFirstNameWhichHas3SymbolsButWithoutRoleSoErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase024TryToSignUpWithFirstNameWhichHas3SymbolsButWithoutRoleSoErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatRolesErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase024TryToSignUpWithFirstNameWhichHas3SymbolsButWithoutRoleSoErrorAppear_2");
    }


    public void testcase024TryToSignUpWithFirstNameWhichHas3SymbolsButWithoutRoleSoErrorAppearDataSet1() {
        testcase024TryToSignUpWithFirstNameWhichHas3SymbolsButWithoutRoleSoErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase025TryToSignUpWithLastNameWhichHas1SymbolAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase025TryToSignUpWithLastNameWhichHas1SymbolAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatLastNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase025TryToSignUpWithLastNameWhichHas1SymbolAndCheckThatErrorAppear_2");
    }


    public void testcase025TryToSignUpWithLastNameWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase025TryToSignUpWithLastNameWhichHas1SymbolAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase026TryToSignUpWithDisplayNameWhichHas1SymbolAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase026TryToSignUpWithDisplayNameWhichHas1SymbolAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatDisplayNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase026TryToSignUpWithDisplayNameWhichHas1SymbolAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase026TryToSignUpWithDisplayNameWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase026TryToSignUpWithDisplayNameWhichHas1SymbolAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "a", "aaa", "aaa");
    }

    public void testcase027TryToSignUpWithDisplayNameWhichHas2SymbolsAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase027TryToSignUpWithUsernameWhichHas2SymbolsAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatDisplayNameErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase027TryToSignUpWithUsernameWhichHas2SymbolsAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase027TryToSignUpWithDisplayNameWhichHas2SymbolsAndCheckThatErrorAppearDataSet1() {
        testcase027TryToSignUpWithDisplayNameWhichHas2SymbolsAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aa", "aaa", "aaa");
    }

    public void testcase028TryToSignUpWithDisplayNameWhichHas3SymbolsButWithoutRoleSoErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase028TryToSignUpWithUsernameWhichHas3SymbolsButWithoutRoleSoErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatRolesErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase028TryToSignUpWithUsernameWhichHas3SymbolsButWithoutRoleSoErrorAppear_2");
    }

    @Test
    public void testcase028TryToSignUpWithDisplayNameWhichHas3SymbolsButWithoutRoleSoErrorAppearDataSet1() {
        testcase028TryToSignUpWithDisplayNameWhichHas3SymbolsButWithoutRoleSoErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase029TryToSignUpWithPasswordWhichHas1SymbolAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase029TryToSignUpWithPasswordWhichHas1SymbolAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase029TryToSignUpWithPasswordWhichHas1SymbolAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase029TryToSignUpWithPasswordWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase029TryToSignUpWithPasswordWhichHas1SymbolAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "a", "a");
    }

    public void testcase030TryToSignUpWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase030TryToSignUpWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase030TryToSignUpWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase030TryToSignUpWithPasswordWhichHas2SymbolsAndCheckThatErrorAppearDataSet1() {
        testcase030TryToSignUpWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aa", "aa");
    }

    public void testcase031TryToSignUpWithPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase031TryToSignUpWithPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatRolesErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase031TryToSignUpWithPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear_2");
    }

    @Test
    public void testcase031TryToSignUpWithPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppearDataSet1() {
        testcase031TryToSignUpWithPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase032TryToSignUpWithRepeatPasswordWhichHas1SymbolAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase032TryToSignUpWithRepeatPasswordWhichHas1SymbolAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase032TryToSignUpWithRepeatPasswordWhichHas1SymbolAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase032TryToSignUpWithRepeatPasswordWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase032TryToSignUpWithRepeatPasswordWhichHas1SymbolAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "a");
    }

    public void testcase033TryToSignUpWithRepeatPasswordWhichHas2SymbolsAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase033TryToSignUpWithRepeatPasswordWhichHas2SymbolsAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase033TryToSignUpWithRepeatPasswordWhichHas2SymbolsAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase033TryToSignUpWithRepeatPasswordWhichHas2SymbolsAndCheckThatErrorAppearDataSet1() {
        testcase033TryToSignUpWithRepeatPasswordWhichHas2SymbolsAndCheckThatErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aa");
    }

    public void testcase034TryToSignUpWithRepeatPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase034TryToSignUpWithRepeatPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatRolesErrorMessageAppearOnSignUpPage();
        actionwords.getScreenshot("testcase034TryToSignUpWithRepeatPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear_2");
    }

    @Test
    public void testcase034TryToSignUpWithRepeatPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppearDataSet1() {
        testcase034TryToSignUpWithRepeatPasswordWhichHas3SymbolsButWithoutRoleSoErrorAppear(websitelink, "Aaaa Aaaa", "roman", "aaa", "aaa", "aaa");
    }

    public void testcase035TryToSignUpWithUserThatRegisterBeforeWithTheSameDisplayNameAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase035TryToSignUpWithUserThatRegisterBeforeWithTheSameDisplayNameAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPageWithoutModifications(email);
        actionwords.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.getScreenshot("testcase035TryToSignUpWithUserThatRegisterBeforeWithTheSameDisplayNameAndCheckThatErrorAppear_2");
    }

    @Test
    public void testcase035TryToSignUpWithUserThatRegisterBeforeWithTheSameDisplayNameAndCheckThatErrorAppearDataSet1() {
        testcase035TryToSignUpWithUserThatRegisterBeforeWithTheSameDisplayNameAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "romanqa2@autotest.com", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase036TryToSignUpWithUserThatRegisterBeforeWithTheSameEmailAndCheckThatErrorAppear(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase036TryToSignUpWithUserThatRegisterBeforeWithTheSameEmailAndCheckThatErrorAppear_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setEmailOnSignUpPageWithoutModifications(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.getScreenshot("testcase036TryToSignUpWithUserThatRegisterBeforeWithTheSameEmailAndCheckThatErrorAppear_2");
    }

    @Test @Ignore
    public void testcase036TryToSignUpWithUserThatRegisterBeforeWithTheSameEmailAndCheckThatErrorAppearDataSet1() {
        testcase036TryToSignUpWithUserThatRegisterBeforeWithTheSameEmailAndCheckThatErrorAppear(websitelink, "Roman Krasniuk", "aaa@aaa.com", "ddd323212", "Romank_1992", "Romank_1992");
    }

    public void testcase037DeleteAllAutotestUsersTest() throws Exception {
        PrizmaAPI.deleteAutotestUsers();
    }

    @Test
    public void testcase037DeleteAllAutotestUsers() throws Exception {
        testcase037DeleteAllAutotestUsersTest();
    }
}
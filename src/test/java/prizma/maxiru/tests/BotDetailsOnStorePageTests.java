package prizma.maxiru.tests;

import org.junit.*;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class BotDetailsOnStorePageTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002DeleteAllBotsBeforeTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase002DeleteAllBotsBeforeTests");
    }

    @Test
    public void testcase002DeleteAllBotsBeforeTestsDataSet1() {
        testcase002DeleteAllBotsBeforeTests(websitelink);
    }

    public void testcase003UploadBot(String link, String fileName, String name, String rate, String description, int position, String botStatus) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.setBotNameOnUploadANewBotDialog(name);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.getScreenshot("testcase003UploadBotFormIsFilled");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(name, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase003BotIsUploaded");
    }

    @Test
    public void testcase003UploadBot() {
        testcase003UploadBot(websitelink, "bot_sample.zip", "autotest-bot", "20", "Uploading bot for auto tests", 1, "Preparing validation");
    }

    public void testcase004WaitUntilBotIsPublished(String link, String name) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(name);
        actionwords.getScreenshot("testcase004BotIsPublished");
    }

    @Test
    public void testcase004WaitUntilBotIsPublished() {
        testcase004WaitUntilBotIsPublished(websitelink, "autotest-bot");
    }

    public void testcase005OpenBotDetails(String link, String name, String rate, String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.clickOnBotOnStorePageToOpenActivateBotDialogByName(name);

        actionwords.verifyThatBotDetailsOnStorePageAreCorrect(name, "1", rate, "Coinbase, Binance",
                "aaa", description);
        actionwords.getScreenshot("testcase005APIKeysAreEntered");

    }

    @Test
    @Ignore
    public void testcase005OpenBotDetails() {
        testcase005OpenBotDetails(websitelink, "autotest-bot", "20%",
                "Uploading bot for auto tests");
    }

    public void testcase006DeleteAllBotsAfterTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase006DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase006DeleteAllBotsAfterTestsDataSet1() {
        testcase006DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase007LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase007LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase007LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase007LogoutFromAccount_3");
    }

    @Test
    public void testcase007LogoutFromAccountDataSet1() {
        testcase007LogoutFromAccount(websitelink);
    }
}
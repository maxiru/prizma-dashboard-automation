package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class BotUploadFullFlowBeforeTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePageDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_BACKTEST_TESTS_USER_A, PASSWORD_FOR_BACKTEST_TESTS_USER_A);
    }

    public void testcase002DeleteAllBotsBeforeTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase002DeleteAllBotsBeforeTests");
    }

    @Test
    public void testcase002DeleteAllBotsBeforeTestsDataSet1() {
        testcase002DeleteAllBotsBeforeTests(websitelink);
    }

    public void testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest(String link,
                                                                                                           String fileName,
                                                                                                           int position,
                                                                                                           String botName,
                                                                                                           String botStatus,
                                                                                                           String rate,
                                                                                                           String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_3");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_4");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_5");
    }

    @Test
    public void testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktestDataSet1() {
        testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest(websitelink, "bot_sample.zip", 1, "maxiru_auto_bot", "Preparing validation", "5", "Uploading bot for auto tests");
    }

    public void testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog(String link, String fileName, int position, String botName, String botStatus) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog("5");
        actionwords.setBotDescriptionOnUploadANewBotDialog("Uploading bot for auto tests");
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_3");
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_4");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_5");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog_6");
    }

    @Test
    public void testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialogDataSet1() {
        testcase004CheckThatPossibleToUploadPrivateValidBotOnUploadBotDialog(websitelink, "bot_sample.zip", 1, "maxiru_valid_pri", "Preparing validation");
    }

    public void testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog(String link, String fileName, int position, String botName, String botStatus) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_1");
        actionwords.clickOnUploadBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog("5");
        actionwords.setBotDescriptionOnUploadANewBotDialog("Uploading bot for auto tests");
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_3");
        actionwords.clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable();
        actionwords.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_4");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_5");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog_6");
    }

    @Test
    public void testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialogDataSet1() {
        testcase005CheckThatPossibleToUploadPrivateInvalidBotOnUploadBotDialog(websitelink, "bots_talib_invalid.zip", 1, "maxiru-qa-invalid", "Preparing validation");
    }

    public void testcase006DeleteAllBotsAfterTests(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase006SignInIntoPrizmaDashboard");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase006DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase006DeleteAllBotsAfterTestsDataSet1() {
        testcase006DeleteAllBotsAfterTests(websitelink);
    }
    
    public void testcase007LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_2");
    }

    @Test
    public void testcase007LogOutFromAccountDataSet1() {
        testcase007LogOutFromAccount(websitelink);
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class LogoutTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter_2");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfterDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002SignInWithCorrectUsernamePasswordSelectInvestorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.getScreenshot("testcase002SignInWithCorrectUsernamePasswordSelectInvestorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase002SignInWithCorrectUsernamePasswordSelectInvestorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter_2");
    }

    @Test
    public void testcase002SignInWithCorrectUsernamePasswordSelectInvestorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfterDataSet1() {
        testcase002SignInWithCorrectUsernamePasswordSelectInvestorRoleAndCheckThatUserRedirectToHomePageAndLogOutAfter(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }
}
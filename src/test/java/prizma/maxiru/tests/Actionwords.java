package prizma.maxiru.tests;

import app.*;
import prizma.maxiru.framework.common.PrizmaConstants;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaUtils;

public class Actionwords {

    public void setFocusInBrowser() {
        PrizmaUtils.setFocusInBrowser();
    }

    public void openWebsite(String link) {
        PrizmaUtils.openWebsite(link);
    }

    public void refreshPage() {
        PrizmaUtils.refreshPage();
    }

    public void getScreenshot(String fileName) {
        PrizmaDriver.getScreenshot(fileName);
    }

    public void clickOnSignInBtnOnSignInPage() {
        PrizmaSigninPage.clickOnSignInBtnOnSignInPage();
    }

    public void clickOnRegistrationBtnOnSignInPage() {
        PrizmaSigninPage.clickOnRegistrationBtnOnSignInPage();
    }

    public void verifyThatUserRedirectFromSignInPageToSignUpPage() {
        PrizmaSigninPage.verifyThatUserRedirectFromSignInPageToSignUpPage();
    }

    public void verifyThatUserRedirectFromSignInPageToHomePage() {
        PrizmaSigninPage.verifyThatUserRedirectFromSignInPageToHomePage();
    }

    public void verifyThatUserRedirectFromSignUpPageToHomePage() {
        PrizmaSignupPage.verifyThatUserRedirectFromSignUpPageToHomePage();
    }

    public void signInBackToAccount(String email, String password) {
        PrizmaSigninPage.signInBackToAccount(email, password);
    }

    public void setEmailOnSignInPage(String email) {
        PrizmaSigninPage.setEmailOnSignInPage(email);
    }

    public void setUsernameOnSignInPage(String username) {
        PrizmaSigninPage.setUsernameOnSignInPage(username);
    }

    public void setUsernameOnSignInPageForUserA() {
        PrizmaSigninPage.setUsernameOnSignInPageForUserA();
    }

    public void setEmailOnSignInPageForUserA() {
        PrizmaSigninPage.setEmailOnSignInPageForUserA();
    }

    public void setUsernameOnSignInPageForUserB() {
        PrizmaSigninPage.setUsernameOnSignInPageForUserB();
    }

    public void setEmailOnSignInPageForUserB() {
        PrizmaSigninPage.setEmailOnSignInPageForUserB();
    }

    public void setPasswordOnSignInPage(String password) {
        PrizmaSigninPage.setPasswordOnSignInPage(password);
    }

    public void setUsernameOnSignInPageWithEnter(String username) {
        PrizmaSigninPage.setUsernameOnSignInPageWithEnter(username);
    }

    public void setEmailOnSignInPageWithEnter(String email) {
        PrizmaSigninPage.setEmailOnSignInPageWithEnter(email);
    }

    public void setUsernameOnSignInPageWithTab(String username) {
        PrizmaSigninPage.setUsernameOnSignInPageWithTab(username);
    }

    public void setEmailOnSignInPageWithTab(String email) {
        PrizmaSigninPage.setEmailOnSignInPageWithTab(email);
    }

    public void setPasswordOnSignInPageWithEnter(String password) {
        PrizmaSigninPage.setPasswordOnSignInPageWithEnter(password);
    }

    public void setPasswordOnSignInPageWithTab(String password) {
        PrizmaSigninPage.setPasswordOnSignInPageWithTab(password);
    }

    public void verifyThatEmailErrorMessageAppearOnSignInPage() {
        PrizmaSigninPage.verifyThatEmailErrorMessageAppearOnSignInPage();
    }

    public void verifyThatUsernameErrorMessageAppearOnSignInPage() {
        PrizmaSigninPage.verifyThatUsernameErrorMessageAppearOnSignInPage();
    }

    public void verifyThatPasswordErrorMessageAppearOnSignInPage() {
        PrizmaSigninPage.verifyThatPasswordErrorMessageAppearOnSignInPage();
    }

    public void verifyThatIncorrectEmailPasswordErrorMessageAppearOnSignInPage() {
        PrizmaSigninPage.verifyThatIncorrectEmailPasswordErrorMessageAppearOnSignInPage();
    }

    public void clickOnShowPasswordBtnOnSignInPage() {
        PrizmaSigninPage.clickOnShowPasswordBtnOnSignInPage();
    }

    public void clickOnHidePasswordBtnOnSignInPage() {
        PrizmaSigninPage.clickOnHidePasswordBtnOnSignInPage();
    }

    public void clickOnShowPasswordBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnShowPasswordBtnOnSignUpPage();
    }

    public void clickOnHidePasswordBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnHidePasswordBtnOnSignUpPage();
    }

    public void clickOnShowRepeatPasswordBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnShowRepeatPasswordBtnOnSignUpPage();
    }

    public void clickOnHideRepeatPasswordBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnHideRepeatPasswordBtnOnSignUpPage();
    }

    public void setFullNameOnSignUpPage(String fullName) {
        PrizmaSignupPage.setFullNameOnSignUpPage(fullName);
    }

    public void setFullNameOnSignUpPageWithEnter(String fullName) {
        PrizmaSignupPage.setFirstNameOnSignUpPageWithEnter(fullName);
    }

    public void setFullNameOnSignUpPageWithTab(String fullName) {
        PrizmaSignupPage.setFullNameOnSignUpPageWithTab(fullName);
    }

    public void setEmailOnSignUpPage(String email) {
        PrizmaSignupPage.setEmailOnSignUpPage(email);
    }

    public void setEmailOnSignUpPageAndSaveUserAToVariable(String email) {
        PrizmaSignupPage.setEmailOnSignUpPageAndSaveUserAToVariable(email);
    }

    public void setEmailOnSignUpPageAndSaveUserBToVariable(String email) {
        PrizmaSignupPage.setEmailOnSignUpPageAndSaveUserBToVariable(email);
    }

    public void setEmailOnSignUpPageWithoutModifications(String email) {
        PrizmaSignupPage.setEmailOnSignUpPageWithoutModifications(email);
    }

    public void setEmailOnSignUpPageWithoutTimestampWithTab(String email) {
        PrizmaSignupPage.setEmailOnSignUpPageWithoutTimestampWithTab(email);
    }

    public void setEmailOnSignUpPageWithoutTimestampWithEnter(String email) {
        PrizmaSignupPage.setEmailOnSignUpPageWithoutTimestampWithEnter(email);
    }

    public void checkCreatorCheckboxOnSignUpPage() {
        PrizmaSignupPage.checkCreatorCheckboxOnSignUpPage();
    }

    public void uncheckCreatorCheckboxOnSignUpPage() {
        PrizmaSignupPage.uncheckCreatorCheckboxOnSignUpPage();
    }

    public void checkCreatorCheckboxOnSignUpPageWithEnter() {
        PrizmaSignupPage.checkCreatorCheckboxOnSignUpPageWithEnter();
    }

    public void uncheckCreatorCheckboxOnSignUpPageWithEnter() {
        PrizmaSignupPage.uncheckCreatorCheckboxOnSignUpPageWithEnter();
    }

    public void checkCreatorCheckboxOnSignUpPageWithTab() {
        PrizmaSignupPage.checkCreatorCheckboxOnSignUpPageWithTab();
    }

    public void checkInvestorCheckboxOnSignUpPage() {
        PrizmaSignupPage.checkInvestorCheckboxOnSignUpPage();
    }

    public void uncheckInvestorCheckboxOnSignUpPage() {
        PrizmaSignupPage.uncheckInvestorCheckboxOnSignUpPage();
    }

    public void checkInvestorCheckboxOnSignUpPageWithEnter() {
        PrizmaSignupPage.checkInvestorCheckboxOnSignUpPageWithEnter();
    }

    public void uncheckInvestorCheckboxOnSignUpPageWithEnter() {
        PrizmaSignupPage.uncheckInvestorCheckboxOnSignUpPageWithEnter();
    }

    public void checkInvestorCheckboxOnSignUpPageWithTab() {
        PrizmaSignupPage.checkInvestorCheckboxOnSignUpPageWithTab();
    }

    public void setDisplayNameOnSignUpPage(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPage(displayName);
    }

    public void setDisplayNameOnSignUpPageAndSaveUserAToVariable(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPageAndSaveUserAToVariable(displayName);
    }

    public void setDisplayNameOnSignUpPageAndSaveUserBToVariable(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPageAndSaveUserBToVariable(displayName);
    }

    public void setDisplayNameOnSignUpPageWithoutTimestamp(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPageWithoutTimestamp(displayName);
    }

    public void setDisplayNameOnSignUpPageWithoutTimestampWithEnter(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPageWithoutTimestampWithEnter(displayName);
    }

    public void setDisplayNameOnSignUpPageWithoutTimestampWithTab(String displayName) {
        PrizmaSignupPage.setDisplayNameOnSignUpPageWithoutTimestampWithTab(displayName);
    }

    public void setPasswordOnSignUpPage(String password) {
        PrizmaSignupPage.setPasswordOnSignUpPage(password);
    }

    public void setPasswordOnSignUpPageWithEnter(String password) {
        PrizmaSignupPage.setPasswordOnSignUpPageWithEnter(password);
    }

    public void setPasswordOnSignUpPageWithTab(String password) {
        PrizmaSignupPage.setPasswordOnSignUpPageWithTab(password);
    }

    public void setRepeatPasswordOnSignUpPage(String repeatPassword) {
        PrizmaSignupPage.setRepeatPasswordOnSignUpPage(repeatPassword);
    }

    public void setRepeatPasswordOnSignUpPageWithEnter(String repeatPassword) {
        PrizmaSignupPage.setRepeatPasswordOnSignUpPageWithEnter(repeatPassword);
    }

    public void setRepeatPasswordOnSignUpPageWithTab(String repeatPassword) {
        PrizmaSignupPage.setRepeatPasswordOnSignUpPageWithTab(repeatPassword);
    }

    public void clickOnRegisterBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnRegisterBtnOnSignUpPage();
    }

    public void clickOnSignInBtnOnSignUpPage() {
        PrizmaSignupPage.clickOnSignInBtnOnSignUpPage();
    }

    public void verifyThatFirstNameErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatFirstNameErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatLastNameErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatLastNameErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatRolesErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatRolesErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatDisplayNameErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatDisplayNameErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatEmailErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatEmailErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatPasswordErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatPasswordErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatUserAlreadyExistErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatUserAlreadyExistErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatEmailAlreadyExistErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatEmailAlreadyExistErrorMessageAppearOnSignUpPage();
    }

    public void verifyThatUserRedirectFromSignUpPageToSignInPage() {
        PrizmaSignupPage.verifyThatUserRedirectFromSignUpPageToSignInPage();
    }

    public void verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage() {
        PrizmaSignupPage.verifyThatPasswordNotMatchErrorMessageAppearOnSignUpPage();
    }

    public void clickOnLogOutBtnOnProfilePage() {
        PrizmaAccountPage.clickOnLogOutButton();
    }

    public void clickOnAccountBtnOnHomePage() {
        PrizmaHomePage.clickOnAccountButton();
    }

    public void clickOnCreatorBtnOnHomePage() {
        PrizmaHomePage.clickOnCreatorBtnOnHomePage();
    }

    public void clickOnInvestorBtnOnHomePage() {
        PrizmaHomePage.clickOnInvestorBtnOnHomePage();
    }

    public void clickOnStoreBtnOnHomePage() {
        PrizmaHomePage.clickOnStoreBtnOnHomePage();
    }

    public void verifyThatUserRedirectFromHomePageToSignInPage() {
        PrizmaHomePage.verifyThatUserRedirectFromHomePageToSignInPage();
    }

    public void verifyBotNameLabelFromVariableInUploadedBotsSectionOnPosition(int position) {
        PrizmaCreatorPage.verifyBotNameLabelFromVariableInUploadedBotsSectionOnPosition(position);
    }

    public void verifyBotNameLabelInUploadedBotsSectionOnPosition(String botName, int position) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaCreatorPage.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
    }

    public void verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(String botName) {
        PrizmaCreatorPage.verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(botName);
    }

    public void verifyBotVersionLabelInUploadedBotsSectionOnPosition(String botVersion, int position) {
        PrizmaCreatorPage.verifyBotVersionLabelInUploadedBotsSectionOnPosition(botVersion, position);
    }

    public void verifyBotBotsEmptyLabelInUploadedBotsSection(String message) {
        PrizmaCreatorPage.verifyBotBotsEmptyLabelInUploadedBotsSection(message);
    }

    public void verifyBotStatusLabelInUploadedBotsSectionOnPosition(String botStatus, int position) {
        PrizmaCreatorPage.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
    }

    public void verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(String botStatus) {
        PrizmaCreatorPage.verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(botStatus);
    }

    public void verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(String name) {
        PrizmaCreatorPage.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(name);
    }

    public void verifyUntilBotStatusIsFailedInUploadedBotsSectionByBotName(String name) {
        PrizmaCreatorPage.verifyUntilBotStatusIsFailedInUploadedBotsSectionByBotName(name);
    }

    public void clickOnDeleteBtn() {

        PrizmaCreatorPage.clickOnDeleteBtn();
    }

    public void clickOnDownloadBtn(int timer) {

        PrizmaCreatorPage.clickOnDownloadBtn(timer);
    }

    public void clickOnPrivateCheckboxToEnable() {
        PrizmaCreatorPage.clickOnPrivateCheckboxToEnable();
    }

    public void clickOnPrivateCheckboxToDisable() {
        PrizmaCreatorPage.clickOnPrivateCheckboxToDisable();
    }

    public void verifyInvestorsCountLabelInUploadedBotsSectionOnPosition(String investorsCount, int position) {
        PrizmaCreatorPage.verifyInvestorsCountLabelInUploadedBotsSectionOnPosition(investorsCount, position);
    }

    public void verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(String investorsCount) {
        PrizmaCreatorPage.verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(investorsCount);
    }

    public void verifyProfitAllTimeLabelInUploadedBotsSectionOnPosition(String profitValue, int position) {
        PrizmaCreatorPage.verifyProfitAllTimeLabelInUploadedBotsSectionOnPosition(profitValue, position);
    }

    public void verifyProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(String profitValue, int position) {
        PrizmaCreatorPage.verifyProfitLastThreeMonthLabelInUploadedBotsSectionOnPosition(profitValue, position);
    }

    public void verifyProfitSinceLastPayoutLabelInUploadedBotsSection(String profitValue, int position) {
        PrizmaCreatorPage.verifyProfitSinceLastPayoutLabelInUploadedBotsSection(profitValue, position);
    }

    public void verifyPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaCreatorPage.verifyPerformanceAllTimeLabelInUploadedBotsSectionOnPosition(performanceValue, position);
    }

    public void verifyPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaCreatorPage.verifyPerformanceLastThreeMonthLabelInUploadedBotsSectionOnPosition(performanceValue, position);
    }

    public void verifyPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(String performanceValue, int position) {
        PrizmaCreatorPage.verifyPerformanceSinceLastPayoutLabelInUploadedBotsSectionOnPosition(performanceValue, position);
    }

    public void verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotNameLimitErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotNameLimitErrorMessageNotAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotNameLimitErrorMessageNotAppearOnUploadBotDialog();
    }

    public void verifyThatBotNameErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotNameErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotNameAlreadyExistErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotNameValidationFailedErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotNameValidationFailedErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotUploadMandatoryErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotUploadMandatoryErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotUploadInvalidFileErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotUploadInvalidFileErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotUploadInvalidFileSizeErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotUploadFileSizeErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatBotUploadNoPermissionErrorMessageAppearOnUploadBotDialog() {
        PrizmaCreatorPage.verifyThatBotUploadNoPermissionErrorMessageAppearOnUploadBotDialog();
    }

    public void verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot() {
        PrizmaCreatorPage.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithIncorrectDataBot();
    }

    public void verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot() {
        PrizmaCreatorPage.verifyThatUploadBotDialogNotCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
    }

    public void verifyThatUploadBotDialogCloseAfterClickOnCancelBtn() {
        PrizmaCreatorPage.verifyThatUploadBotDialogCloseAfterClickOnCancelBtn();
    }

    public void clickOnUploadAnotherBotBtnOnCreatorPage() {
        PrizmaCreatorPage.clickOnUploadAnotherBotBtnOnCreatorPage();
    }

    public void clickOnUploadBotBtnOnCreatorPage() {
        PrizmaCreatorPage.clickOnUploadBotBtnOnCreatorPage();
    }

    public void clickOnPrivateBotCheckboxOnUploadBotDialogToDisable() {
        PrizmaCreatorPage.clickOnPrivateBotCheckboxOnUploadBotDialogToDisable();
    }

    public void clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable() {
        PrizmaCreatorPage.clickOnPrivateBotCheckboxOnUploadBotDialogAgainToEnable();
    }

    public void clickOnBotUploadConfirmBtnOnUploadBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaCreatorPage.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public void clickOnBotUploadCancelBtnOnUploadBotDialog() {
        PrizmaCreatorPage.clickOnBotUploadCancelBtnOnUploadBotDialog();
    }

    public void clickOnDownloadPrizmaBtnOnCreatorPage(String fileName, int timer) {
        PrizmaCreatorPage.clickOnDownloadPrizmaBtnOnCreatorPage(fileName, timer);
    }

    public void setBotNameOnUploadANewBotDialog(String botName) {
        PrizmaCreatorPage.setBotNameOnUploadANewBotDialog(botName);
    }

    public void setBotRateOnUploadANewBotDialog(String botRate) {
        PrizmaCreatorPage.setBotRateOnUploadANewBotDialog(botRate);
    }

    public void setBotDescriptionOnUploadANewBotDialog(String botDescription) {
        PrizmaCreatorPage.setBotDescriptionOnUploadANewBotDialog(botDescription);
    }

    public void setBotNameOnUploadANewBotDialogWhichIsAutoGenerated() {
        PrizmaCreatorPage.setBotNameOnUploadANewBotDialogWhichIsAutoGenerated();
    }

    public void setBotNameOnUploadANewBotDialogWithEnter(String botName) {
        PrizmaCreatorPage.setBotNameOnUploadANewBotDialogWithEnter(botName);
    }

    public void uploadNewBotZipFile(String fileName) {

        PrizmaCreatorPage.uploadNewBotZipFile(fileName);
    }

    public void clickOnBotNameInUnloadBotsSectionByPosition(int position) {
        PrizmaCreatorPage.clickOnBotNameInUnloadBotsSectionByPosition(position);
    }

    public void verifyNextPayoutEarnedLabel(String count) {

        PrizmaCreatorPage.verifyNextPayoutEarnedLabel(count);
    }

    public void verifyTotalEarnedCountLabel(String count) {

        PrizmaCreatorPage.verifyTotalEarnedCountLabel(count);
    }

    public void verifyTotalInvestorsCountLabel(String count) {
        PrizmaCreatorPage.verifyTotalInvestorsCountLabel(count);
    }

    public void verifyBotsUploadCountLabel(String count) {

        PrizmaCreatorPage.verifyBotsUploadCountLabel(count);
    }

    public void deleteTheDownloadFolder() {

        PrizmaUtils.deleteTheDownloadFolder();
    }

    public void createTheDownloadFolder() {

        PrizmaUtils.createTheDownloadFolder();
    }

    public void checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaDashboard() {
        PrizmaUtils.checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaDashboard();
    }

    public void clickOnBotDetailsCloseDialogBtn() {

        PrizmaCreatorBotDetailsPage.clickOnBotDetailsCloseDialogBtn();
    }

    public void clickOnBotDetailsDeleteBtn() {

        PrizmaCreatorBotDetailsPage.clickOnBotDetailsDeleteBtn();
    }

    public void clickOnBotDetailsDownloadBtn(int timer) {

        PrizmaCreatorBotDetailsPage.clickOnBotDetailsDownloadBtn(timer);
    }

    public void clickOnBotDetailsPrivateCheckboxToEnable() {
        PrizmaCreatorBotDetailsPage.clickOnBotDetailsPrivateCheckboxToEnable();
    }

    public void clickOnBotDetailsPrivateCheckboxToDisable() {
        PrizmaCreatorBotDetailsPage.clickOnBotDetailsPrivateCheckboxToDisable();
    }

    public void verifyThatBotDetailsDialogCloseAfterClickOnCloseBtn() {
        PrizmaCreatorBotDetailsPage.verifyThatBotDetailsDialogCloseAfterClickOnCloseBtn();
    }

    public void verifyThatBotDetailsEmptyStatisticsTextPresentOnBotDetailsDialog(String fileName) {
        PrizmaCreatorBotDetailsPage.verifyThatBotDetailsEmptyStatisticsTextPresentOnBotDetailsDialog(fileName);
    }

    public void clickOnViewStoreBtnOnInvestorPage() {
        PrizmaInvestorPage.clickOnViewStoreBtnOnInvestorPage();
    }

    public void verifyThatUserRedirectFromInvestorPageToStorePageAfterClickOnViewStoreBtn() {
        PrizmaInvestorPage.verifyThatUserRedirectFromInvestorPageToStorePageAfterClickOnViewStoreBtn();
    }

    public void clickOnUserAgreementOpenBtnOnInvestorPage() {
        PrizmaInvestorPage.clickOnUserAgreementOpenBtnOnInvestorPage();
    }

    public void clickOnUserAgreementCloseBtnOnInvestorPage() {
        PrizmaInvestorPage.clickOnUserAgreementCloseBtnOnInvestorPage();
    }

    public void clickOnUserAgreementToggleOnInvestorPageToEnable() {
        PrizmaInvestorPage.clickOnUserAgreementToggleOnInvestorPageToEnable();
    }

    public void clickOnUserAgreementToggleOnInvestorPageToDisable() {
        PrizmaInvestorPage.clickOnUserAgreementToggleOnInvestorPageToDisable();
    }

    public void clickOnStartInvestingBtnOnInvestorPage() {
        PrizmaInvestorPage.clickOnStartInvestingBtnOnInvestorPage();
    }

    public void verifyThatInvestorSplashScreenPresentOnInvestorPage() {
        PrizmaInvestorPage.verifyThatInvestorSplashScreenPresentOnInvestorPage();
    }

    public void verifyThatInvestorSplashScreenNotPresentOnInvestorPage() {
        PrizmaInvestorPage.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
    }

    public void clickOnUserAgreementOpenBtnOnCreatorPage() {
        PrizmaCreatorPage.clickOnUserAgreementOpenBtnOnCreatorPage();
    }

    public void clickOnUserAgreementCloseBtnOnCreatorPage() {
        PrizmaCreatorPage.clickOnUserAgreementCloseBtnOnCreatorPage();
    }

    public void clickOnUserAgreementToggleOnCreatorPageToEnable() {
        PrizmaCreatorPage.clickOnUserAgreementToggleOnCreatorPageToEnable();
    }

    public void clickOnUserAgreementToggleOnCreatorPageToDisable() {
        PrizmaCreatorPage.clickOnUserAgreementToggleOnCreatorPageToDisable();
    }

    public void clickOnStartCreatingBtnOnCreatorPage() {
        PrizmaCreatorPage.clickOnStartCreatingBtnOnCreatorPage();
    }

    public void verifyThatCreatorSplashScreenPresentOnCreatorPage() {
        PrizmaCreatorPage.verifyThatCreatorSplashScreenPresentOnCreatorPage();
    }

    public void verifyThatCreatorSplashScreenNotPresentOnCreatorPage() {
        PrizmaCreatorPage.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
    }

    public void clickOnStoreTableSwitcherIcon() {
        PrizmaStorePage.clickOnStoreTableSwitcherIcon();
    }

    public void clickOnStoreGridSwitcherIcon() {
        PrizmaStorePage.clickOnStoreGridSwitcherIcon();
    }

    public void clickOnStoreTableAscDescIcon() {
        PrizmaStorePage.clickOnStoreTableAscDescIcon();
    }

    public void clickOnStoreShowMoreButton() {
        PrizmaStorePage.clickOnStoreShowMoreButton();
    }

    public void sortByNumberOfInvestors(String value) {
        PrizmaStorePage.sortByValue(value);
    }

    public void clickOnCreatedByMeCheckboxToEnable() {
        PrizmaStorePage.clickOnCreatedByMeCheckboxToEnable();
    }

    public void clickOnCreatedByMeCheckboxToDisable() {
        PrizmaStorePage.clickOnCreatedByMeCheckboxToDisable();
    }

    public void verifyThatCreatedByMeCheckboxIsPresent() {
        PrizmaStorePage.verifyThatCreatedByMeCheckboxIsPresent();
    }

    public void verifyThatCreatedByMeCheckboxIsNotPresent() {
        PrizmaStorePage.verifyThatCreatedByMeCheckboxIsNotPresent();
    }

    public void verifyThatCreatedByMeCheckboxIsEnabled() {
        PrizmaStorePage.verifyThatCreatedByMeCheckboxIsEnabled();
    }

    public void verifyThatCreatedByMeCheckboxIsDisabled() {
        PrizmaStorePage.verifyThatCreatedByMeCheckboxIsDisabled();
    }

    public void verifyBotNameInStoreMainTableByPosition(String botName, int position) {
        PrizmaStorePage.verifyBotNameInStoreMainTableByPosition(botName, position);
    }

    public void verifyBotNameInStoreMainTableByPositionFromVariable(String botName) {
        PrizmaStorePage.verifyBotNameInStoreMainTableByPositionFromVariable(botName);
    }

    public void verifyBotNameFromVariableInStoreMainTableByPositionFromVariable() {
        PrizmaStorePage.verifyBotNameFromVariableInStoreMainTableByPositionFromVariable();
    }

    public void verifyBotInvestorCountInStoreMainTableByPosition(String botInvestorCount, int position) {
        PrizmaStorePage.verifyBotInvestorCountInStoreMainTableByPosition(botInvestorCount, position);
    }

    public void verifyBotInvestorCountInStoreMainTableByPositionFromVariable(String botInvestorCount) {
        PrizmaStorePage.verifyBotInvestorCountInStoreMainTableByPositionFromVariable(botInvestorCount);
    }

    public void verifyBotRiskLevelInStoreMainTableByPosition(String botRiskLevel, int position) {
        PrizmaStorePage.verifyBotRiskLevelInStoreMainTableByPosition(botRiskLevel, position);
    }

    public void verifyBotRiskLevelInStoreMainTableByPositionFromVariable(String botRiskLevel) {
        PrizmaStorePage.verifyBotRiskLevelInStoreMainTableByPositionFromVariable(botRiskLevel);
    }

    public void verifyBotPublishedDateInStoreMainTableByPosition(String botPublishedDate, int position) {
        PrizmaStorePage.verifyBotPublishedDateInStoreMainTableByPosition(botPublishedDate, position);
    }

    public void verifyBotPublishedDateInStoreMainTableByPositionFromVariable(String botPublishedDate) {
        PrizmaStorePage.verifyBotPublishedDateInStoreMainTableByPositionFromVariable(botPublishedDate);
    }

    public void verifyBotPublishedDateAutogeneratedInStoreMainTableByPositionFromVariable() {
        PrizmaStorePage.verifyBotPublishedDateAutogeneratedInStoreMainTableByPositionFromVariable();
    }

    public void verifyBotPerfomanceBacktestInStoreMainTableByPosition(String botPerfomanceBacktestValue, int position) {
        PrizmaStorePage.verifyBotPerfomanceBacktestInStoreMainTableByPosition(botPerfomanceBacktestValue, position);
    }

    public void verifyBotPerfomanceBacktestInStoreMainTableByPositionFromVariable(String botPerfomanceBacktestValue) {
        PrizmaStorePage.verifyBotPerfomanceBacktestInStoreMainTableByPositionFromVariable(botPerfomanceBacktestValue);
    }

    public void verifyBotPerfomanceAllTimeInStoreMainTableByPosition(String botAllTimeValue, int position) {
        PrizmaStorePage.verifyBotPerfomanceAllTimeInStoreMainTableByPosition(botAllTimeValue, position);
    }

    public void verifyBotPerfomanceAllTimeInStoreMainTableByPositionFromVariable(String botAllTimeValue) {
        PrizmaStorePage.verifyBotPerfomanceAllTimeInStoreMainTableByPositionFromVariable(botAllTimeValue);
    }

    public void verifyThatBotPresentInListOfBotsOnStorePage(String botName, String fileName) {
        PrizmaStorePage.verifyThatBotPresentInListOfBotsOnStorePage(botName, fileName);
    }

    public void verifyThatBotFromVariablePresentInListOfBotsOnStorePage() {
        PrizmaStorePage.verifyThatBotFromVariablePresentInListOfBotsOnStorePage();
    }

    public void verifyThatBotNotPresentInListOfBotsOnStorePage(String botName, String fileName) {
        PrizmaStorePage.verifyThatBotNotPresentInListOfBotsOnStorePage(botName, fileName);
    }

    public void verifyThatBotFromVariableNotPresentInListOfBotsOnStorePage(String fileName) {
        PrizmaStorePage.verifyThatBotFromVariableNotPresentInListOfBotsOnStorePage(fileName);
    }

    public void clickOnBotsPerPageSelectorAndSelectOption1() {
        PrizmaStorePage.clickOnBotsPerPageSelectorAndSelectOption1();
    }

    public void clickOnBotsPerPageSelectorAndSelectOption2() {
        PrizmaStorePage.clickOnBotsPerPageSelectorAndSelectOption2();
    }

    public void clickOnBotsPerPageSelectorAndSelectOption3() {
        PrizmaStorePage.clickOnBotsPerPageSelectorAndSelectOption3();
    }

    public void clickOnBotsPerPageSelectorAndSelectOption4() {
        PrizmaStorePage.clickOnBotsPerPageSelectorAndSelectOption4();
    }

    public void clickOnBotsNextPageButton() {
        PrizmaStorePage.clickOnBotsNextPageButton();
    }

    public void clickOnBotsPreviousPageButton() {
        PrizmaStorePage.clickOnBotsPreviousPageButton();
    }

    public void clickOnBotsSecondPageButton() {
        PrizmaStorePage.clickOnBotsSecondPageButton();
    }

    public void clickOnBotsThirdPageButton() {
        PrizmaStorePage.clickOnBotsThirdPageButton();
    }

    public void verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(int validationCount) {
        PrizmaStorePage.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount);
    }

    public void verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilterAndIsNotMoreThenMax(int validationCount) {
        PrizmaStorePage.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilterAndIsNotMoreThenMax(validationCount);
    }

    public void saveBotFromPositionOneForFutureTests() {
        PrizmaStorePage.saveBotFromPositionOneForFutureTests();
    }

    public void saveBotFromPositionOneForFutureTestsOnInvestorPage() {
        PrizmaInvestorPage.saveBotFromPositionOneForFutureTests();
    }

    public void saveBotFromPositionXForFutureTestsOnInvestorPage(int position) {
        PrizmaInvestorPage.saveBotFromPositionXForFutureTests(position);
    }

    public void verifyThatBotPresentInListOfBotsOnInvestorPage(String botName, String fileName) {
        PrizmaInvestorPage.verifyThatBotPresentInListOfBotsOnInvestorPage(botName, fileName);
    }

    public void verifyThatBotPresentInListOfBotsOnInvestorPage(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.WAIT_TIME);
        PrizmaInvestorPage.verifyThatBotPresentInListOfBotsOnInvestorPage(botName);
    }

    public void verifyThatBotFromVariablePresentInListOfBotsOnInvestorPage() {
        PrizmaInvestorPage.verifyThatBotFromVariablePresentInListOfBotsOnInvestorPage();
    }

    public void verifyThatBotNotPresentInListOfBotsOnInvestorPage(String botName, String fileName) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaInvestorPage.verifyThatBotNotPresentInListOfBotsOnInvestorPage(botName, fileName);
    }

    public void verifyThatBotNotPresentInListOfBotsOnInvestorPage(String botName) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaInvestorPage.verifyThatBotNotPresentInListOfBotsOnInvestorPage(botName);
    }

    public void verifyThatBotFromVariableNotPresentInListOfBotsOnInvestorPage(String fileName) {
        PrizmaInvestorPage.verifyThatBotFromVariableNotPresentInListOfBotsOnInvestorPage(fileName);
    }

    public void verifyThatBotToggleForBotOnPositionXIsEnabled(int position) {
        PrizmaInvestorPage.verifyThatBotToggleForBotOnPositionXIsEnabled(position);
    }

    public void verifyThatBotToggleForBotOnPositionXIsDisabled(int position) {
        PrizmaInvestorPage.verifyThatBotToggleForBotOnPositionXIsDisabled(position);
    }

    public void verifyThatBotToggleForBotOnPositionFromVariableIsEnabled() {
        PrizmaInvestorPage.verifyThatBotToggleForBotOnPositionFromVariableIsEnabled();
    }

    public void verifyThatBotToggleForBotOnPositionFromVariableIsDisabled() {
        PrizmaInvestorPage.verifyThatBotToggleForBotOnPositionFromVariableIsDisabled();
    }

    public void verifyBotNameInInvestorTableByPosition(String botInfo, int position) {
        PrizmaInvestorPage.verifyBotNameInInvestorTableByPosition(botInfo, position);
    }

    public void verifyBotNameInvestorTableByPositionByPositionFromVariable(String botInfo) {
        PrizmaInvestorPage.verifyBotNameInvestorTableByPositionByPositionFromVariable(botInfo);
    }

    public void clickOnBotDetailsViewErrorDetailsDialogBtn() {
        PrizmaCreatorBotDetailsPage.clickOnBotDetailsViewErrorDetailsDialogBtn();
    }

    public void verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent() {
        PrizmaCreatorPage.verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent();
    }

    public void verifyThatBotDetailsViewErrorDetailsDialogBtnIsNotPresent() {
        PrizmaCreatorPage.verifyThatBotDetailsViewErrorDetailsDialogBtnIsNotPresent();
    }

    public void getAndStoreBotPositionOnCreatorPage(String botName) {
        PrizmaCreatorPage.getAndStoreBotPositionOnCreatorPage(botName);
    }

    public void clickOnBotNameInUnloadBotsSectionByPositionFromVariable() {
        PrizmaCreatorPage.clickOnBotNameInUnloadBotsSectionByPositionFromVariable();
    }

    public void clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(int position) {
        PrizmaStorePage.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionX(position);
    }

    public void clickOnBotOnStorePageToOpenActivateBotDialogByName(String name) {
        PrizmaStorePage.clickOnBotOnStorePageToOpenActivateBotDialogByName(name);
    }

    public void clickOnBotOnInvestorPageToOpenBotDialogByName(String name) {
        PrizmaInvestorPage.clickOnBotOnInvestorPageToOpenBotDetailsByName(name);
    }

    public void clickOnBotOnStorePageToOpenActivateBotDialogOnPositionFromVariable() {
        PrizmaStorePage.clickOnBotOnStorePageToOpenActivateBotDialogOnPositionFromVariable();
    }

    public void clickOnActivateBotCloseDialogBtn() {
        PrizmaActivateBotPage.clickOnActivateBotCloseDialogBtn();
    }

    public void verifyThatActivateBotDialogCloseAfterClickOnCloseBtn() {
        PrizmaActivateBotPage.verifyThatActivateBotDialogCloseAfterClickOnCloseBtn();
    }

    public void verifyThatUserNotRedirectToSecondPageOfActivateBotFlow() {
        PrizmaActivateBotPage.verifyThatUserNotRedirectToSecondPageOfActivateBotFlow();
    }

    public void clickOnActivateBotCancelBtn() {
        PrizmaActivateBotPage.clickOnActivateBotCancelBtn();
    }

    public void clickOnActivateBotNextBtn() {
        PrizmaActivateBotPage.clickOnActivateBotNextBtn();
    }

    public void clickOnConnectBotBtn() {
        PrizmaActivateBotPage.clickOnConnectBotBtn();
    }

    public void clickOnActivateBotConnectBtn() {
        PrizmaActivateBotPage.clickOnActivateBotConnectBtn();
    }

    public void verifyThanNextBtnIsDisabled() {
        PrizmaActivateBotPage.verifyThanNextBtnIsDisabled();
    }

    public void verifyThatActivateBotDialogCloseAfterClickOnCancelBtn() {
        PrizmaActivateBotPage.verifyThatActivateBotDialogCloseAfterClickOnCancelBtn();
    }

    public void clickOnActivateBotSelectExchangeDropDownAndSelectBinanceOption() {
        PrizmaActivateBotPage.clickOnActivateBotSelectExchangeDropDownAndSelectBinanceOption();
    }

    public void clickOnActivateBotSelectExchangeDropDownAndSelectCoinbaseOption() {
        PrizmaActivateBotPage.clickOnActivateBotSelectExchangeDropDownAndSelectCoinbaseOption();
    }

    public void clickOnActivateBotOpenUserAgreementBtn() {
        PrizmaActivateBotPage.clickOnActivateBotOpenUserAgreementBtn();
    }

    public void clickOnActivateBotUserAgreementCloseBtn() {
        PrizmaActivateBotPage.clickOnActivateBotUserAgreementCloseBtn();
    }

    public void setApiKeyOnActivateBotDialog(String apiKey) {
        PrizmaActivateBotPage.setApiKeyOnActivateBotDialog(apiKey);
    }

    public void setApiSecretKeyOnActivateBotDialog(String apiSecretKey) {
        PrizmaActivateBotPage.setApiSecretKeyOnActivateBotDialog(apiSecretKey);
    }

    public void setPassphraseOnActivateBotDialog(String passphrase) {
        PrizmaActivateBotPage.setPassphraseOnActivateBotDialog(passphrase);
    }

    public void verifyThatActivateBotErrorMessageForBinanceExchange() {
        PrizmaActivateBotPage.verifyThatActivateBotErrorMessageForBinanceExchange();
    }

    public void verifyThatActivateBotErrorMessageForCoinbaseExchange() {
        PrizmaActivateBotPage.verifyThatActivateBotErrorMessageForCoinbaseExchange();
    }

    public void clickOnActivateBotUserAgreementCheckboxToEnable() {
        PrizmaActivateBotPage.clickOnActivateBotUserAgreementCheckboxToEnable();
    }

    public void clickOnActivateBotUserAgreementCheckboxToDisable() {
        PrizmaActivateBotPage.clickOnActivateBotUserAgreementCheckboxToDisable();
    }

    public void verifyThatActivateBotUserAgreementCheckboxIsEnabled() {
        PrizmaActivateBotPage.verifyThatActivateBotUserAgreementCheckboxIsEnabled();
    }

    public void verifyThatActivateBotUserAgreementCheckboxIsDisabled() {
        PrizmaActivateBotPage.verifyThatActivateBotUserAgreementCheckboxIsDisabled();
    }

    public void verifyThatActivateBotExchangeValueForBinance() {
        PrizmaActivateBotPage.verifyThatActivateBotExchangeValueForBinance();
    }

    public void verifyThatActivateBotExchangeValueForCoinbase() {
        PrizmaActivateBotPage.verifyThatActivateBotExchangeValueForCoinbase();
    }

    public void clickOnActivateBotReinvestProfitCheckboxToEnable() {
        PrizmaActivateBotPage.clickOnActivateBotReinvestProfitCheckboxToEnable();
    }

    public void clickOnActivateBotReinvestProfitCheckboxToDisable() {
        PrizmaActivateBotPage.clickOnActivateBotReinvestProfitCheckboxToDisable();
    }

    public void clickOnNotifyIfLowerCheckboxToEnable() {
        PrizmaActivateBotPage.clickOnNotifyIfLowerCheckboxToEnable();
    }

    public void clickOnNotifyIfLowerCheckboxToDisable() {
        PrizmaActivateBotPage.clickOnNotifyIfLowerCheckboxToDisable();
    }

    public void verifyThatActivateBotReinvestProfitCheckboxIsEnabled() {
        PrizmaActivateBotPage.verifyThatActivateBotReinvestProfitCheckboxIsEnabled();
    }

    public void verifyThatActivateBotReinvestProfitCheckboxIsDisabled() {
        PrizmaActivateBotPage.verifyThatActivateBotReinvestProfitCheckboxIsDisabled();
    }

    public void verifyThatBotDetailsOnStorePageAreCorrect(String botName, String investors, String rate,
                                                          String exchanges, String author, String description) {
        PrizmaActivateBotPage.verifyThatBotDetailsOnStorePageAreCorrect(botName, investors, rate,
                exchanges, author, description);
    }

    public void setBotBalanceOnActivateBotDialog(String botBalanceValue) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaActivateBotPage.setBotBalanceOnActivateBotDialog(botBalanceValue);
    }

    public void setBotStopLostOnActivateBotDialog(String botStopLostValue) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaActivateBotPage.setBotStopLostOnActivateBotDialog(botStopLostValue);
    }

    public void deleteAllBotsFromThePage() {
        PrizmaCreatorPage.deleteAllBotsFromThePage();
    }

    public void verifyThatTotalReturnOnInvestorPageIsCorrect() {
        PrizmaInvestorPage.verifyThatTotalReturnIsCorrect();
    }

    public void verifyThatTotalInvestedAmountOnInvestorPageIsCorrect() {
        PrizmaInvestorPage.verifyThatTotalInvestedAmountIsCorrect();
    }

    public void verifyThatTotalAssetsOnInvestorPageIsCorrect() {
        PrizmaInvestorPage.verifyThatTotalAssetsIsCorrect();
    }

    public void verifyThatBotAvatarInBotsListOnInvestorPageIsCorrect(int position) {
        PrizmaInvestorPage.verifyThatBotAvatarIsCorrect(position);
    }

    public void verifyThatBotExchangeInBotsListOnInvestorPageIsCorrect(String exchange, int position) {
        PrizmaInvestorPage.verifyThatBotExchangeIsCorrect(exchange, position);
    }

    public void verifyThatBotActivationDateInBotsListOnInvestorPageIsCorrect(int position) {
        PrizmaInvestorPage.verifyThatBotActivationDateIsCorrect(position);
    }

    public void verifyThatBotRateInBotsListOnInvestorPageIsCorrect(int position) {
        PrizmaInvestorPage.verifyThatBotRateCorrect(position);
    }

    public void verifyThatStateInBotsListOnInvestorPageIsCorrect(String state, int position) {
        PrizmaInvestorPage.verifyThatStateIsCorrect(state, position);
    }

    public void verifyThatBotProfitInListEqualsProfitInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatBotProfitEqualsProfitInBotsDetails(botName, position);
    }

    public void verifyThatBotPerformanceInListEqualsPerformanceInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatBotPerformanceEqualsPerformanceInBotsDetails(botName, position);
    }

    public void verifyThatBotInvestedAmountInListEqualsAmountInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatBotInvestedAmountEqualsAmountInBotsDetails(botName, position);
    }

    public void verifyThatExitAmountInListEqualsAmountInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatExitAmountEqualsAmountInBotsDetails(botName, position);
    }

    public void verifyThatCurrentAssetsInStablecoinInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatCurrentAssetsInStablecoinEqualsAssetsInBotsDetails(botName, position);
    }

    public void verifyThatBoughtSymbolAssetsInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatBoughtSymbolAssetsEqualsAssetsInBotsDetails(botName, position);
    }

    public void verifyThatBoughtSymbolAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatBoughtSymbolAssetsInUsdEqualsAssetsInBotsDetails(botName, position);
    }

    public void verifyThatTotalAssetsInUsdInListEqualsAssetsInBotDetailsOnInvestorPage(String botName, int position) {
        PrizmaInvestorPage.verifyThatTotalAssetsAssetsInUsdEqualsAssetsInBotsDetails(botName, position);
    }

    public void clickOnDeactivateBotButton(int position) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaInvestorPage.clickOnDeleteBotButton(position);
    }

    public void clickOnBotOkBtnOnDeactivateBotDialog() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaInvestorPage.clickOnBotOkBtnOnDeactivateBotDialog();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public void verifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatBotAvatarEqualsAvatarInBotsList(position);
    }

    public void verifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatBotExchangeEqualsExchangeInBotsList(position);
    }

    public void verifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatBotActivationDateEqualsDateInBotsList(position);
    }

    public void verifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatBotRateEqualsRateInBotsList(position);
    }

    public void verifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatStateEqualsStateInBotsList(position);
    }

    public void verifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatTotalProfitInDetailsEqualsProfitInPositions();
    }

    public void verifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatPerformanceInDetailsEqualsPerformanceInPositions();
    }

    public void verifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositions();
    }

    public void verifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositions();
    }

    public void verifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatTotalAssetsInDetailsIsCalculatedCorrectly();
    }

    public void verifyThatPairInPositionIsCorrectOnInvestorPage(String pair) {
        PrizmaInvestorBotDetailsPage.verifyThatPairInPositionIsCorrect(pair);
    }

    public void verifyThatTypeInPositionIsCorrectOnInvestorPage(String type) {
        PrizmaInvestorBotDetailsPage.verifyThatTypeInPositionIsCorrect(type);
    }

    public void verifyThatCurrentSizeInPositionIsCorrectOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatCurrentSizeInPositionIsCorrect();
    }

    public void verifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatCurrentSizeInUsdInPositionIsNotEmpty();
    }

    public void verifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatAverageProfitInPositionIsCalculatedCorrectly();
    }

    public void verifyThatAveragePerformanceInPositionIsCorrectOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatAveragePerformanceInPositionIsCorrect();
    }

    public void verifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatTotalProfitInPositionIsCalculatedCorrectly();
    }

    public void verifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatTotalPerformanceInPositionIsCalculatedCorrectly();
    }

    public void verifyThatAllOrdersValueIsCorrectOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatAllOrdersValueIsCorrect();
    }

    public void verifyThatFilledOrdersValueIsCorrectOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.verifyThatFilledAllOrdersValueIsCorrect();
    }

    public void clickOnPositionBlockOnBotDetailsOnInvestorPage() {
        PrizmaInvestorBotDetailsPage.clickOnPositionBlock();
    }

    public void verifyThatFilledStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatFilledStatusIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatOpenStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatOpenStatusIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatCanceledStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatCanceledStatusIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatRejectedStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatRejectedStatusIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatOrderSideIsCorrectInPositionInOrdersTableOnInvestorPage(int position, String side) {
        PrizmaInvestorBotDetailsPage.verifyThatOrderSideIsCorrectInPositionInOrdersTable(position, side);
    }

    public void verifyThatMarketOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatMarketOrderIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatLimitOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatLimitOrderIsDisplayedInPositionInOrdersTable(position);
    }

    public void verifyThatStopLimitOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(int position) {
        PrizmaInvestorBotDetailsPage.verifyThatStopLimitOrderIsDisplayedInPositionInOrdersTable(position);
    }

    public void typeSearchRequest(String searchRequest) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaStorePage.typeSearchRequest(searchRequest);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public void verifySearchResultIsDisplayed(String searchResult) {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaStorePage.verifySearchResultIsDisplayed(searchResult);
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public void verifySearchNoResultDisplayed() {
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
        PrizmaStorePage.verifySearchNoResultDisplayed();
        PrizmaDriver.waitFor(PrizmaConstants.LONG_WAIT_TIME);
    }

    public void clickOnFullName (){
        PrizmaAccountPage.clickOnFullName();
    }

    public void clickOnEmail (){
        PrizmaAccountPage.clickOnEmail();
    }

    public void clickOnPassword (){
        PrizmaAccountPage.clickOnPassword();
    }

    public void clickOnDisplayName (){
        PrizmaAccountPage.clickOnDisplayName();
    }

    public void clickOnUSDCAddress (){
        PrizmaAccountPage.clickOnUSDCAddress();
    }

    public void clickOnCloseModalButton (){
        PrizmaAccountPage.clickOnCloseModalButton();
    }

    public void clickOnCancelModalButton (){
        PrizmaAccountPage.clickOnCancelModalButton();
    }

    public void clickOnOkModalButton (){
        PrizmaAccountPage.clickOnOkModalButton();
    }

    public void setValueInUserEditModalTextInput (String value){
        PrizmaAccountPage.setValueInUserEditModalTextInput(value);
    }

    public void setValueInUserEditModalPasswordInput (String value){
        PrizmaAccountPage.setValueInUserEditModalPasswordInput(value);
    }

    public void setValueInUserEditModalCurrentPasswordInput (String value){
        PrizmaAccountPage.setValueInUserEditModalCurrentPasswordInput(value);
    }

    public void setValueInUserEditModalNewPasswordInput (String value){
        PrizmaAccountPage.setValueInUserEditModalNewPasswordInput(value);
    }

    public void setValueInUserEditModalConfirmPasswordInput (String value){
        PrizmaAccountPage.setValueInUserEditModalConfirmPasswordInput(value);
    }

    public void verifyElementsOnAccountPage (){
        PrizmaAccountPage.verifyElementsOnAccountPage();
    }

    public void verifyUserEditModalIsDisplayed (){
        PrizmaAccountPage.verifyUserEditModalIsDisplayed();
    }

    public void verifyUserEditModalIsNotDisplayed (){
        PrizmaAccountPage.verifyUserEditModalIsNotDisplayed();
    }


}
package prizma.maxiru.tests;

import org.junit.*;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreatorTotalSectionTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePage(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePage");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePageDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordSelectCreatorRoleAndCheckThatUserRedirectToHomePage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002CheckTotalBotsSection(String link, String totalEarnedCountLabel, String totalInvestorsCountLabel, String botsUploadCount) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckTotalBotsSection_1");
        actionwords.verifyTotalEarnedCountLabel(totalEarnedCountLabel);
        actionwords.verifyTotalInvestorsCountLabel(totalInvestorsCountLabel);
        actionwords.verifyBotsUploadCountLabel(botsUploadCount);
        actionwords.getScreenshot("testcase002CheckTotalBotsSection_1");
    }

    @Test
    public void testcase002CheckTotalBotsSectionDataSet1() {
        testcase002CheckTotalBotsSection(websitelink, "$ 0", "0", "1");
    }

    public void testcase003CheckNextPayoutSection(String link, String nextPayoutEarnedCount) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckNextPayoutSection_1");
        actionwords.verifyNextPayoutEarnedLabel(nextPayoutEarnedCount);
        actionwords.getScreenshot("testcase003CheckNextPayoutSection_2");
    }

    @Test
    public void testcase003CheckNextPayoutSectionDataSet1() {
        testcase003CheckNextPayoutSection(websitelink, "0");
    }

    public void testcase004LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase004LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase004LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase004LogoutFromAccount_3");
    }

    @Test
    public void testcase004LogoutFromAccountDataSet1() {
        testcase004LogoutFromAccount(websitelink);
    }
}
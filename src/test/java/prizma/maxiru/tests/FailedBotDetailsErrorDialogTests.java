package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class FailedBotDetailsErrorDialogTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage_1");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePageDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002CheckThatPossibleToUploadFailedBot(String link,
                                                                                                           String fileName,
                                                                                                           int position,
                                                                                                           String botName,
                                                                                                           String rate,
                                                                                                           String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_4");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyUntilBotStatusIsFailedInUploadedBotsSectionByBotName(botName);
        actionwords.getScreenshot("testcase002BotIsPublished");
    }

    @Test
    public void testcase002CheckThatPossibleToUploadFailedBotDataSet1() {
        testcase002CheckThatPossibleToUploadFailedBot(websitelink, "bots_talib_invalid.zip", 1, BOT_NAME_FOR_FAILED_BOT_TESTS_1, "5", "Uploading bot for auto tests");
    }

    public void testcase003CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog(String link, String botName) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_1");
        actionwords.getAndStoreBotPositionOnCreatorPage(botName);
        actionwords.getScreenshot("testcase002CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_2");
        actionwords.verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent();
        actionwords.clickOnBotDetailsViewErrorDetailsDialogBtn();
        actionwords.getScreenshot("testcase002CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_3");
    }

    @Test
    public void testcase003CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialogDataSet1() {
        testcase003CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog(websitelink, BOT_NAME_FOR_FAILED_BOT_TESTS_1);
    }

    public void testcase004DeleteAllBotsAfterTests(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase006SignInIntoPrizmaDashboard");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase006DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase004DeleteAllBotsAfterTestsDataSet1() {
        testcase004DeleteAllBotsAfterTests(websitelink); }

    public void testcase005CheckThatPossibleToUploadValidPublishedBot(String link,
                                                                      String fileName,
                                                                      int position,
                                                                      String botName,
                                                                      String rate,
                                                                      String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_2");
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_4");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(botName);
        actionwords.getScreenshot("testcase003CheckThatPossibleToUploadValidPublicBotOnUploadBotDialogWhichShouldPassBacktest_5");
    }

    @Test
    public void testcase005CheckThatPossibleToUploadValidPublishedBotDataSet1() {
        testcase005CheckThatPossibleToUploadValidPublishedBot(websitelink, "bot_sample.zip", 1, BOT_NAME_FOR_FAILED_BOT_TESTS_2, "5", "Uploading bot for auto tests");
    }

    public void testcase006CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresent(String link, String botName) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresent_1");
        actionwords.getAndStoreBotPositionOnCreatorPage(botName);
        actionwords.getScreenshot("testcase003CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresent_2");
        actionwords.verifyThatBotDetailsViewErrorDetailsDialogBtnIsNotPresent();
        actionwords.getScreenshot("testcase003CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresent_3");
    }

    @Test
    public void testcase006CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresentDataSet1() {
        testcase006CheckAbilityToOpenPublishedBotDetailsDialogAndCheckThatButtonToOpenErrorDialogNotPresent(websitelink, BOT_NAME_FOR_FAILED_BOT_TESTS_2);
    }

    public void testcase007DeleteAllBotsAfterTests(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase006SignInIntoPrizmaDashboard");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase006DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase007DeleteAllBotsAfterTestsDataSet1() {
        testcase007DeleteAllBotsAfterTests(websitelink); }

    public void testcase008LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_2");
    }

    @Test
    public void testcase008LogOutFromAccountDataSet1() {
        testcase008LogOutFromAccount(websitelink);
    }
}
package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class DownloadWebSDKTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001CheckAbilityToDownloadWebSDKFromCreatorPage(String link, String email, String password, String fileName, int timer) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase001CheckAbilityToDownloadWebSDKFromCreatorPage_1");
        actionwords.deleteTheDownloadFolder();
        actionwords.createTheDownloadFolder();
        actionwords.clickOnDownloadPrizmaBtnOnCreatorPage(fileName, timer);
        actionwords.checkThatFolderIsNotEmptyAfterDownloadSomeFilesFromPrizmaDashboard();
        actionwords.getScreenshot("testcase001CheckAbilityToDownloadWebSDKFromCreatorPage_2");
    }

    @Test
    public void testcase001CheckAbilityToDownloadWebSDKFromCreatorPageDataSet1() {
        testcase001CheckAbilityToDownloadWebSDKFromCreatorPage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS, "prizma-sdk.zip", 600);
    }

    public void testcase002LogoutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase002LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase002LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase002LogoutFromAccount_3");
    }

    @Test
    public void testcase002LogoutFromAccountDataSet1() {
        testcase002LogoutFromAccount(websitelink);
    }
}
package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StoreBotsPerPageTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test @Ignore
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002OpenStorePageClickOnBotsPerPageSelectorAndSelectOption1(String link, int validationCount) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount);
        actionwords.getScreenshot("testcase002OpenStorePageClickOnBotsPerPageSelectorAndSelectOption1_1");
        actionwords.clickOnBotsPerPageSelectorAndSelectOption1();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount);
        actionwords.getScreenshot("testcase002OpenStorePageClickOnBotsPerPageSelectorAndSelectOption1_2");
    }

    @Test @Ignore
    public void testcase002OpenStorePageClickOnBotsPerPageSelectorAndSelectOption1DataSet1() {
        testcase002OpenStorePageClickOnBotsPerPageSelectorAndSelectOption1(websitelink, BOTS_PER_PAGE_OPTION_1);
    }

    public void testcase003OpenStorePageClickOnBotsPerPageSelectorAndSelectOption2(String link, int validationCount1, int validationCount2) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount1);
        actionwords.getScreenshot("testcase003OpenStorePageClickOnBotsPerPageSelectorAndSelectOption2_1");
        actionwords.clickOnBotsPerPageSelectorAndSelectOption2();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount2);
        actionwords.getScreenshot("testcase003OpenStorePageClickOnBotsPerPageSelectorAndSelectOption2_2");
    }

    @Test @Ignore
    public void testcase003OpenStorePageClickOnBotsPerPageSelectorAndSelectOption2DataSet1() {
        testcase003OpenStorePageClickOnBotsPerPageSelectorAndSelectOption2(websitelink, BOTS_PER_PAGE_OPTION_1, BOTS_PER_PAGE_OPTION_2);
    }

    public void testcase004OpenStorePageClickOnBotsPerPageSelectorAndSelectOption3(String link, int validationCount1, int validationCount2) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount1);
        actionwords.getScreenshot("testcase004OpenStorePageClickOnBotsPerPageSelectorAndSelectOption3_1");
        actionwords.clickOnBotsPerPageSelectorAndSelectOption3();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount2);
        actionwords.getScreenshot("testcase004OpenStorePageClickOnBotsPerPageSelectorAndSelectOption3_2");
    }

    @Test @Ignore
    public void testcase004OpenStorePageClickOnBotsPerPageSelectorAndSelectOption3DataSet1() {
        testcase004OpenStorePageClickOnBotsPerPageSelectorAndSelectOption3(websitelink, BOTS_PER_PAGE_OPTION_1, BOTS_PER_PAGE_OPTION_3);
    }

    public void testcase005OpenStorePageClickOnBotsPerPageSelectorAndSelectOption4(String link, int validationCount1, int validationCount2) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilter(validationCount1);
        actionwords.getScreenshot("testcase005OpenStorePageClickOnBotsPerPageSelectorAndSelectOption4_1");
        actionwords.clickOnBotsPerPageSelectorAndSelectOption4();
        actionwords.verifyThatOnUIPresentCorrectCountOfBotsAfterApplyFilterAndIsNotMoreThenMax(validationCount2);
        actionwords.getScreenshot("testcase005OpenStorePageClickOnBotsPerPageSelectorAndSelectOption4_2");
    }

    @Test @Ignore
    public void testcase005OpenStorePageClickOnBotsPerPageSelectorAndSelectOption4DataSet1() {
        testcase005OpenStorePageClickOnBotsPerPageSelectorAndSelectOption4(websitelink, BOTS_PER_PAGE_OPTION_1, BOTS_PER_PAGE_OPTION_4);
    }

    public void testcase006LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase006LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase006LogOutFromAccount_2");
    }

    @Test @Ignore
    public void testcase006LogOutFromAccountDataSet1() {
        testcase006LogOutFromAccount(websitelink);
    }
}
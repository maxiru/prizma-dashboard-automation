package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaSignupPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreatorInvestorNoSplashScreenTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001PassSignUpWithValidTestDataAsInvestorAndCreator(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestorAndCreator_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.checkInvestorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestorAndCreator_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestorAndCreator_3");
    }

    @Test
    public void testcase001PassSignUpWithValidTestDataAsInvestorAndCreatorDataSet1() {
        testcase001PassSignUpWithValidTestDataAsInvestorAndCreator(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase002ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase002ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase002ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreenDataSet1() {
        testcase002ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreenDataSet1() {
        testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase004LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase004LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase004LogoutFromAccount_2");
    }

    @Test
    public void testcase004LogoutFromAccountDataSet1() {
        testcase004LogoutFromAccount(websitelink);
    }

    public void testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(String link, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(LP_EMAIL_FOR_FUTURE_TESTS);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_1");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_2");
        actionwords.refreshPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_3");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_4");
    }

    @Test
    public void testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPageDataSet1() {
        testcase005SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(websitelink, "Romank_1992");
    }

    public void testcase006LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase006LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase006LogoutFromAccount_2");
    }

    @Test
    public void testcase006LogoutFromAccountDataSet1() {
        testcase006LogoutFromAccount(websitelink);
    }
}
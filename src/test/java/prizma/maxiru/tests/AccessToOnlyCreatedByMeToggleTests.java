package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AccessToOnlyCreatedByMeToggleTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001PassSignUpWithValidTestDataAsCreator(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsCreator_3");
    }

    @Test
    public void testcase001PassSignUpWithValidTestDataAsCreatorDataSet1() {
        testcase001PassSignUpWithValidTestDataAsCreator(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenDataSet1() {
        testcase002ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreenDataSet1() {
        testcase003ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase004OpenStorePageAndCheckThatCreatedByMeToggleVisible(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsPresent();
        actionwords.getScreenshot("testcase004OpenStorePageAndCheckThatCreatedByMeToggleVisible");
    }

    @Test
    public void testcase004OpenStorePageAndCheckThatCreatedByMeToggleVisibleDataSet1() {
        testcase004OpenStorePageAndCheckThatCreatedByMeToggleVisible(websitelink);
    }

    public void testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_1");
        actionwords.clickOnUserAgreementToggleOnInvestorPageToEnable();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_2");
        actionwords.clickOnStartInvestingBtnOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_3");
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_4");
        actionwords.refreshPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole_5");
    }

    @Test
    public void testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRoleDataSet1() {
        testcase005ClickOnInvestorBtnAndCheckThatUserLandOnInvestorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartInvestingBtnToUpdateUserRole(websitelink);
    }

    public void testcase006OpenStorePageAndCheckThatCreatedByMeToggleVisible(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsPresent();
        actionwords.getScreenshot("testcase006OpenStorePageAndCheckThatCreatedByMeToggleVisible");
    }

    @Test
    public void testcase006OpenStorePageAndCheckThatCreatedByMeToggleVisibleDataSet1() {
        testcase006OpenStorePageAndCheckThatCreatedByMeToggleVisible(websitelink);
    }

    public void testcase007LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase007LogOutFromAccount_2");
    }

    @Test
    public void testcase007LogOutFromAccountDataSet1() {
        testcase007LogOutFromAccount(websitelink);
    }

    public void testcase008PassSignUpWithValidTestDataAsInvestor(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase008PassSignUpWithValidTestDataAsInvestor_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkInvestorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase008PassSignUpWithValidTestDataAsInvestor_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase008PassSignUpWithValidTestDataAsInvestor_3");
    }

    @Test
    public void testcase008PassSignUpWithValidTestDataAsInvestorDataSet1() {
        testcase008PassSignUpWithValidTestDataAsInvestor(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase009ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase009ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase009ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenDataSet1() {
        testcase009ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase010ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase010ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase010ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreenDataSet1() {
        testcase010ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase011OpenStorePageAndCheckThatCreatedByMeToggleHidden(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsNotPresent();
        actionwords.getScreenshot("testcase011OpenStorePageAndCheckThatCreatedByMeToggleHidden");
    }

    @Test
    public void testcase011OpenStorePageAndCheckThatCreatedByMeToggleHiddenDataSet1() {
        testcase011OpenStorePageAndCheckThatCreatedByMeToggleHidden(websitelink);
    }

    public void testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_1");
        actionwords.clickOnUserAgreementToggleOnCreatorPageToEnable();
        actionwords.getScreenshot("testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_2");
        actionwords.clickOnStartCreatingBtnOnCreatorPage();
        actionwords.getScreenshot("testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_3");
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_4");
        actionwords.refreshPage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_5");
    }

    @Test
    public void testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRoleDataSet1() {
        testcase012ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole(websitelink);
    }

    public void testcase013OpenStorePageAndCheckThatCreatedByMeToggleVisible(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsPresent();
        actionwords.getScreenshot("testcase013OpenStorePageAndCheckThatCreatedByMeToggleVisible");
    }

    @Test
    public void testcase013OpenStorePageAndCheckThatCreatedByMeToggleVisibleDataSet1() {
        testcase013OpenStorePageAndCheckThatCreatedByMeToggleVisible(websitelink);
    }

    public void testcase014LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase014LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase014LogOutFromAccount_2");
    }

    @Test
    public void testcase014LogOutFromAccountDataSet1() {
        testcase014LogOutFromAccount(websitelink);
    }

    public void testcase015PassSignUpWithValidTestDataAsInvestorAndCreator(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase015PassSignUpWithValidTestDataAsInvestorAndCreator_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkCreatorCheckboxOnSignUpPage();
        actionwords.checkInvestorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase015PassSignUpWithValidTestDataAsInvestorAndCreator_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase015PassSignUpWithValidTestDataAsInvestorAndCreator_3");
    }

    @Test
    public void testcase015PassSignUpWithValidTestDataAsInvestorAndCreatorDataSet1() {
        testcase015PassSignUpWithValidTestDataAsInvestorAndCreator(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase016ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase016ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase016ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreenDataSet1() {
        testcase016ClickOnCreatorBtnAndCheckThatUserNotLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase017ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase017ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase017ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreenDataSet1() {
        testcase017ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase018OpenStorePageAndCheckThatCreatedByMeToggleVisible(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsPresent();
        actionwords.getScreenshot("testcase018OpenStorePageAndCheckThatCreatedByMeToggleVisible");
    }

    @Test
    public void testcase018OpenStorePageAndCheckThatCreatedByMeToggleVisibleDataSet1() {
        testcase018OpenStorePageAndCheckThatCreatedByMeToggleVisible(websitelink);
    }

    public void testcase019LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase019LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase019LogOutFromAccount_2");
    }

    @Test
    public void testcase019LogOutFromAccountDataSet1() {
        testcase019LogOutFromAccount(websitelink);
    }
}
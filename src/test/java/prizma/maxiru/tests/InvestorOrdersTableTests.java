package prizma.maxiru.tests;


import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaAPIOrders;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InvestorOrdersTableTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_INVESTOR_STATISTICS_TESTS, PASSWORD_FOR_INVESTOR_STATISTICS_TESTS);
    }

    public void testcase002CheckThatCorrectBotIsDisplayed(String botName) {
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(botName);
        actionwords.getScreenshot("testcase0021CheckThatCorrectBotIsDisplayed");
    }

    @Test
    public void testcase002CheckThatCorrectBotIsDisplayedSet1() {
        testcase002CheckThatCorrectBotIsDisplayed(BOT_NAME_FOR_ORDERS_TESTS);
    }

    public void testcase003OpenBotDetails(String botName) {
        actionwords.clickOnBotOnInvestorPageToOpenBotDialogByName(botName);
        actionwords.getScreenshot("testcase003OpenBotDetails");
    }

    @Test
    public void testcase003OpenBotDetailsSet1() {
        testcase003OpenBotDetails(BOT_NAME_FOR_ORDERS_TESTS);
    }

    public void testcase004SendMarketBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage() {
        PrizmaAPIOrders.sendBuyAndSellMarketOrdersAndVerifyThatDataIsCorrect();
        actionwords.getScreenshot("testcase004SendMarketBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase004SendMarketBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPageSet1() {
        testcase004SendMarketBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage();
    }

    public void testcase005VerifyThatMarketBuySellRequestsAreCorrectInOrdersTableOnInvestorPage(String firstOrderSide, String secondOrderSide) {
        actionwords.verifyThatFilledStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(1);
        actionwords.verifyThatFilledStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(2);
        actionwords.verifyThatOrderSideIsCorrectInPositionInOrdersTableOnInvestorPage(1, firstOrderSide);
        actionwords.verifyThatOrderSideIsCorrectInPositionInOrdersTableOnInvestorPage(2, secondOrderSide);
        actionwords.verifyThatMarketOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(1);
        actionwords.verifyThatMarketOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(2);
        actionwords.getScreenshot("testcase005VerifyThatMarketBuySellRequestsAreCorrectInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase005VerifyThatMarketBuySellRequestsAreCorrectInOrdersTableOnInvestorPageSet1() {
        testcase005VerifyThatMarketBuySellRequestsAreCorrectInOrdersTableOnInvestorPage("Sell","Buy");
    }

    public void testcase006SendLimitBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage() {
        PrizmaAPIOrders.sendBuyAndSellLimitOrdersAndVerifyThatDataIsCorrect();
        actionwords.getScreenshot("testcase006SendLimitBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase006SendLimitBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPageSet1() {
        testcase006SendLimitBuyAndSellRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage();
    }

    public void testcase007VerifyThatLimitBuyRequestIsCorrectInOrdersTableOnInvestorPage(String orderSide) {
        actionwords.verifyThatFilledStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(2);
        actionwords.verifyThatOrderSideIsCorrectInPositionInOrdersTableOnInvestorPage(2, orderSide);
        actionwords.verifyThatLimitOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(2);
        actionwords.getScreenshot("testcase007VerifyThatLimitBuyRequestIsCorrectInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase007VerifyThatLimitBuyRequestIsCorrectInOrdersTableOnInvestorPageSet1() {
        testcase007VerifyThatLimitBuyRequestIsCorrectInOrdersTableOnInvestorPage("Buy");
    }

    public void testcase008SendStopLimitBuyAndCancelRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage() {
        PrizmaAPIOrders.sendBuyAndCancelStopLimitOrdersAndVerifyThatDataIsCorrect();
        actionwords.getScreenshot("testcase008SendStopLimitBuyAndCancelRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage");
    }

    @Test
//    @Ignore
    public void testcase008SendStopLimitBuyAndCancelRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPageSet1() {
        testcase008SendStopLimitBuyAndCancelRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage();
    }

    public void testcase009SendRejectedOrderRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage() {
        PrizmaAPIOrders.sendRejectedOrderAndVerifyThatDataIsCorrect();
        actionwords.getScreenshot("testcase009SendRejectedOrderRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase009SendRejectedOrderRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPageSet1() {
        testcase009SendRejectedOrderRequestAndVerifyThatDataIsCorrectInOrdersTableOnInvestorPage();
    }

    public void testcase010VerifyThatRejectedDataIsDisplayedInPositionInOrdersTableOnInvestorPage(String orderSide) {
        actionwords.verifyThatRejectedStatusIsDisplayedInPositionInOrdersTableOnInvestorPage(1);
        actionwords.verifyThatOrderSideIsCorrectInPositionInOrdersTableOnInvestorPage(1, orderSide);
        actionwords.verifyThatMarketOrderIsDisplayedInPositionInOrdersTableOnInvestorPage(1);
        actionwords.getScreenshot("testcase010VerifyThatRejectedDataIsDisplayedInPositionInOrdersTableOnInvestorPage");
    }

    @Test
    public void testcase010VerifyThatRejectedDataIsDisplayedInPositionInOrdersTableOnInvestorPageSet1() {
        testcase010VerifyThatRejectedDataIsDisplayedInPositionInOrdersTableOnInvestorPage("Buy");
    }

    public void testcase011LogOutFromAccount() {
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase011LogOutFromAccount");
    }

    @Test
    public void testcase011LogOutFromAccountDataSet1() {
        testcase011LogOutFromAccount();
    }
}
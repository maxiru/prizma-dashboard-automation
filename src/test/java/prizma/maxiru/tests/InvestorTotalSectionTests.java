package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InvestorTotalSectionTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_PRIZMA_USER,
                PASSWORD_FOR_PRIZMA_USER);
    }

    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed(String botName) {
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(botName);
        actionwords.getScreenshot("testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed");
    }

    @Test
    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayedSet1() {
        testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed("Autotest_live");
    }

    public void testcase003VerifyThatTotalReturnOnInvestorPageIsCorrect() {
        actionwords.verifyThatTotalReturnOnInvestorPageIsCorrect();
        actionwords.getScreenshot("testcase003VerifyThatTotalReturnOnInvestorPageIsCorrect");
    }

    @Test
    public void testcase003VerifyThatTotalReturnOnInvestorPageIsCorrectSet1() {
        testcase003VerifyThatTotalReturnOnInvestorPageIsCorrect();
    }

    public void testcase004VerifyThatTotalInvestedAmountOnInvestorPageIsCorrect() {
        actionwords.verifyThatTotalInvestedAmountOnInvestorPageIsCorrect();
        actionwords.getScreenshot("testcase004VerifyThatTotalInvestedAmountOnInvestorPageIsCorrect");
    }

    @Test
    public void testcase004VerifyThatTotalInvestedAmountOnInvestorPageIsCorrectSet1() {
        testcase004VerifyThatTotalInvestedAmountOnInvestorPageIsCorrect();
    }

    public void testcase005VerifyThatTotalAssetsOnInvestorPageIsCorrect() {
        actionwords.verifyThatTotalAssetsOnInvestorPageIsCorrect();
        actionwords.getScreenshot("testcase005VerifyThatTotalAssetsOnInvestorPageIsCorrect");
    }

    @Test
    public void testcase005VerifyThatTotalAssetsOnInvestorPageIsCorrectSet1() {
        testcase005VerifyThatTotalAssetsOnInvestorPageIsCorrect();
    }

    public void testcase006VerifyThatStoreIsOpenedByClickOnViewStoreBtn() {
        actionwords.clickOnViewStoreBtnOnInvestorPage();
        actionwords.verifyThatUserRedirectFromInvestorPageToStorePageAfterClickOnViewStoreBtn();
        actionwords.getScreenshot("testcase004VerifyThatStoreIsOpenedByClickOnViewStoreBtn");
    }

    @Test
    public void testcase006VerifyThatStoreIsOpenedByClickOnViewStoreBtnOnInvestorPage() {
        testcase006VerifyThatStoreIsOpenedByClickOnViewStoreBtn();
    }

    public void testcase007LogOutFromAccount() {
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase008LogOutFromAccount");
    }

    @Test
    public void testcase007LogOutFromAccountDataSet1() {
        testcase007LogOutFromAccount();
    }
}
package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static app.PrizmaStorePage.STORE_BOT_NAME_FOR_FUTURE_TESTS_1;
import static app.PrizmaStorePage.STORE_BOT_NAME_FOR_FUTURE_TESTS_2;
import static prizma.maxiru.framework.common.PrizmaConstants.EMAIL_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaConstants.PASSWORD_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StoreSearchBotByName {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002UploadBot(String link, String fileName, String name, String rate, String description, int position, String botStatus) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);

        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.setBotNameOnUploadANewBotDialog(name);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.getScreenshot("testcase002UploadBotFormIsFilled");
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(name, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase002BotIsUploaded");
    }

    @Test
    public void testcase002UploadBot() {
        testcase002UploadBot(websitelink, "bot_sample.zip", "auto-bot-search-123", "5", "Uploading bot for auto tests", 1, "Preparing validation");
    }

    public void testcase003WaitUntilBotIsPublished(String link, String name) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);

        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(name);
        actionwords.getScreenshot("testcase003BotIsPublished");
    }

    @Test
    public void testcase003WaitUntilBotIsPublished() {
        testcase003WaitUntilBotIsPublished(websitelink, "auto-bot-search-123");
    }

    public void testcase004SearchBot(String link, String searchRequest, String result) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.typeSearchRequest(searchRequest);
        actionwords.verifySearchResultIsDisplayed(result);
        actionwords.getScreenshot("testcase004SearchResult");
    }

    @Test
    public void testcase004Type3Symbols() {
        testcase004SearchBot(websitelink, "aut", "aut");
    }

    @Test
    public void testcase005Type3SymbolsNumerical() {
        testcase004SearchBot(websitelink, "123", "123");
    }

    @Test
    public void testcase006TypeWholeName() {
        testcase004SearchBot(websitelink, "auto-bot-search-123", "auto-bot-search-123");
    }

    @Test
    public void testcase006TypePartName() {
        testcase004SearchBot(websitelink, "search-123", "search-123");
    }

    public void testcase007NoSearchResult(String link, String searchRequest) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.typeSearchRequest(searchRequest);
        actionwords.verifySearchNoResultDisplayed();
        actionwords.getScreenshot("testcase007NoSearchResult");
    }

    @Test
    public void testcase007NoSearchResult() {
        testcase007NoSearchResult(websitelink, "qwertyasdfg");
    }


    public void testcase008DeleteAllBotsAfterTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase008SignInIntoPrizmaDashboard");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase008DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase008DeleteAllBotsAfterTestsDataSet1() {
        testcase008DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase009LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase009LogoutFromAccount_3");
    }

    @Test
    public void testcase009LogoutFromAccountDataSet1() {
        testcase009LogoutFromAccount(websitelink);
    }
}
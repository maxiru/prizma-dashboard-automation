package prizma.maxiru.tests;

import org.junit.*;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InvestorBotDetailsTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    String botNameForStatistics = "e1_bsample_01";
    int botPositionInList = 1;

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_PRIZMA_USER, PASSWORD_FOR_PRIZMA_USER);
    }

    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed(String botName) {
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(botName);
        actionwords.getScreenshot("testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed");
    }

    @Test
    public void testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayedSet1() {
        testcase002OpenInvestorPageAndCheckThatCorrectBotIsDisplayed(botNameForStatistics);
    }

    public void testcase003VerifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPage(String botName, int position) {
        actionwords.clickOnBotOnInvestorPageToOpenBotDialogByName(botName);
        actionwords.verifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPage(position);
        actionwords.getScreenshot("testcase003VerifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPage");
    }

    @Test
    public void testcase003VerifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPageSet1() {
        testcase003VerifyThatBotAvatarInBotDetailsEqualsAvatarInBotsListOnInvestorPage(botNameForStatistics, botPositionInList);
    }

    public void testcase004VerifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPage(int position) {
        actionwords.verifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPage(position);
        actionwords.getScreenshot("testcase004VerifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPage");
    }

    @Test
    public void testcase004VerifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPageSet1() {
        testcase004VerifyThatBotExchangeInBotDetailsEqualsExchangeInBotsListOnInvestorPage(botPositionInList);
    }

    public void testcase005VerifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPage(int position) {
        actionwords.verifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPage(position);
        actionwords.getScreenshot("testcase005VerifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPage");
    }

    @Test
    public void testcase005VerifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPageSet1() {
        testcase005VerifyThatBotActivationDateInBotDetailsEqualsDateInBotsListOnInvestorPage(botPositionInList);
    }

    public void testcase006VerifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPage(int position) {
        actionwords.verifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPage(position);
        actionwords.getScreenshot("testcase006VerifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPage");
    }

    @Test
    public void testcase006VerifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPageSet1() {
        testcase006VerifyThatBotRateInBotDetailsEqualsRateInBotsListOnInvestorPage(botPositionInList);
    }

    public void testcase007VerifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPage(int position) {
        actionwords.verifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPage(position);
        actionwords.getScreenshot("testcase007VerifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPage");
    }

    @Test
    public void testcase007VerifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPageSet1() {
        testcase007VerifyThatStateInBotDetailsEqualsStateInBotsListOnInvestorPage(botPositionInList);
    }

    public void testcase008VerifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPage() {
        actionwords.verifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPage();
        actionwords.getScreenshot("testcase008VerifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPage");
    }

    @Test
    @Ignore
    public void testcase008VerifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPageSet1() {
        testcase008VerifyThatTotalProfitInDetailsEqualsProfitInPositionsOnInvestorPage();
    }

    public void testcase009VerifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPage() {
        actionwords.verifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPage();
        actionwords.getScreenshot("testcase009VerifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPage");
    }

    @Test
    public void testcase009VerifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPageSet1() {
        testcase009VerifyThatPerformanceInDetailsEqualsPerformanceInPositionsOnInvestorPage();
    }

    public void testcase010VerifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPage() {
        actionwords.verifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPage();
        actionwords.getScreenshot("testcase010VerifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPage");
    }

    @Test
    @Ignore
    public void testcase010VerifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPageSet1() {
        testcase010VerifyThatBoughtCryptoInDetailsEqualsBoughtSizeInPositionsOnInvestorPage();
    }

    public void testcase011VerifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPage() {
        actionwords.verifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPage();
        actionwords.getScreenshot("testcase011VerifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPage");
    }

    @Test
    public void testcase011VerifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPageSet1() {
        testcase011VerifyThatBoughtCryptoUsdInDetailsEqualsBoughtSizeUsdInPositionsOnInvestorPage();
    }

    public void testcase012VerifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPage() {
        actionwords.verifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPage();
        actionwords.getScreenshot("testcase012VerifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPage");
    }

    @Test
    public void testcase012VerifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPageSet1() {
        testcase012VerifyThatTotalAssetsInDetailsIsCalculatedCorrectlyOnInvestorPage();
    }

    public void testcase013VerifyThatPairInPositionIsCorrectOnInvestorPage(String pair) {
        actionwords.verifyThatPairInPositionIsCorrectOnInvestorPage(pair);
        actionwords.getScreenshot("testcase013VerifyThatPairInPositionIsCorrectOnInvestorPage");
    }

    @Test
    public void testcase013VerifyThatPairInPositionIsCorrectOnInvestorPageSet1() {
        testcase013VerifyThatPairInPositionIsCorrectOnInvestorPage("BTC USDT");
    }

    public void testcase014VerifyThatTypeInPositionIsCorrectOnInvestorPage(String type) {
        actionwords.verifyThatTypeInPositionIsCorrectOnInvestorPage(type);
        actionwords.getScreenshot("testcase014VerifyThatTypeInPositionIsCorrectOnInvestorPage");
    }

    @Test
    public void testcase014VerifyThatTypeInPositionIsCorrectOnInvestorPageSet1() {
        testcase014VerifyThatTypeInPositionIsCorrectOnInvestorPage("LONG");
    }

    public void testcase015VerifyThatCurrentSizeInPositionIsCorrectOnInvestorPage() {
        actionwords.clickOnPositionBlockOnBotDetailsOnInvestorPage();
        actionwords.verifyThatCurrentSizeInPositionIsCorrectOnInvestorPage();
        actionwords.getScreenshot("testcase015VerifyThatCurrentSizeInPositionIsCorrectOnInvestorPage");
    }

    @Test
    public void testcase015VerifyThatCurrentSizeInPositionIsCorrectOnInvestorPageSet1() {
        testcase015VerifyThatCurrentSizeInPositionIsCorrectOnInvestorPage();
    }

    public void testcase016VerifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPage() {
        actionwords.verifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPage();
        actionwords.getScreenshot("testcase016VerifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPage");
    }

    @Test
    public void testcase016VerifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPageSet1() {
        testcase016VerifyThatCurrentSizeInUsdInPositionIsNotEmptyOnInvestorPage();
    }

    public void testcase017VerifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPage() {
        actionwords.verifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPage();
        actionwords.getScreenshot("testcase017VerifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPage");
    }

    @Test
    public void testcase017VerifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPageSet1() {
        testcase017VerifyThatAverageProfitInPositionIsCalculatedCorrectlyOnInvestorPage();
    }

    public void testcase018VerifyThatAveragePerformanceInPositionIsCorrectOnInvestorPage() {
        actionwords.verifyThatAveragePerformanceInPositionIsCorrectOnInvestorPage();
        actionwords.getScreenshot("testcase018VerifyThatAveragePerformanceInPositionIsCorrectOnInvestorPage");
    }

    @Test
    @Ignore
    public void testcase018VerifyThatAveragePerformanceInPositionIsCorrectOnInvestorPageSet1() {
        testcase018VerifyThatAveragePerformanceInPositionIsCorrectOnInvestorPage();
    }

    public void testcase019VerifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPage() {
        actionwords.verifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPage();
        actionwords.getScreenshot("testcase019VerifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPage");
    }

    @Test
    public void testcase019VerifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPageSet1() {
        testcase019VerifyThatTotalProfitInPositionIsCalculatedCorrectlyOnInvestorPage();
    }

    public void testcase020VerifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPage() {
        actionwords.verifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPage();
        actionwords.getScreenshot("testcase020VerifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPage");
    }

    @Test
    public void testcase020VerifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPageSet1() {
        testcase020VerifyThatTotalPerformanceInPositionIsCalculatedCorrectlyOnInvestorPage();
    }

    public void testcase021VerifyThatAllOrdersValueIsCorrectOnInvestorPage() {
        actionwords.verifyThatAllOrdersValueIsCorrectOnInvestorPage();
        actionwords.getScreenshot("testcase021VerifyThatAllOrdersValueIsCorrectOnInvestorPagee");
    }

    @Test
    public void testcase021VerifyThatAllOrdersValueIsCorrectOnInvestorPageSet1() {
        testcase021VerifyThatAllOrdersValueIsCorrectOnInvestorPage();
    }

    public void testcase022VerifyThatFilledOrdersValueIsCorrectOnInvestorPage() {
        actionwords.verifyThatFilledOrdersValueIsCorrectOnInvestorPage();
        actionwords.getScreenshot("testcase022VerifyThatFilledOrdersValueIsCorrectOnInvestorPage");
    }

    @Test
    public void testcase022VerifyThatFilledOrdersValueIsCorrectOnInvestorPageSet1() {
        testcase022VerifyThatFilledOrdersValueIsCorrectOnInvestorPage();
    }

    public void testcase023LogOutFromAccount() {
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase021LogOutFromAccount");
    }

    @Test
    public void testcase023LogOutFromAccountDataSet1() {
        testcase023LogOutFromAccount();
    }
}
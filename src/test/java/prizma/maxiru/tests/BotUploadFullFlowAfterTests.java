package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class BotUploadFullFlowAfterTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePageDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_BACKTEST_TESTS_USER_A, PASSWORD_FOR_BACKTEST_TESTS_USER_A);
    }

    public void testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep(String link, String botName1, String botName2, String botName3) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatBotPresentInListOfBotsOnStorePage(botName1, "testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep_1");
        actionwords.verifyThatBotPresentInListOfBotsOnStorePage(botName2, "testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep_2");
        actionwords.verifyThatBotNotPresentInListOfBotsOnStorePage(botName3, "testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep_3");
        actionwords.getScreenshot("testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep_4");
    }

    @Test
    public void testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStepDataSet1() {
        testcase002OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListForUserACreatedByUserAAWhenPassValidationStep(websitelink, "maxiru_auto_bot", "maxiru_valid_pri", "maxiru-qa-invalid");
    }

    public void testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_1");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_2");
        actionwords.clickOnCreatedByMeCheckboxToDisable();
        actionwords.verifyThatCreatedByMeCheckboxIsDisabled();
        actionwords.getScreenshot("testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_3");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_4");
    }

    @Test
    public void testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFineFineDataSet1() {
        testcase003OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(websitelink);
    }

    public void testcase004OpenCreatorPageAndCheckHowEachBotPassBacktest(String link, String botName1, String botName2, String botName3, String botStatus1, String botStatus2, String botStatus3, String investorsCount1, String investorsCount2) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getAndStoreBotPositionOnCreatorPage(botName1);
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(botName1);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(botStatus1);
        actionwords.verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(investorsCount1);
        actionwords.getScreenshot("testcase004OpenCreatorPageAndCheckHowEachBotPassBacktest_1");
        actionwords.getAndStoreBotPositionOnCreatorPage(botName2);
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(botName2);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(botStatus1);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(botStatus3);
        actionwords.verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(investorsCount2);
        actionwords.getScreenshot("testcase004OpenCreatorPageAndCheckHowEachBotPassBacktest_2");
        actionwords.getAndStoreBotPositionOnCreatorPage(botName3);
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPositionFromVariable(botName3);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPositionFromVariable(botStatus2);
        actionwords.verifyInvestorsCountLabelInUploadedBotsSectionOnPositionFromVariable(investorsCount2);
        actionwords.getScreenshot("testcase004OpenCreatorPageAndCheckHowEachBotPassBacktest_3");
    }

    @Test
    public void testcase004OpenCreatorPageAndCheckHowEachBotPassBacktestDataSet1() {
        testcase004OpenCreatorPageAndCheckHowEachBotPassBacktest(websitelink, "maxiru_auto_bot", "maxiru_valid_pri", "maxiru-qa-invalid", "Published", "Validation failed", "Private", "1", "0");
    }

    public void testcase005LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase005LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase005LogOutFromAccount_2");
    }

    @Test
    public void testcase005LogOutFromAccountDataSet1() {
        testcase005LogOutFromAccount(websitelink);
    }

    public void testcase006SignInWithCorrectUsernamePasswordAsUserBAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase006SignInWithCorrectUsernamePasswordAsUserBAndCheckThatRedirectToHomePage");
    }

    @Test
    public void testcase006SignInWithCorrectUsernamePasswordAsUserBAndCheckThatRedirectToHomePageDataSet1() {
        testcase006SignInWithCorrectUsernamePasswordAsUserBAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_BACKTEST_TESTS_USER_B, PASSWORD_FOR_BACKTEST_TESTS_USER_B);
    }

    public void testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest(String link, String botName1, String botName2, String botName3) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatBotPresentInListOfBotsOnStorePage(botName1, "testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest_1");
        actionwords.verifyThatBotNotPresentInListOfBotsOnStorePage(botName2, "testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest_2");
        actionwords.verifyThatBotNotPresentInListOfBotsOnStorePage(botName3, "testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest_3");
        actionwords.getScreenshot("testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest_4");
    }

    @Test
    public void testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktestDataSet1() {
        testcase007OpenStorePageAndCheckThatOnlyUploadPublicBotIsPresentInBotsListForUserBCreatedByUserAWhenPassBacktest(websitelink, "maxiru_auto_bot", "maxiru_valid_pri", "maxiru-qa-invalid");
    }

    public void testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_1");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_2");
        actionwords.clickOnCreatedByMeCheckboxToDisable();
        actionwords.verifyThatCreatedByMeCheckboxIsDisabled();
        actionwords.getScreenshot("testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_3");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_4");
    }

    @Test
    public void testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFineDataSet1() {
        testcase008OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(websitelink);
    }

    public void testcase009LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase009LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase009LogOutFromAccount_2");
    }

    @Test
    public void testcase009LogOutFromAccountDataSet1() {
        testcase009LogOutFromAccount(websitelink);
    }

    public void testcase010SignInAsPrizmaVirtualUserAndCheckThatUserRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase010SignInAsPrizmaVirtualUserAndCheckThatUserRedirectToHomePage_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase010SignInAsPrizmaVirtualUserAndCheckThatUserRedirectToHomePage_2");
    }

    @Test
    public void testcase010SignInAsPrizmaVirtualUserAndCheckThatUserRedirectToHomePageDataSet1() {
        testcase010SignInAsPrizmaVirtualUserAndCheckThatUserRedirectToHomePage(websitelink, EMAIL_FOR_PRIZMA_USER, PASSWORD_FOR_PRIZMA_USER);
    }

    public void testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots(String link, String botName1, String botName2, String botName3) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatBotPresentInListOfBotsOnInvestorPage(botName1, "testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots_1");
        actionwords.verifyThatBotNotPresentInListOfBotsOnInvestorPage(botName2, "testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots_2");
        actionwords.verifyThatBotNotPresentInListOfBotsOnInvestorPage(botName3, "testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots_3");
        actionwords.getScreenshot("testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots_4");
    }

    @Test
    public void testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBotsDataSet1() {
        testcase011OpenInvestorPageAndCheckThatBotIsPresentInTheListOfBots(websitelink, "maxiru_auto_bot", "maxiru_valid_pri", "maxiru-qa-invalid");
    }

    public void testcase012LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase012LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase012LogOutFromAccount_2");
    }

    @Test
    public void testcase012LogOutFromAccountDataSet1() {
        testcase012LogOutFromAccount(websitelink);
    }

    public void testcase013SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase013SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage");
    }

    @Test
    public void testcase013SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePageDataSet1() {
        testcase013SignInWithCorrectUsernamePasswordAsUserAAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_BACKTEST_TESTS_USER_A, PASSWORD_FOR_BACKTEST_TESTS_USER_A);
    }

    public void testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog(String link, String botName) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_1");
        actionwords.getAndStoreBotPositionOnCreatorPage(botName);
        actionwords.clickOnBotNameInUnloadBotsSectionByPositionFromVariable();
        actionwords.getScreenshot("testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_2");
        actionwords.verifyThatBotDetailsViewErrorDetailsDialogBtnIsPresent();
        actionwords.clickOnBotDetailsViewErrorDetailsDialogBtn();
        actionwords.getScreenshot("testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog_3");
    }

    @Test
    public void testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialogDataSet1() {
        testcase014CheckAbilityToOpenFailedBotDetailsDialogAndOpenErrorDialog(websitelink, "maxiru-qa-invalid");
    }

    public void testcase015DeleteAllBotsAfterTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase015DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase015DeleteAllBotsAfterTestsDataSet1() {
        testcase015DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase016LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase016LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase016LogOutFromAccount_2");
    }

    @Test
    public void testcase016LogOutFromAccountDataSet1() {
        testcase016LogOutFromAccount(websitelink);
    }
}
package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaStorePage.*;
import static prizma.maxiru.framework.common.PrizmaConstants.EMAIL_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaConstants.PASSWORD_FOR_SIGN_IN_TESTS;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StoreTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialog(String link, String fileName, int position, String botStatus, String botName, String rate, String description) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialog_1");
        actionwords.clickOnUploadAnotherBotBtnOnCreatorPage();
        actionwords.setBotNameOnUploadANewBotDialog(botName);
        actionwords.setBotRateOnUploadANewBotDialog(rate);
        actionwords.setBotDescriptionOnUploadANewBotDialog(description);
        actionwords.uploadNewBotZipFile(fileName);
        actionwords.clickOnBotUploadConfirmBtnOnUploadBotDialog();
        actionwords.verifyThatUploadBotDialogCloseWhenUserTriedToUploadNewBotWithCorrectDataBot();
        actionwords.getScreenshot("testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialog_4");
        actionwords.verifyBotNameLabelInUploadedBotsSectionOnPosition(botName, position);
        actionwords.verifyBotStatusLabelInUploadedBotsSectionOnPosition(botStatus, position);
        actionwords.getScreenshot("testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialog_5");
    }

    @Test
    public void testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialogDataSet1() {
        testcase002CheckThatPossibleToUploadPublicBotOnUploadBotDialog(websitelink, "bot_sample.zip", 1, "Preparing validation", "store-test-bot", "10", "Uploading bot for auto tests");
    }

    public void testcase003OpenStorePageAndCheckThatNewUploadBotIsNotPresentInBotsListForUserACreatedByUserAWhenIsOnValidationStep(String link, String botName) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenStorePageAndCheckThatNewUploadBotIsNotPresentInBotsListForUserACreatedByUserAWhenIsOnValidationStep_1");
        actionwords.verifyThatBotNotPresentInListOfBotsOnStorePage(botName, "testcase003OpenStorePageAndCheckThatNewUploadBotIsNotPresentInBotsListForUserACreatedByUserAWhenIsOnValidationStep_2");
    }

    @Test
    public void testcase003OpenStorePageAndCheckThatNewUploadBotIsNotPresentInBotsListForUserACreatedByUserAWhenIsOnValidationStepnDataSet1() {
        testcase003OpenStorePageAndCheckThatNewUploadBotIsNotPresentInBotsListForUserACreatedByUserAWhenIsOnValidationStep(websitelink, "store-test-bot");
    }

    public void testcase004WaitUntilBotIsPublished(String link, String name) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyUntilBotStatusIsPublishedInUploadedBotsSectionByBotName(name);
        actionwords.getScreenshot("testcase004BotIsPublished");
    }

    @Test
    public void testcase004WaitUntilBotIsPublished() {
        testcase004WaitUntilBotIsPublished(websitelink, "store-test-bot");
    }

    public void testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle_1");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle_2");
        actionwords.clickOnCreatedByMeCheckboxToDisable();
        actionwords.verifyThatCreatedByMeCheckboxIsDisabled();
        actionwords.getScreenshot("testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle_3");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle_4");
    }

    @Test
    public void testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggleDataSet1() {
        testcase005OpenStorePageAndCheckThatNewUploadBotIsPresentInBotsListAndCheckAbilityEnableDisableCreatedByMeToggle(websitelink);
    }

    public void testcase006DeleteAllBotsAfterTests(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.deleteAllBotsFromThePage();
        actionwords.getScreenshot("testcase006DeleteAllBotsAfterTests");
    }

    @Test
    public void testcase006DeleteAllBotsAfterTestsDataSet1() {
        testcase006DeleteAllBotsAfterTests(websitelink);
    }

    public void testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI(String link, String botName1, String botName2) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI_1");
        actionwords.verifyThatBotPresentInListOfBotsOnStorePage(botName2, "testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI_2");
        actionwords.verifyThatBotNotPresentInListOfBotsOnStorePage(botName1, "testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI_3");
        actionwords.getScreenshot("testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI_4");
    }

    @Test
    public void testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUIDataSet1() {
        testcase007OpenStorePageAndCheckThatBotWithLongNameIsCorrectlyCutAndPresentOnUI(websitelink, STORE_BOT_NAME_FOR_FUTURE_TESTS_1, STORE_BOT_NAME_FOR_FUTURE_TESTS_2);
    }

    public void testcase008LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase008LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase008LogOutFromAccount_2");
    }

    @Test
    public void testcase008LogOutFromAccountDataSet1() {
        testcase008LogOutFromAccount(websitelink);
    }
}
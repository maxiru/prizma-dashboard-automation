package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CheckCreatedByMeToggleFilterTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage");
    }

    @Test
    public void testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePageDataSet1() {
        testcase001SignInWithCorrectUsernamePasswordAndCheckThatRedirectToHomePage(websitelink, EMAIL_FOR_SIGN_IN_TESTS_2, PASSWORD_FOR_SIGN_IN_TESTS_2);
    }

    public void testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_1");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_2");
        actionwords.clickOnCreatedByMeCheckboxToDisable();
        actionwords.verifyThatCreatedByMeCheckboxIsDisabled();
        actionwords.getScreenshot("testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_3");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine_4");
    }

    @Test
    public void testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFineDataSet1() {
        testcase002OpenStorePageAndCheckThatCreatedByMeToggleWorksFine(websitelink);
    }

    public void testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState_1");
        actionwords.clickOnCreatedByMeCheckboxToEnable();
        actionwords.verifyThatCreatedByMeCheckboxIsEnabled();
        actionwords.getScreenshot("testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState_2");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState_3");
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.verifyThatCreatedByMeCheckboxIsDisabled();
        actionwords.getScreenshot("testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState_4");
    }

    @Test
    public void testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreStateDataSet1() {
        testcase003OpenStoreEnableCreatedByMeToggleAndSwitchBetweenPageToCheckThatWeStoreState(websitelink);
    }

    public void testcase004LogOutFromAccount(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_2");
    }

    @Test
    public void testcase004LogOutFromAccountDataSet1() {
        testcase004LogOutFromAccount(websitelink);
    }
}
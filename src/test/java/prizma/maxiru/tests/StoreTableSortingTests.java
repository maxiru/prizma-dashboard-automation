package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StoreTableSortingTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002OpenStorePageAndSortStoreBotsTableByName(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();

        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.clickOnStoreTableSwitcherIcon();
        actionwords.getScreenshot("testcase002OpenStorePageAndSortStoreBotsTableByName_1");
        actionwords.clickOnStoreTableAscDescIcon();
        actionwords.getScreenshot("testcase002OpenStorePageAndSortStoreBotsTableByName_2");
        actionwords.clickOnStoreTableAscDescIcon();
        actionwords.getScreenshot("testcase002OpenStorePageAndSortStoreBotsTableByName_3");
    }

    @Test
    public void testcase002OpenStorePageAndSortStoreBotsTableByNameDataSet1() {
        testcase002OpenStorePageAndSortStoreBotsTableByName(websitelink);
    }

    public void testcase003OpenStorePageAndSortStoreBotsTableByValue(String link, String value) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.clickOnStoreTableSwitcherIcon();
        actionwords.getScreenshot("testcase003-0OpenStorePageAndSortStoreBotsTableByValue"+value);
        actionwords.sortByNumberOfInvestors(value);
        actionwords.getScreenshot("testcase003-1OpenStorePageAndSortStoreBotsTableByValue"+value);
        actionwords.sortByNumberOfInvestors(value);
        actionwords.getScreenshot("testcase003-2OpenStorePageAndSortStoreBotsTableByValue"+value);
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByPerformance() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "backtestPerformance");
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByRisk() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "risk");
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByFrequency() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "frequency");
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByInvestor() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "subscribers");
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByCommission() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "commission");
    }

    @Test
    public void testcase003OpenStorePageAndSortStoreBotsTableByPublishedDate() {
        testcase003OpenStorePageAndSortStoreBotsTableByValue(websitelink, "publishedDate");
    }


    public void testcase008LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase008LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase008LogOutFromAccount_2");
    }

    @Test
    public void testcase008LogOutFromAccountDataSet1() {
        testcase008LogOutFromAccount(websitelink);
    }
}
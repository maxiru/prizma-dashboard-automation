package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class StoreSwitchPagesTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002OpenStorePageAndClickShowMoreOnGridView(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase002OpenStorePageAndClickShowMoreOnGridView_1");
        actionwords.clickOnStoreGridSwitcherIcon();
        actionwords.clickOnStoreShowMoreButton();
        actionwords.getScreenshot("testcase002OpenStorePageAndClickShowMoreOnGridView_2");
    }

    @Test
    public void testcase002OpenStorePageAndClickShowMoreOnGridView() {
        testcase002OpenStorePageAndClickShowMoreOnGridView(websitelink);
    }

    public void testcase003OpenStorePageAndClickShowMoreOnTableView(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnStoreBtnOnHomePage();
        actionwords.getScreenshot("testcase003OpenStorePageAndClickShowMoreOnTableView_1");
        actionwords.clickOnStoreTableSwitcherIcon();
        actionwords.clickOnStoreShowMoreButton();
        actionwords.getScreenshot("testcase003OpenStorePageAndClickShowMoreOnTableView_2");
    }

    @Test
    public void testcase003OpenStorePageAndClickShowMoreOnTableView() {
        testcase003OpenStorePageAndClickShowMoreOnTableView(websitelink);
    }

    public void testcase004LogOutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase004LogOutFromAccount_2");
    }

    @Test
    public void testcase004LogOutFromAccountDataSet1() {
        testcase004LogOutFromAccount(websitelink);
    }
}
package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import prizma.maxiru.framework.common.PrizmaDriver;
import prizma.maxiru.framework.common.PrizmaScreenshotRule;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.websitelink;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AccountPageTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001SignInIntoPrizmaDashboard(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase001SignInIntoPrizmaDashboard");
    }

    @Test
    public void testcase001SignInIntoPrizmaDashboardDataSet1() {
        testcase001SignInIntoPrizmaDashboard(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase002CheckAccountPage(String link) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.getScreenshot("testcase002CheckAccountPage");
        actionwords.verifyElementsOnAccountPage();
    }

    @Test
    public void testcase002CheckAccountPageDataSet1() {
        testcase002CheckAccountPage(websitelink);
    }

    public void testcase003ChangeFullName(String link, String fullName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnFullName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCloseModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnFullName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCancelModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnFullName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.setValueInUserEditModalTextInput(fullName);
        actionwords.setValueInUserEditModalPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
    }

    @Test
    public void testcase003ChangeFullName() {
        testcase003ChangeFullName(websitelink, "autotest full name");
    }

    public void testcase004ChangeFullNameNegativeTest(String link, String fullName, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnFullName();
        actionwords.setValueInUserEditModalTextInput(fullName);
        actionwords.setValueInUserEditModalPasswordInput(password);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsDisplayed();
    }

    @Test
    public void testcase004ChangeFullNameEmptyNameAndPassword() {
        testcase004ChangeFullNameNegativeTest(websitelink, "", "");
    }

    @Test
    public void testcase005ChangeFullNameIncorrectName() {
        testcase004ChangeFullNameNegativeTest(websitelink, "!", PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase006ChangeFullNameIncorrectPassword() {
        testcase004ChangeFullNameNegativeTest(websitelink, "aaa", "!!!");
    }

    public void testcase007ChangeEmail(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnEmail();
        actionwords.verifyUserEditModalIsNotDisplayed();
    }

    @Test
    public void testcase007ChangeEmail() {
        testcase007ChangeEmail(websitelink);
    }

    public void testcase008ChangePassword(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnPassword();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCloseModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnPassword();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCancelModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnPassword();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.setValueInUserEditModalCurrentPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.setValueInUserEditModalNewPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.setValueInUserEditModalConfirmPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
    }

    @Test
    public void testcase008ChangePassword() {
        testcase008ChangePassword(websitelink);
    }

    public void testcase009ChangePasswordNegativeTest(String link, String current, String newPass, String confirm) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnPassword();
        actionwords.setValueInUserEditModalCurrentPasswordInput(current);
        actionwords.setValueInUserEditModalNewPasswordInput(newPass);
        actionwords.setValueInUserEditModalConfirmPasswordInput(confirm);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsDisplayed();
    }

    @Test
    public void testcase009ChangePasswordEmptyValues() {
        testcase009ChangePasswordNegativeTest(websitelink, "", "", "");
    }

    @Test
    public void testcase010ChangePasswordIncorrectCurrent() {
        testcase009ChangePasswordNegativeTest(websitelink, "qq", PASSWORD_FOR_SIGN_IN_TESTS,  PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase011ChangePasswordIncorrectNew() {
        testcase009ChangePasswordNegativeTest(websitelink, PASSWORD_FOR_SIGN_IN_TESTS, "qq", PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase012ChangePasswordIncorrectConfirm() {
        testcase009ChangePasswordNegativeTest(websitelink, PASSWORD_FOR_SIGN_IN_TESTS, "qq", PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase013ChangePasswordWrongCurrent() {
        testcase009ChangePasswordNegativeTest(websitelink,"qqq", PASSWORD_FOR_SIGN_IN_TESTS,  PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase014ChangePasswordWrongNew() {
        testcase009ChangePasswordNegativeTest(websitelink, PASSWORD_FOR_SIGN_IN_TESTS, "qqq", PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase015ChangePasswordWrongConfirm() {
        testcase009ChangePasswordNegativeTest(websitelink, PASSWORD_FOR_SIGN_IN_TESTS,  PASSWORD_FOR_SIGN_IN_TESTS,"qqq");
    }

    public void testcase016ChangeDisplayName(String link, String name) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnDisplayName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCloseModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnDisplayName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCancelModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnDisplayName();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.setValueInUserEditModalTextInput(name);
        actionwords.setValueInUserEditModalPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
    }

    @Test
    public void testcase016ChangeDisplayName() {
        testcase016ChangeDisplayName(websitelink, "autotest display name");
    }

    public void testcase017ChangeDisplayNameNegativeTest(String link, String name, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnDisplayName();
        actionwords.setValueInUserEditModalTextInput(name);
        actionwords.setValueInUserEditModalPasswordInput(password);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsDisplayed();
    }

    @Test
    public void testcase017ChangeDisplayNameEmptyNameAndPassword() {
        testcase017ChangeDisplayNameNegativeTest(websitelink, "", "");
    }

    @Test
    public void testcase018ChangeDisplayNameIncorrectName() {
        testcase017ChangeDisplayNameNegativeTest(websitelink, "!", PASSWORD_FOR_SIGN_IN_TESTS);
    }

    @Test
    public void testcase019ChangeDisplayNameIncorrectPassword() {
        testcase017ChangeDisplayNameNegativeTest(websitelink, "aaa", "!!!");
    }

    public void testcase020ChangeUSDCAddress(String link, String name) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnUSDCAddress();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCloseModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnUSDCAddress();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.clickOnCancelModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
        actionwords.clickOnUSDCAddress();
        actionwords.verifyUserEditModalIsDisplayed();
        actionwords.setValueInUserEditModalTextInput(name);
        actionwords.setValueInUserEditModalPasswordInput(PASSWORD_FOR_SIGN_IN_TESTS);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsNotDisplayed();
    }

    @Test
    public void testcase020ChangeUSDCAddress() {
        testcase020ChangeUSDCAddress(websitelink, "autotest USDC Address");
    }

    public void testcase021ChangeUSDCAddressNegativeTest(String link, String name, String password) {
        Assume.assumeTrue(PrizmaDriver.isChromeBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnUSDCAddress();
        actionwords.setValueInUserEditModalTextInput(name);
        actionwords.setValueInUserEditModalPasswordInput(password);
        actionwords.clickOnOkModalButton();
        actionwords.verifyUserEditModalIsDisplayed();
    }

    @Test
    public void testcase021ChangeUSDCAddressEmptyNameAndPassword() {
        testcase021ChangeUSDCAddressNegativeTest(websitelink, "", "");
    }

    @Test
    public void testcase022ChangeUSDCAddressIncorrectPassword() {
        testcase021ChangeUSDCAddressNegativeTest(websitelink, "aaa", "!!!");
    }

    public void testcase023LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_3");
    }

    @Test
    public void testcase023LogoutFromAccountDataSet1() {
        testcase023LogoutFromAccount(websitelink);
    }
}
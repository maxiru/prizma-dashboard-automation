package prizma.maxiru.tests;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static app.PrizmaSignupPage.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreatorSplashScreenTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001PassSignUpWithValidTestDataAsInvestor(String link, String fullName, String email, String displayName, String password, String repeatPassword) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestor_1");
        actionwords.setFullNameOnSignUpPage(fullName);
        actionwords.setEmailOnSignUpPage(email);
        actionwords.setDisplayNameOnSignUpPage(displayName);
        actionwords.checkInvestorCheckboxOnSignUpPage();
        actionwords.setPasswordOnSignUpPage(password);
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestor_2");
        actionwords.setRepeatPasswordOnSignUpPage(repeatPassword);
        actionwords.clickOnRegisterBtnOnSignUpPage();
        actionwords.verifyThatUserRedirectFromSignUpPageToHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase001PassSignUpWithValidTestDataAsInvestor_3");
    }

    @Test
    public void testcase001PassSignUpWithValidTestDataAsInvestorDataSet1() {
        testcase001PassSignUpWithValidTestDataAsInvestor(websitelink, "Roman Krasniuk", "roman", "rkrasniuk", "Romank_1992", "Romank_1992");
    }

    public void testcase002ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase002ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen");
    }

    @Test
    public void testcase002ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenDataSet1() {
        testcase002ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreen(websitelink);
    }

    public void testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen");
    }

    @Test
    public void testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreenDataSet1() {
        testcase003ClickOnInvestorBtnAndCheckThatUserNotLandOnInvestorSplashScreen(websitelink);
    }

    public void testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreement(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreement_1");
        actionwords.clickOnUserAgreementOpenBtnOnCreatorPage();
        actionwords.getScreenshot("testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreement_2");
        actionwords.clickOnUserAgreementCloseBtnOnCreatorPage();
        actionwords.getScreenshot("testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreement_3");
    }

    @Test
    public void testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreementDataSet1() {
        testcase004ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAncCheckAbilityOpenCloseCreatorUserAgreement(websitelink);
    }

    public void testcase005ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenTryToClickOnStartCreatingBtnOnCreatorPageWhenUserAgreementNotApprove(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase005ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenTryToClickOnStartCreatingBtnOnCreatorPageWhenUserAgreementNotApprove_1");
        actionwords.clickOnStartCreatingBtnOnCreatorPage();
        actionwords.getScreenshot("testcase005ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenTryToClickOnStartCreatingBtnOnCreatorPageWhenUserAgreementNotApprove_2");
    }

    @Test
    public void testcase005ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenTryToClickOnStartCreatingBtnOnCreatorPageWhenUserAgreementNotApproveDataSet1() {
        testcase005ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenTryToClickOnStartCreatingBtnOnCreatorPageWhenUserAgreementNotApprove(websitelink);
    }

    public void testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreement(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreement_1");
        actionwords.clickOnUserAgreementToggleOnCreatorPageToEnable();
        actionwords.getScreenshot("testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreement_2");
        actionwords.clickOnUserAgreementToggleOnCreatorPageToDisable();
        actionwords.getScreenshot("testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreement_2");
    }

    @Test
    public void testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreementDataSet1() {
        testcase006ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableDisableUserAgreement(websitelink);
    }

    public void testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenPresentOnCreatorPage();
        actionwords.getScreenshot("testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_1");
        actionwords.clickOnUserAgreementToggleOnCreatorPageToEnable();
        actionwords.getScreenshot("testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_2");
        actionwords.clickOnStartCreatingBtnOnCreatorPage();
        actionwords.getScreenshot("testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_3");
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_4");
        actionwords.refreshPage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole_5");
    }

    @Test
    public void testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRoleDataSet1() {
        testcase007ClickOnCreatorBtnAndCheckThatUserLandOnCreatorSplashScreenAndCheckAbilityEnableUserAgreementAndClickStartCreatingBtnToUpdateUserRole(websitelink);
    }

    public void testcase008LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase008LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase008LogoutFromAccount_2");
    }

    @Test
    public void testcase008LogoutFromAccountDataSet1() {
        testcase008LogoutFromAccount(websitelink);
    }

    public void testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(String link, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(LP_EMAIL_FOR_FUTURE_TESTS);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_1");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_2");
        actionwords.refreshPage();
        actionwords.clickOnInvestorBtnOnHomePage();
        actionwords.verifyThatInvestorSplashScreenNotPresentOnInvestorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_3");
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.verifyThatCreatorSplashScreenNotPresentOnCreatorPage();
        actionwords.getScreenshot("testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage_4");
    }

    @Test
    public void testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPageDataSet1() {
        testcase009SignInBackWithCorrectUsernamePasswordFromSignUpFlowAndCheckThatUserRedirectToInvestorPage(websitelink, "Romank_1992");
    }

    public void testcase010LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase010LogoutFromAccount_1");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase010LogoutFromAccount_2");
    }

    @Test
    public void testcase010LogoutFromAccountDataSet1() {
        testcase010LogoutFromAccount(websitelink);
    }
}
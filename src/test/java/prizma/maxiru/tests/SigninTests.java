package prizma.maxiru.tests;

import org.junit.Assume;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import prizma.maxiru.framework.common.*;

import static prizma.maxiru.framework.common.PrizmaConstants.*;
import static prizma.maxiru.framework.common.PrizmaDriver.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SigninTests {
    @Rule
    public PrizmaScreenshotRule screenshotRule = new PrizmaScreenshotRule();

    public Actionwords actionwords = new Actionwords();

    public void testcase001TryToSignInWithEmptyCredentialsAndCheckThatErrorAppear(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatEmailErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase001TryToSignInWithEmptyCredentialsAndCheckThatErrorAppear");
    }

    @Test
    public void testcase001TryToSignInWithEmptyCredentialsAndCheckThatErrorAppearDataSet1() {
        testcase001TryToSignInWithEmptyCredentialsAndCheckThatErrorAppear(websitelink);
    }

    public void testcase002TryToSignInWithEmptyEmailAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatEmailErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase002TryToSignInWithEmptyUsernameAndCheckThatErrorAppear");
    }

    @Test
    public void testcase002TryToSignInWithEmptyEmailAndCheckThatErrorAppearDataSet1() {
        testcase002TryToSignInWithEmptyEmailAndCheckThatErrorAppear(websitelink, "", "Romank_1992");
    }

    public void testcase003TryToSignInWithEmptyPasswordAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase003TryToSignInWithEmptyPasswordAndCheckThatErrorAppear");
    }

    @Test
    public void testcase003TryToSignInWithEmptyPasswordAndCheckThatErrorAppearDataSet1() {
        testcase003TryToSignInWithEmptyPasswordAndCheckThatErrorAppear(websitelink, EMAIL_FOR_SIGN_IN_TESTS, "");
    }

    public void testcase004TryToSignInUsernameWhichValidButPasswordIsNotValidPasswordAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.getScreenshot("testcase004TryToSignInUsernameWhichValidButPasswordIsNotValidPasswordAndCheckThatErrorAppear");
    }

    @Test
    public void testcase004TryToSignInUsernameWhichValidButPasswordIsNotValidPasswordAndCheckThatErrorAppearDataSet1() {
        testcase004TryToSignInUsernameWhichValidButPasswordIsNotValidPasswordAndCheckThatErrorAppear(websitelink, EMAIL_FOR_SIGN_IN_TESTS, "aaa1");
    }

    public void testcase005TryToSignInWithPasswordWhichHas1SymbolAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase005TryToSignInWithPasswordWhichHas1SymbolAndCheckThatErrorAppear");
    }

    @Test
    public void testcase005TryToSignInWithPasswordWhichHas1SymbolAndCheckThatErrorAppearDataSet1() {
        testcase005TryToSignInWithPasswordWhichHas1SymbolAndCheckThatErrorAppear(websitelink, EMAIL_FOR_SIGN_IN_TESTS, "a");
    }

    public void testcase006TryToSignInWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatPasswordErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase006TryToSignInWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear");
    }

    @Test
    public void testcase006TryToSignInWithPasswordWhichHas2SymbolsAndCheckThatErrorAppearDataSet1() {
        testcase006TryToSignInWithPasswordWhichHas2SymbolsAndCheckThatErrorAppear(websitelink, EMAIL_FOR_SIGN_IN_TESTS, "aa");
    }

    public void testcase007TryToSignInPasswordWhichHas3SymbolsButPasswordIsNotValidUsernameAndCheckThatErrorAppear(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatIncorrectEmailPasswordErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase007TryToSignInPasswordWhichHas3SymbolsButPasswordIsNotValidUsernameAndCheckThatErrorAppear");
    }

    @Test
    public void testcase007TryToSignInPasswordWhichHas3SymbolsButPasswordIsNotValidUsernameAndCheckThatErrorAppearDataSet1() {
        testcase007TryToSignInPasswordWhichHas3SymbolsButPasswordIsNotValidUsernameAndCheckThatErrorAppear(websitelink, EMAIL_FOR_SIGN_IN_TESTS, "aa1");
    }

    public void testcase008SwitchFromSignInPageToSignUpPage(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase008SwitchFromSignInPageToSignUpPage_1");
        actionwords.clickOnRegistrationBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToSignUpPage();
        actionwords.getScreenshot("testcase008SwitchFromSignInPageToSignUpPage_2");
    }

    @Test
    public void testcase008SwitchFromSignInPageToSignUpPageDataSet1() {
        testcase008SwitchFromSignInPageToSignUpPage(websitelink);
    }

    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(String link, String email, String password, String fileName) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatEmailErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot(fileName);
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet1() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "r@autotest.com", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet1");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet2() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "roman@i.ua", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet2");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet3() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "roman@gmailcom", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet3");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet4() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "roman@@autotest.com", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet4");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet5() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "roman@gmail..com", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet5");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet6() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "romangmail.com", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet6");
    }

    @Test
    public void testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet7() {
        testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppear(websitelink, "роман@autotest.com", PASSWORD_FOR_SIGN_IN_TESTS, "testcase009TryToSignInWithIncorrectEmailAndCheckThatErrorAppearDataSet7");
    }

    public void testcase010SignInWithCorrectEmailPasswordAndCheckThatUserRedirectToHomePage(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.clickOnSignInBtnOnSignInPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase010SignInWithCorrectEmailPasswordAndCheckThatUserRedirectToHomePage_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase010SignInWithCorrectEmailPasswordAndCheckThatUserRedirectToHomePage_2");
    }

    @Test
    public void testcase010SignInWithCorrectEmailPasswordAndCheckThatUserRedirectToHomePageDataSet1() {
        testcase010SignInWithCorrectEmailPasswordAndCheckThatUserRedirectToHomePage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase011LogoutFromAccount(String link) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase011LogoutFromAccount_3");
    }

    @Test
    public void testcase011LogoutFromAccountDataSet1() {
        testcase011LogoutFromAccount(websitelink);
    }

    public void testcase012TryToHitEnterOnKeyboardOnUsernameInputWithEmptyCredentialsAndCheckThatErrorAppear(String link, String email) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPageWithEnter(email);
        actionwords.verifyThatEmailErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase012TryToHitEnterOnKeyboardOnUsernameInputWithEmptyCredentialsAndCheckThatErrorAppear");
    }

    @Test
    public void testcase012TryToHitEnterOnKeyboardOnUsernameInputWithEmptyCredentialsAndCheckThatErrorAppearDataSet1() {
        testcase012TryToHitEnterOnKeyboardOnUsernameInputWithEmptyCredentialsAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase013TryToHitEnterOnKeyboardOnPasswordInputWithEmptyCredentialsAndCheckThatErrorAppear(String link, String password) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setPasswordOnSignInPageWithEnter(password);
        actionwords.verifyThatEmailErrorMessageAppearOnSignInPage();
        actionwords.getScreenshot("testcase013TryToHitEnterOnKeyboardOnPasswordInputWithEmptyCredentialsAndCheckThatErrorAppear");
    }

    @Test
    public void testcase013TryToHitEnterOnKeyboardOnPasswordInputWithEmptyCredentialsAndCheckThatErrorAppearDataSet1() {
        testcase013TryToHitEnterOnKeyboardOnPasswordInputWithEmptyCredentialsAndCheckThatErrorAppear(websitelink, "");
    }

    public void testcase014SignInWithCorrectUsernamePasswordHitEnterOnKeyboardAndCheckThatUserRedirectToHomePage(String link, String email, String password) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPageWithEnter(password);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase014SignInWithCorrectUsernamePasswordHitEnterOnKeyboardAndCheckThatUserRedirectToHomePage_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase014SignInWithCorrectUsernamePasswordHitEnterOnKeyboardAndCheckThatUserRedirectToHomePage_2");
    }

    @Test
    public void testcase014SignInWithCorrectUsernamePasswordHitEnterOnKeyboardAndCheckThatUserRedirectToHomePageDataSet1() {
        testcase014SignInWithCorrectUsernamePasswordHitEnterOnKeyboardAndCheckThatUserRedirectToHomePage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }

    public void testcase015LogoutFromAccount(String link) {
        Assume.assumeTrue(!PrizmaDriver.isFirefoxBrowser());
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.clickOnCreatorBtnOnHomePage();
        actionwords.getScreenshot("testcase015LogoutFromAccount_1");
        actionwords.refreshPage();
        actionwords.verifyThatUserRedirectFromSignInPageToHomePage();
        actionwords.getScreenshot("testcase015LogoutFromAccount_2");
        actionwords.clickOnAccountBtnOnHomePage();
        actionwords.clickOnLogOutBtnOnProfilePage();
        actionwords.verifyThatUserRedirectFromHomePageToSignInPage();
        actionwords.getScreenshot("testcase015LogoutFromAccount_3");
    }

    @Test
    public void testcase015LogoutFromAccountDataSet1() {
        testcase015LogoutFromAccount(websitelink);
    }

    public void testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPage(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.getScreenshot("testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPage_1");
        actionwords.setEmailOnSignInPageWithTab(email);
        actionwords.getScreenshot("testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPage_2");
        actionwords.setPasswordOnSignInPageWithTab(password);
        actionwords.getScreenshot("testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPage_3");
    }

    @Test
    public void testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPageDataSet1() {
        testcase016CheckTabBtnOnKeyboardSwitchPointerBetweenInputsSignInBtnOnSignInPage(websitelink, "", "");
    }

    public void testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage(String link, String email, String password) {
        actionwords.setFocusInBrowser();
        actionwords.openWebsite(link);
        actionwords.setEmailOnSignInPage(email);
        actionwords.setPasswordOnSignInPage(password);
        actionwords.getScreenshot("testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage_1");
        actionwords.clickOnShowPasswordBtnOnSignInPage();
        actionwords.getScreenshot("testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage_2");
        actionwords.clickOnHidePasswordBtnOnSignInPage();
        actionwords.getScreenshot("testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage_3");
        actionwords.clickOnShowPasswordBtnOnSignInPage();
        actionwords.getScreenshot("testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage_4");
    }

    @Test
    public void testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPageDataSet1() {
        testcase017CheckHowWorksShowHidePasswordFeatureOnSignInPage(websitelink, EMAIL_FOR_SIGN_IN_TESTS, PASSWORD_FOR_SIGN_IN_TESTS);
    }
}